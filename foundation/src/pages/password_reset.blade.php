---
subject: Reset Password
---
<style type="text/css">
  body,
  html, 
  .body {
    background: #f3f3f3 !important;
  }

  .header {
    background: #f3f3f3;
  }
</style>

<container>

  <row class="header">
    <columns>
        
      <spacer size="16"></spacer>
      
      <h4 class="text-center">IRM Web Application</h4>

    </columns>
  </row>

  <row>
    <columns>

      <spacer size="16"></spacer>

      <h1 class="text-center">Forgot Your Password?</h1>
      
      <spacer size="16"></spacer>

      <p class="text-center">It happens. Click the link below to reset your password.</p>
      <button class="large expand" href="#">Reset Password</button>

    </columns>
  </row>

  <spacer size="16"></spacer>
</container>
