# IRM RESTful API

Laravel-based PHP Project for IRM Back-end API.
Development version is deployed on http://api.angelus.io

Works with https://github.com/naturalui/IRM-Client


### Installation

Prerequisites; to launch the Vagrant based development environment please make sure the following is installed on your local system:

  - [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
  - [Vagrant](https://www.vagrantup.com)
  - Git

### Launching

After the prerequisites have been installed clone this repository and in the cloned directory run the following command:

    vagrant up

This will launch the Vagrant based virtual development environment provisioned with the required packages and config. Building the environment will take a little while since a number of dependecies need to be downloaded.

### Access

To access the application point your browser to the following location:

    http://localhost:8000

To access the mysql server from outside the Vagrant VM use port `13306` with username `angelusdev` and password `angelus`.

### Testing

To run unit testing for various API endpoints, run the following command from the root directory.

    phpunit

We have created the following test users:

	admin@test.com/admin
	manager@test.com/manager
	client@test.com/client


### Development

Edit files on your local file system with your editor/IDE of choice. 
Run Git on your local machine. Run Composer from within the Vagrant box.

## API Reference

## 1. Authentication / Registration Endpoints

### 1.1 Authentication : POST /v1/user/auth

Authenticate user based on credentials (email and password). On success, it returns JWT Token. Otherwise error message with error code is returned.

#### Parameters

<table>
<thead>
	<tr>
		<th>Name</th>
		<th>Required?</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
</thead>

<tbody>
	<tr>
		<td>email</td>
		<td>*required</td>
		<td>string</td>
		<td>user email address</td>
	</tr>
	<tr>
		<td>password</td>
		<td>*required</td>
		<td>string</td>
		<td>user password</td>
	</tr>
</tbody>

</table>

#### Example Request

```bash
curl -H "Content-Type: application/json" \
-H "X-Requested-With: XMLHttpRequest" \
-d '{"email": "admin@test.com", "password": "admin"}' \
-X POST http://api.angelus.io/v1/user/auth 
```

#### Example Response

```json
{
	"code":0,
	"message":"success",
	"data":
		{
			"token" :"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2FwaS5hbmdlbHVzLmlvXC9hcGlcL3YxXC91c2VyXC9hdXRoIiwiaWF0IjoxNDYzNzUwMzYzLCJleHAiOjE0NjM3NTM5NjMsIm5iZiI6MTQ2Mzc1MDM2MywianRpIjoiYjkzMGY0OTJkN2UwMGM3YjE1ZWI2NDFiMWI2ODlkM2EifQ.g8uYSErw9vqlqhvkn-67KSEuhZt2xEfh5tzxHymuhDo"
		}
}
```


#### Note

JWT Token included in the response will be used for further REST API calls in the following format in Header.

`
Authorization: Bearer {your jwt token here}
`

### 1.2 Registration : POST /v1/user/register

Create new user. JWT Token for newly registered user will be returned.

#### Parameters

<table>
<thead>
	<tr>
		<th>Name</th>
		<th>Required?</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
</thead>

<tbody>
	<tr>
		<td>email</td>
		<td>*required</td>
		<td>string</td>
		<td>user email address</td>
	</tr>
	<tr>
		<td>password</td>
		<td>*required</td>
		<td>string</td>
		<td>user password. mimimum 6 characters</td>
	</tr>
	<tr>
		<td>first_name</td>
		<td>optional</td>
		<td>string</td>
		<td>First name of user</td>
	</tr>
	<tr>
		<td>last_name</td>
		<td>optional</td>
		<td>string</td>
		<td>Last name of user</td>
	</tr>
	<tr>
		<td>phone</td>
		<td>optional</td>
		<td>string</td>
		<td>Phone number</td>
	</tr>
	<tr>
		<td>street</td>
		<td>optional</td>
		<td>string</td>
		<td>Address: Street</td>
	</tr>
	<tr>
		<td>city</td>
		<td>optional</td>
		<td>string</td>
		<td>Address: City</td>
	</tr>
	<tr>
		<td>zip</td>
		<td>optional</td>
		<td>string</td>
		<td>Address: Zip</td>
	</tr>
	<tr>
		<td>country</td>
		<td>optional</td>
		<td>string</td>
		<td>Address: Country</td>
	</tr>
	<tr>
		<td>agree_terms</td>
		<td>*required</td>
		<td>string</td>
		<td>1 or 0</td>
	</tr>
</tbody>

</table>

#### Example Request

```bash
curl -H "Content-Type: application/json" \
-H "X-Requested-With: XMLHttpRequest" \
-d '{"email": "test02@test.com", "password": "newpassword", "agree_terms" : "1"}' \
-X POST http://api.angelus.io/v1/user
```

#### Example Response

```json
{
	"code":0,
	"message":"success",
	"data":
		{
			"token" :"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6XC9cL2FwaS5hbmdlbHVzLmlvXC9hcGlcL3YxXC91c2VyIiwiaWF0IjoxNDYzNzUwOTQ2LCJleHAiOjE0NjM3NTQ1NDYsIm5iZiI6MTQ2Mzc1MDk0NiwianRpIjoiNTg3NGE0YWNkMzgxZGU4MjA1ZGExNjExMzE0OGUxODkifQ.Zpa5LdUib6Z6hUEp1kn80VV4G7-unNmH7qtT795nV6E"
		}
}
```


### 1.3 Get current user details : GET /v1/user/self

Get user details for current token.

#### Parameters

None

#### Example Request

```bash
curl -H "Content-Type: application/json" \
-H "X-Requested-With: XMLHttpRequest" \
-H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo5MDkwXC9hcGlcL3YxXC91c2VyXC9hdXRoIiwiaWF0IjoxNDY1MjE1ODIzLCJleHAiOjE0NjUyMTk0MjMsIm5iZiI6MTQ2NTIxNTgyMywianRpIjoiYzUwYjFhOGZlZWE4ZjY0NDBlNTUyNmQxNWNiY2IwYjkifQ.bC6xD7ECylUxoaWN9KAKWCxZOJXRqgTgJs-i_EGb2Qw" \
-X GET http://api.angelus.io/v1/user/self
```

#### Example Response

```json

{
  "code": 0,
  "message": "success",
  "data": {
    "id": 1,
    "first_name": "First name",
    "last_name": "Last name",
    "email": "admin@gmail.com",
    "phone": null,
    "street": null,
    "city": null,
    "zip": null,
    "country": null
  }
}

```

## 2. Accounts Relalated Endpoints

### 2.1 Account model info : GET /v1/account/info

Get account model information. 

#### Parameters

None

#### Headers

`
Authorization: Bearer {your jwt token here}
`

### 2.2 Account list : GET /v1/account

Get list of accounts

#### Parameters

None

#### Headers

`
Authorization: Bearer {your jwt token here}
`

### 2.3 Account details : GET /v1/account/{id}

Get details of account.

#### Parameters

 id :  account id 

#### Headers

`
Authorization: Bearer {your jwt token here}
`

### 2.4 Account creation : POST /v1/account

Create new account.

#### Parameters

<table>
<thead>
	<tr>
		<th>Name</th>
		<th>Required?</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
</thead>

<tbody>
	<tr>
		<td>title</td>
		<td>*required</td>
		<td>string</td>
		<td>account title</td>
	</tr>
	<tr>
		<td>type</td>
		<td>*required</td>
		<td>int</td>
		<td>account type id</td>
	</tr>
	<tr>
		<td>status</td>
		<td>*required</td>
		<td>int</td>
		<td>account status id</td>
	</tr>
</tbody>

</table>

#### Headers

`
Authorization: Bearer {your jwt token here}
`

#### Example Request

```bash
curl -H "Content-Type: application/json" \
-H "X-Requested-With: XMLHttpRequest" \
-H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2FwaS5hbmdlbHVzLmlvXC9hcGlcL3YxXC91c2VyXC9hdXRoIiwiaWF0IjoxNDY0ODkyNzg0LCJleHAiOjE0NjQ4OTYzODQsIm5iZiI6MTQ2NDg5Mjc4NCwianRpIjoiNTVkYTdkMTM2OGFjYTQ5N2ZjNzhhODVjMDQ0ZjdlODQifQ.Jkq0xmZbyeWOojf7v17vpnmTT9UwEEgp5L2_pI90haE" \
-d '{"title": "new account", "type": 1, "status": 2}' \
-X POST http://api.angelus.io/v1/account
```

### 2.5 Account update : PUT /v1/account/{id}

Update account information.

#### Parameters

<table>
<thead>
	<tr>
		<th>Name</th>
		<th>Required?</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
</thead>

<tbody>
	<tr>
		<td>title</td>
		<td>*required</td>
		<td>string</td>
		<td>account title</td>
	</tr>
	<tr>
		<td>type</td>
		<td>*required</td>
		<td>int</td>
		<td>account type id</td>
	</tr>
	<tr>
		<td>status</td>
		<td>*required</td>
		<td>int</td>
		<td>account status id</td>
	</tr>
</tbody>

</table>

#### Headers

`
Authorization: Bearer {your jwt token here}
`

#### Example Request

```bash
curl -H "Content-Type: application/json" \
-H "X-Requested-With: XMLHttpRequest" \
-H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2FwaS5hbmdlbHVzLmlvXC9hcGlcL3YxXC91c2VyXC9hdXRoIiwiaWF0IjoxNDY0ODkyNzg0LCJleHAiOjE0NjQ4OTYzODQsIm5iZiI6MTQ2NDg5Mjc4NCwianRpIjoiNTVkYTdkMTM2OGFjYTQ5N2ZjNzhhODVjMDQ0ZjdlODQifQ.Jkq0xmZbyeWOojf7v17vpnmTT9UwEEgp5L2_pI90haE" \
-d '{"title": "modified account", "type": 1, "status": 2}' \
-X PUT http://api.angelus.io/v1/account/10
```

### 2.6 Account delete : DELETE /v1/account/{id}

Remove account.

#### Parameters

 id :  account id 

#### Headers

`
Authorization: Bearer {your jwt token here}
`

#### Example Request

```bash
curl -H "Content-Type: application/json" \
-H "X-Requested-With: XMLHttpRequest" \
-H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2FwaS5hbmdlbHVzLmlvXC9hcGlcL3YxXC91c2VyXC9hdXRoIiwiaWF0IjoxNDY0ODkyNzg0LCJleHAiOjE0NjQ4OTYzODQsIm5iZiI6MTQ2NDg5Mjc4NCwianRpIjoiNTVkYTdkMTM2OGFjYTQ5N2ZjNzhhODVjMDQ0ZjdlODQifQ.Jkq0xmZbyeWOojf7v17vpnmTT9UwEEgp5L2_pI90haE" \
-X DELETE http://api.angelus.io/v1/account/15
```

#### Example Response

```json

{
	"code":0,
	"message":"success"
}

```

### 2.7 Transaction creation : POST /v1/account/transaction

Create new transaction for account.

#### Parameters

<table>
<thead>
	<tr>
		<th>Name</th>
		<th>Required?</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
</thead>

<tbody>
	<tr>
		<td>title</td>
		<td>*required</td>
		<td>string</td>
		<td>transaction title</td>
	</tr>
	<tr>
		<td>type</td>
		<td>*required</td>
		<td>int</td>
		<td>transaction type id</td>
	</tr>
	<tr>
		<td>status</td>
		<td>*required</td>
		<td>int</td>
		<td>transaction status id</td>
	</tr>
	<tr>
		<td>amount</td>
		<td>optional</td>
		<td>float</td>
		<td>amount</td>
	</tr>
	<tr>
		<td>account_id</td>
		<td>*required</td>
		<td>integer</td>
		<td>account id for this transaction</td>
	</tr>
</tbody>

</table>

#### Headers

`
Authorization: Bearer {your jwt token here}
`

### 2.8 Transaction update : PUT /v1/account/transaction/{id}

Update account transaction information.

#### Parameters

<table>
<thead>
	<tr>
		<th>Name</th>
		<th>Required?</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
</thead>

<tbody>
	<tr>
		<td>title</td>
		<td>*required</td>
		<td>string</td>
		<td>transaction title</td>
	</tr>
	<tr>
		<td>type</td>
		<td>*required</td>
		<td>int</td>
		<td>transaction type id</td>
	</tr>
	<tr>
		<td>status</td>
		<td>*required</td>
		<td>int</td>
		<td>transaction status id</td>
	</tr>
	<tr>
		<td>amount</td>
		<td>optional</td>
		<td>float</td>
		<td>amount</td>
	</tr>
	<tr>
		<td>account_id</td>
		<td>*required</td>
		<td>integer</td>
		<td>account id for this transaction</td>
	</tr>
</tbody>

</table>

#### Headers

`
Authorization: Bearer {your jwt token here}
`

### 2.9 Transaction delete : DELETE /v1/account/transaction/{id}

Remove account transaction

#### Parameters

 id :  transaction id 

#### Headers

`
Authorization: Bearer {your jwt token here}
`

### 2.10 Transaction details : GET /v1/account/transaction/{id}

Get details of account transaction.

#### Parameters

 id :  transaction id 

#### Headers

`
Authorization: Bearer {your jwt token here}
`
