<?php

namespace Verheijen\Bakery\Objects\Forms;

use Bakery;
use Verheijen\Bakery\Traits\FieldOven;
use Verheijen\Bakery\Traits\FieldAnalyzer;
use Verheijen\Bakery\Objects\BakeryModel;
use Verheijen\Bakery\Contracts\BakeryFormContract;

use Illuminate\Support\Collection;

class BakeryAddForm implements BakeryFormContract {

    use FieldOven;
    use FieldAnalyzer;

    private $model;
    private $fields;

    /*
     | Constructor
     |
     | @param           BakeryModel             $model we are generating the add form for
     | @return          void
    */
    public function __construct(BakeryModel $model)
    {
        $this->model = $model;
        $this->fields = $this->generate_fields();
    }

    #
    # Getters
    #

    public function get_model()
    {
        return $this->model;
    }

    public function get_fields()
    {
        return $this->fields;
    }

    #
    # Rendering
    #

    public function render()
    {
        return view('bakery::bread.renderer', [
            'model' => $this->model,
            'fields' => $this->fields
        ])->render();
    }

    #
    # Private functions
    #

    private function generate_fields()
    {
        $fields = new Collection;
        $exclusions = $this->get_excluded_fields();

        # If we have fillable attributes
        if( $this->model->has_fillable_attributes() )
        {
            # Process them
            foreach( $this->model->get_fillable_attributes() as $fillable_attribute )
            {
                # Make sure it is not an excluded field
                if( ! in_array($fillable_attribute->get_name(), $exclusions) )
                {
                    $field = $this->bake_field($fillable_attribute);
                    if( $field )
                    {
                        $fields->push($field);
                    }
                }
            }
        }

        # Loop through all the relationships of this model if it has any
        if( $this->model->has_relations() )
        {
            foreach( $this->model->get_relations() as $relation )
            {
                switch( $relation->get_type() )
                {
                    case "HasOne":
                        if( $relation->get_related_class()->has_fillable_attributes() )
                        {
                            // $fields->push($this->bake_custom_field("subtitle", "Has one ".$relation->get_method_name()));
                            foreach( $relation->get_related_class()->get_fillable_attributes() as $fillable_attribute )
                            {
                                if( $fillable_attribute->get_name() != $relation->get_instance()->getPlainForeignKey() )
                                {
                                    $fields->push($this->bake_field($fillable_attribute, null, null, $relation->get_method_name()));
                                }
                            }
                        }
                    break;
                    case "BelongsToMany":
                        $entries = $relation->get_related_class()->get_entries();
                        $title_field = $relation->get_related_class()->get_title_field();
                        $select_options = [];
                        if( $entries->count() > 0 )
                        {
                            foreach( $entries as $entry )
                            {
                                $select_options[$entry->get_id()] = $entry->get_data()->$title_field;
                            }
                        }
                        // $fields->push($this->bake_custom_field("subtitle", "Belongs to many ".$relation->get_method_name()));
                        $fields->push($this->bake_custom_field("multiple", $relation->get_method_name(), null, ['select_options' => $select_options]));
                    break;
                    case "MorphTo":
                        $method_name = $relation->get_method_name();
                        $all_models = Bakery::get_models();
                        $possible_morph_types = new Collection;
                        foreach( $all_models as $p_model )
                        {
                            if( $p_model->has_relations() )
                            {
                                foreach( $p_model->get_relations() as $relation )
                                {
                                    if( $relation->get_type() == "MorphMany" )
                                    {
                                        $flag = explode("_", $relation->get_instance()->getPlainMorphType())[0];
                                        if( $flag == $method_name )
                                        {
                                            $parent_model = $relation->get_parent_class();
                                            $title_field  = $parent_model->get_title_field();
                                            $possible_morph_types->push([
                                                'model' => $parent_model,
                                                'title' => $title_field
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                        $fields->push($this->bake_custom_field("morphTo", $method_name, null, ['morph_types' => $possible_morph_types]));
                    break;
                    case "MorphToMany":
                        $entries = $relation->get_related_class()->get_entries();
                        $title_field = $relation->get_related_class()->get_title_field();
                        $select_options = [];
                        if( $entries->count() > 0 )
                        {
                            foreach( $entries as $entry )
                            {
                                $select_options[$entry->get_id()] = $entry->get_data()->$title_field;
                            }
                        }
                        $fields->push($this->bake_custom_field("multiple", $relation->get_method_name(), null, ['select_options' => $select_options]));
                    break;
                }
            }
        }

        # Return our baked fields
        return $fields;
    }

    private function get_excluded_fields()
    {
        # Exclusions, these should be minimal but we need to at least skip one of the two morphTo relationship fields otherwise we get double fields in the form (it has the morphable_type and morphable_id attributes)
        # We skip the morphable_type attribute instead of the _id field since our algorithm already searches for attributes ending with _id so this saves us lines of code
        $exclusions = [];

        # Check if this model has any attributes related to relationships that we need to exclude
        if( $this->model->has_relations() )
        {
            foreach( $this->model->get_relations() as $relation )
            {
                switch( $relation->get_type() )
                {
                    case "MorphTo":
                        # Find out what the attribute name is of the morphable_type field (for example: likable_type)
                        # And add it to the excluded fields
                        $exclusions[] = $relation->get_instance()->getForeignKey();
                        $exclusions[] = $relation->get_instance()->getMorphType();
                    break;
                }
            }
        }

        # Check if the dev has specifically told us to exclude certain attributes from the edit form
        if( isset($this->model->get_instance()->add_exclude_attributes) )
        {
            $user_set_exclusions = $this->model->get_instance()->add_exclude_attributes;
            $exclusions = array_merge($exclusions, $user_set_exclusions);
        }

        return $exclusions;
    }

}
