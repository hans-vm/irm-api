<?php

namespace Verheijen\Bakery\Objects\Forms;

use Verheijen\Bakery\Objects\BakeryModel;
use Verheijen\Bakery\Objects\BakeryEntry;
use Verheijen\Bakery\Contracts\BakeryFormContract;

use Illuminate\Support\Collection;

class BakeryDeleteForm implements BakeryFormContract {

    private $model;
    private $entry;
    private $fields;

    /*
     | Constructor
     |
     | @param           BakeryModel             $model the $entry belongs to
     | @param           BakeryEntry             $entry we are generating the delete form for
     | @return          void
    */
    public function __construct(BakeryModel $model, BakeryEntry $entry)
    {
        $this->model = $model;
        $this->fields = $this->generate_fields();
    }

    #
    # Getters
    #

    public function get_model()
    {
        return $this->model;
    }

    public function get_fields()
    {
        return $this->fields;
    }

    #
    # Rendering
    #

    public function render()
    {
        return view('bakery::forms.delete', [
            'model' => $this->model,
            'entry' => $this->entry
        ]);
    }

    #
    # Private functions
    #

    private function generate_fields()
    {
        $fields = new Collection;



        return $fields;
    }

}
