<?php

namespace Verheijen\Bakery\Objects\Forms;

use Bakery;
use Verheijen\Bakery\Traits\FieldOven;
use Verheijen\Bakery\Traits\FieldAnalyzer;
use Verheijen\Bakery\Objects\BakeryModel;
use Verheijen\Bakery\Objects\BakeryEntry;
use Verheijen\Bakery\Contracts\BakeryFormContract;

use Illuminate\Support\Collection;

class BakeryEditForm implements BakeryFormContract {

    use FieldOven;
    use FieldAnalyzer;

    private $model;
    private $entry;
    private $fields;

    /*
     | Constructor
     |
     | @param           BakeryModel             $model the $entry belongs to
     | @param           BakeryEntry             $entry we are generating the edit form for
     | @return          void
    */
    public function __construct(BakeryModel $model, BakeryEntry $entry)
    {
        $this->model  = $model;
        $this->entry  = $entry;
        $this->fields = $this->generate_fields();
    }

    #
    # Getters
    #

    public function get_model()
    {
        return $this->model;
    }

    public function get_entry()
    {
        return $this->entry;
    }

    public function get_fields()
    {
        return $this->fields;
    }

    #
    # Rendering
    #

    public function render()
    {
        return view('bakery::bread.renderer', [
            'models' => $this->model,
            'fields' => $this->fields
        ]);
    }

    #
    # Private functions
    #

    private function generate_fields()
    {
        $fields = new Collection;
        $exclusions = $this->get_excluded_fields();

        # If we have fillable attributes
        if( $this->model->has_fillable_attributes() )
        {
            # Process them
            foreach( $this->model->get_fillable_attributes() as $fillable_attribute )
            {
                # Make sure it is not an excluded field
                if( ! in_array($fillable_attribute->get_name(), $exclusions) )
                {
                    $field = $this->bake_field($fillable_attribute, $this->entry);
                    if( $field )
                    {
                        $fields->push($field);
                    }
                }
            }
        }

        # Loop through all the relationships of this model if it has any
        if( $this->model->has_relations() )
        {
            foreach( $this->model->get_relations() as $relation )
            {
                switch( $relation->get_type() )
                {
                    case "HasOne":
                        if( $relation->get_related_class()->has_fillable_attributes() )
                        {
                            $method_name    = $relation->get_method_name();
                            $related_class  = $relation->get_related_class();
                            $relative       = $this->entry->get_data()->$method_name;
                            foreach( $relation->get_related_class()->get_fillable_attributes() as $fillable_attribute )
                            {
                                if( $fillable_attribute->get_name() != $relation->get_instance()->getPlainForeignKey() )
                                {
                                    $attr  = $fillable_attribute->get_name();
                                    $value = is_null($relative) ? null : new BakeryEntry($related_class, $relative->id);
                                    $fields->push($this->bake_field($fillable_attribute, $value, null, $relation->get_method_name()));
                                }
                            }
                        }
                    break;
                    case "BelongsToMany":
                        $method_name = $relation->get_method_name();
                        $entries     = $relation->get_related_class()->get_entries();
                        $title_field = $relation->get_related_class()->get_title_field();
                        $select_options = [];
                        if( $entries->count() > 0 )
                        {
                            foreach( $entries as $entry )
                            {
                                $select_options[$entry->get_id()] = $entry->get_data()->$title_field;
                            }
                        }
                        $relatives = $this->entry->get_data()->$method_name;
                        $value     = "";
                        if( $relatives->count() > 0 )
                        {
                            foreach( $relatives as $relative )
                            {
                                if( $value != "" ) $value .= ":";
                                $value .= $relative->id;
                            }
                        }
                        $options = ['select_options' => $select_options];
                        $fields->push($this->bake_custom_field("multiple", $relation->get_method_name(), $value, $options));
                    break;
                    case "MorphTo":
                        $method_name = $relation->get_method_name();
                        $all_models = Bakery::get_models();
                        # Determine the possibilities
                        $possible_morph_types = new Collection;
                        foreach( $all_models as $p_model )
                        {
                            if( $p_model->has_relations() )
                            {
                                foreach( $p_model->get_relations() as $p_relation )
                                {
                                    if( $p_relation->get_type() == "MorphMany" )
                                    {
                                        $flag = explode("_", $p_relation->get_instance()->getPlainMorphType())[0];
                                        if( $flag == $method_name )
                                        {
                                            $parent_model = $p_relation->get_parent_class();
                                            $title_field  = $parent_model->get_title_field();
                                            $possible_morph_types->push([
                                                'model' => $parent_model,
                                                'title' => $title_field
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                        # Determine the values
                        $values     = new Collection;

                        # Bugfix nr.1 part I
                        if( method_exists($relation->get_instance(), 'getPlainMorphType') )
                        {
                            $type_field = $relation->get_instance()->getPlainMorphType();
                        }
                        else
                        {
                            $type_field_parts = explode(".", $relation->get_instance()->getMorphType());
                            $last_type_part   = count($type_field_parts) - 1;
                            $type_field       = $type_field_parts[$last_type_part];
                        }

                        # Bugfix nr.2 part II
                        if( method_exists($relation->get_instance(), 'getPlainForeignKey') )
                        {
                            $key_field  = $relation->get_instance()->getPlainForeignKey();
                        }
                        else
                        {
                            $key_field_parts = explode(".", $relation->get_instance()->getForeignKey());
                            $last_key_part   = count($key_field_parts) - 1;
                            $key_field       = $key_field_parts[$last_key_part];
                        }

                        if( $this->entry->get_data()->$type_field != "" )
                        {
                            $values->put('type', $this->entry->get_data()->$type_field);
                            $values->put('child', $this->entry->get_data()->$key_field);
                        }

                        $options = ['morph_types' => $possible_morph_types];

                        # Push the morph to field on to our field stack
                        $fields->push($this->bake_custom_field("morphTo", $method_name, $values, $options));

                    break;
                    case "MorphToMany":
                        $method_name    = $relation->get_method_name();
                        $entries        = $relation->get_related_class()->get_entries();
                        $title_field    = $relation->get_related_class()->get_title_field();
                        $select_options = [];
                        if( $entries->count() > 0 )
                        {
                            foreach( $entries as $entry )
                            {
                                $select_options[$entry->get_id()] = $entry->get_data()->$title_field;
                            }
                        }
                        $relatives = $this->entry->get_data()->$method_name;
                        $value     = "";
                        if( $relatives->count() > 0 )
                        {
                            foreach( $relatives as $relative )
                            {
                                if( $value != "" ) $value .= ":";
                                $value .= $relative->id;
                            }
                        }
                        $options = ['select_options' => $select_options];
                        $fields->push($this->bake_custom_field("multiple", $relation->get_method_name(), $value, $options));
                    break;
                }
            }
        }

        # Return our baked fields
        return $fields;
    }

    private function get_excluded_fields()
    {
        # Exclusions, these should be minimal but we need to at least skip one of the two morphTo relationship fields otherwise we get double fields in the form (it has the morphable_type and morphable_id attributes)
        # We skip the morphable_type attribute instead of the _id field since our algorithm already searches for attributes ending with _id so this saves us lines of code
        $exclusions = [];

        # Check if this model has any attributes related to relationships that we need to exclude
        if( $this->model->has_relations() )
        {
            foreach( $this->model->get_relations() as $relation )
            {
                if( $relation->get_type() == "MorphTo" )
                {
                    # Find out what the attribute name is of the morphable_type field (for example: likable_type)
                    # And add it to the excluded fields
                    $exclusions[] = $relation->get_instance()->getForeignKey();
                    $exclusions[] = $relation->get_instance()->getMorphType();
                }
            }
        }

        # Check if the dev has specifically told us to exclude certain attributes from the edit form
        if( isset($this->model->get_instance()->edit_exclude_attributes) )
        {
            $user_set_exclusions = $this->model->get_instance()->edit_exclude_attributes;
            $exclusions = array_merge($exclusions, $user_set_exclusions);
        }

        return $exclusions;
    }

}
