<?php

namespace Verheijen\Bakery\Objects;

use Bakery;

class BakeryEntry {

    # @var BakeryModel - The (bakery) model this entry belongs to
    private $model;

    # @var Eloquent\Model - The entry of the model and all the data is posesses
    private $data;

    /*
     | Constructor
     |
     | @param           BakeryModel             $model this entry belongs to
     | @param           integer                 $id of the entry
     | @return          void
    */
    public function __construct(BakeryModel $model, $record)
    {
        $this->model = $model;
        $this->data  = $record;
    }

    #
    # Getters
    #

    public function get_model()
    {
        return $this->model;
    }

    public function get_data()
    {
        return $this->data;
    }

    public function get_id()
    {
        return $this->data->id;
    }

    public function get_value($attribute_name, $limit = 0)
    {
        # Try and grab the attribute from the model
        $attribute = $this->model->get_attribute($attribute_name);

        # If we failed to find the attribute, this must mean it is a special attribute we have added manually
        if( ! $attribute )
        {
            # This array will hold all the special attributes we might have automatically added
            $special_attributes = [];

            # Determine which special attributes could have been added
            if( $this->model->has_relations() )
            {
                foreach( $this->model->get_relations() as $relation )
                {
                    switch( $relation->get_type() )
                    {
                        case "MorphTo":
                            dd($relation->get_instance()->getMorphType());
                            $attr_name = explode("_", $relation->get_instance()->getMorphType())[0];
                            $special_attributes[] = $attr_name;
                        break;
                    }
                }
            }

            # If the given attribute is indeed a special attribute we automatically added
            if( in_array($attribute_name, $special_attributes) )
            {
                #
                # For now the only special attribute that could be added is the flag for the MorphTo relationship field
                #
                if( $this->model->has_relations() )
                {
                    foreach( $this->model->get_relations() as $relation )
                    {
                        switch( $relation->get_type() )
                        {
                            case "MorphTo":
                                $morphable_type = $relation->get_instance()->getMorphType();
                                $morphable_id   = $relation->get_instance()->getForeignKey();

                                $real_related_model = Bakery::get_model_by_namespaced_classname($this->data->$morphable_type);
                                $related_entry_id   = $this->data->$morphable_id;
                                if( $morphable_id != 0 and $real_related_model->has_entries() )
                                {
                                    foreach( $real_related_model->get_entries() as $possible_related_entry )
                                    {
                                        if( $possible_related_entry->get_data()->id == $related_entry_id )
                                        {
                                            return "<a href='".route('bakery.read', ['model_slug' => $real_related_model->get_slug(), 'identifier' => $related_entry_id])."'>".$possible_related_entry->get_data()->$title_field."</a>";
                                        }
                                    }
                                }
                            break;
                        }
                    }
                }

                # If all else failed
                return "<i>No relative found</i>";
            }

        }
        # If we indeed found an attribute
        else
        {
            # Grab the corresponding value of the attribute
            $value = $this->get_data()->$attribute_name;

            # Check if this attribute is somehow involved in the model's relations
            if( $attribute->is_relational() )
            {
                # Find the relation this foreign key corresponds to
                $relation = $this->get_relation_for_foreign_key($attribute_name);
                # Find out which method was called on the model to define this relationship
                $method = $relation->get_method_name();
                # Find out which field we should consider the title field of the related model
                $title_field = $relation->get_related_class()->get_title_field();
                # If no relationship was set
                if( $value == 0 )
                {
                    return "<i>No related record</i>";
                }
                # If a relationship has been set
                else
                {
                    switch( $relation->get_type() )
                    {
                        case "BelongsTo":

                            $relatedModel = Bakery::get_model_by_namespaced_classname(get_class($relation->get_instance()->getRelated()));
                            $foreignKey   = $relation->get_instance()->getForeignKey();
                            $otherKey     = $relation->get_instance()->getOtherKey();

                            if( $relatedModel->has_entries() )
                            {
                                foreach( $relatedModel->get_entries() as $related_entry )
                                {
                                    if( $related_entry->get_data()->$otherKey == $this->data->$foreignKey )
                                    {
                                        $title = $related_entry->get_data()->$title_field;
                                        return "<a href='".route('bakery.read', ['model_slug' => $relatedModel->get_slug(), 'identifier' => $value])."'>".$title."</a>";
                                    }
                                }
                            }
                            else
                            {
                                return "<i>Broken relationship</i>";
                            }

                        break;
                        default:
                            dd("Unsupported relationship in BakeryEntry::get_value() - ".$relation->get_type()." - Contact Nick");
                        break;
                    }
                }
            }
            # If this is a regular attribute
            else
            {
                # Switch between types since some get special treatment
                switch( $attribute->get_type() )
                {
                    case 'boolean':
                        return $value ? 'Yes' : 'No';
                    break;
                    default:
                        if( $value == "" )
                        {
                            return "<i>empty</i>";
                        }
                        else
                        {
                            return $value = $limit > 0 ? str_limit($value, $limit, "...") : $value;
                        }
                    break;
                }
            }
        }
    }

    public function get_title()
    {
        $title_field = $this->model->get_title_field();
        return $this->data->$title_field;
    }

    #
    # Private functions
    #

    private function get_relation_for_foreign_key($attribute_name)
    {
        if( $this->model->has_relations() )
        {
            foreach( $this->model->get_relations() as $relation )
            {
                switch( $relation->get_type() )
                {
                    case "HasOne":
                    case "BelongsTo":
                        if( $relation->get_instance()->getForeignKey() == $attribute_name ) return $relation;
                    break;
                }
            }
        }
        return false;
    }

}
