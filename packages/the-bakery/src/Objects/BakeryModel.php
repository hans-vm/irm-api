<?php

namespace Verheijen\Bakery\Objects;

use DB;
use Bakery;
use ReflectionClass;
use ReflectionMethod;
use Illuminate\Support\Collection;

class BakeryModel {

    # Array
    private $relation_classes = [
        'Illuminate\Database\Eloquent\Relations\BelongsTo',
        'Illuminate\Database\Eloquent\Relations\BelongsToMany',
        'Illuminate\Database\Eloquent\Relations\HasMany',
        'Illuminate\Database\Eloquent\Relations\HasManyThrough',
        'Illuminate\Database\Eloquent\Relations\HasOne',
        'Illuminate\Database\Eloquent\Relations\MorphMany',
        'Illuminate\Database\Eloquent\Relations\MorphOne',
        'Illuminate\Database\Eloquent\Relations\MorphTo',
        'Illuminate\Database\Eloquent\Relations\MorphToMany',
        'Illuminate\Database\Eloquent\Relations\MorphedByMany'
    ];

    # String
    private $namespaced_classname;

    # Eloquent\Model
    private $instance;

    # Collection
    private $entries;

    # Collection
    private $relations;

    # Boolean
    private $use_caching;

    # Integer
    private $cache_for;

    /*
     | Constructor
     |
     | @param           String              The namespaced classname of the model we want to load
     | @return          void
    */
    public function __construct($namespaced_classname)
    {
        # Save the namespaced classname
        $this->namespaced_classname = $namespaced_classname;

        # Call that namespaced classname to get a fresh instance of the model we're analyzing
        $this->instance = new $namespaced_classname();

        # Pre-load all the entries of this model
        # $this->entries = $this->load_entries();
        $this->entries = $this->load_entries();

        # Pre-load all the relationships of this model
        $this->relations = $this->load_relations();
    }

    #
    #
    # General information about the model
    #
    #

    public function get_namespace()
    {
        $namespace  = "";
        $parts      = explode("\\", $this->namespaced_classname);
        $last_part  = count($parts) - 1;
        for( $i = 0; $i < count($parts); $i++ )
        {
            if( $i != $last_part )
            {
                if( $namespace != "" ) $namespace .= "\\";
                $namespace .= $parts[$i];
            }
        }
        return $namespace;
    }

    public function get_classname()
    {
        $parts = explode("\\", $this->namespaced_classname);
        $last_part = count($parts) - 1;
        return $parts[$last_part];
    }

    public function get_namespaced_classname()
    {
        return $this->namespaced_classname;
    }

    public function get_slug()
    {
        $slug  = "";
        foreach( explode("\\", $this->namespaced_classname) as $part ){
            $thePart = "";
            $pieces = preg_split('/(?=[A-Z])/', $part);
            $useablePieces = [];
            foreach( $pieces as $piece ){
                if( $piece != "" ){
                    $useablePieces[] = strtolower($piece);
                }
            }
            foreach( $useablePieces as $useablePiece ){
                if( $thePart != "" ) $thePart .= "-";
                $thePart .= $useablePiece;
            }
            if( $slug != "" ) $slug .= "--";
            $slug .= $thePart;
        }
        return $slug;
    }

    public function get_instance()
    {
        return $this->instance;
    }

    #
    #
    # Decorator functions for displaying the data we've gathered nicely in our views
    #
    #

    public function get_menu_title()
    {
        $parts = preg_split("/(?=[A-Z])/", $this->get_classname());
        $last  = count($parts) - 1;
        $output = "";
        for( $i = 0; $i < count($parts); $i++ )
        {
            if( $output != "" ) $output .= " ";
            $output .= $i == $last ? str_plural($parts[$i]) : $parts[$i];
        }
        return $output;
    }

    public function get_browse_title()
    {
        $parts = preg_split("/(?=[A-Z])/", $this->get_classname());
        $last  = count($parts) - 1;
        $out   = "";
        for($i = 0; $i < count($parts); $i++)
        {
            if( $out != "" ) $out .= " ";
            $out .= $i == $last ? strtolower(str_plural($parts[$i])) : strtolower($parts[$i]);
        }
        return $out;
    }

    public function get_read_pre_title()
    {
        $classname    = strtolower($this->get_classname());
        $first_letter = substr($classname, 0, 1);
        $vowels       = ['a', 'e', 'o', 'u', 'i'];
        return in_array($first_letter, $vowels) ? 'an' : 'a';
    }

    public function get_read_title()
    {
        $parts = preg_split("/(?=[A-Z])/", $this->get_classname());
        $out   = "";
        for($i = 0; $i < count($parts); $i++)
        {
            if( $out != "" ) $out .= " ";
            $out .= strtolower($parts[$i]);
        }
        return $out;
    }

    public function get_add_title()
    {
        $parts = preg_split("/(?=[A-Z])/", $this->get_classname());
        $out   = "";
        for($i = 0; $i < count($parts); $i++)
        {
            if( $out != "" ) $out .= " ";
            $out .= strtolower($parts[$i]);
        }
        return $out;
    }

    public function get_edit_title()
    {
        $parts = preg_split("/(?=[A-Z])/", $this->get_classname());
        $out   = "";
        for($i = 0; $i < count($parts); $i++)
        {
            if( $out != "" ) $out .= " ";
            $out .= strtolower($parts[$i]);
        }
        return $out;
    }

    public function get_delete_title()
    {
        $parts = preg_split("/(?=[A-Z])/", $this->get_classname());
        $last  = count($parts) - 1;
        $out   = "";
        for($i = 0; $i < count($parts); $i++)
        {
            if( $out != "" ) $out .= " ";
            $out .= $i == $last ? strtolower(str_plural($parts[$i])) : strtolower($parts[$i]);
        }
        return $out;
    }

    #
    #
    # Attribute related functions
    #
    #

    public function get_attributes()
    {
        $attributes = new Collection;
        $table      = $this->instance->getTable();
        $columns    = DB::connection()->getDoctrineSchemaManager()->listTableColumns($table);

        if( count($columns) > 0 )
        {
            foreach( $columns as $name => $column )
            {
                $attributes->push(new BakeryAttribute($this, str_replace("`", "", $name), $column));
            }
        }

        return $attributes;
    }

    public function get_attribute($attribute_name)
    {
        $attributes = $this->get_attributes();
        foreach( $attributes as $attribute )
        {
            if( $attribute->get_name() == $attribute_name ) return $attribute;
        }
        return false;
    }

    public function get_overview_attributes()
    {
        $overview_attributes = new Collection;

        # Merge the global and any (if were set) specific exclusions for this model
        $exclusions = array_merge(config('bakery.browse_exclude_attributes'), $this->user_set_browse_exclusions());

        # Special exclusions
        # We need to check if this model has any MorphTo relationships. These bring about 2 fields with them we dont want to display ***_type and ***_id
        # Instead of these 2 fields we want a direct link to the read page of the given relative, so along with this check there is another one to add the attribute we want to display
        if( $this->has_relations() )
        {
            foreach( $this->get_relations() as $relation )
            {
                if( $relation->get_type() == "MorphTo" )
                {
                    $exclusions = array_merge($exclusions, [
                        $relation->get_instance()->getForeignKey(),
                        $relation->get_instance()->getMorphType()
                    ]);
                }
            }
        }

        # Grab the maximum amount of attributes we should be displaying (and thus adding to the collection of attributes), can be set to 0
        $max_attributes = config('bakery.browse_max_attributes');

        # User set configuration on the model
        $user_browse_attributes = $this->user_set_browse_attributes();

        # All the attributes we get to pick and choose from
        $attributes = $this->get_attributes();

        # Make sure there are attributs to work with
        if( $attributes->count() > 0 )
        {
            # Counter for all of the added attributes
            $added_attributes = 0;

            # If the developer has explicitely told us which attributes to display
            if( $user_browse_attributes )
            {
                foreach( $attributes as $attribute )
                {
                    # We simply select and add those attributes without any other checks
                    if( in_array($attribute->get_name(), $user_browse_attributes) ) $overview_attributes->push($attribute);
                }
            }
            # If the developer has explicitely told us which attributes NOT to display
            # Or if no configuration has been set
            else
            {
                foreach( $attributes as $attribute )
                {
                    # If a max amount of attributes to display has been set we need to make sure we're not exceeding that treshold
                    # We also need to make sure that the attribute was not added to the global or model specific exclusions
                    if( $max_attributes > 0 and $added_attributes < $max_attributes and ! in_array($attribute->get_name(), $exclusions) )
                    {
                        $overview_attributes->push($attribute);
                        $added_attributes++;
                    }
                }
            }
        }

        # Special attributes
        # As we mentioned before we might have instances in which we want to add some extra (custom) attributes. When we are displaying a model with
        # a MorphTo relationship we are excluding the ***_type and ***_id fields but we want to display a direct link to whatever is being linked to the entry
        if( $this->has_relations() )
        {
            foreach( $this->get_relations() as $relation )
            {
                if( $relation->get_type() == "MorphTo" )
                {
                    $morphable_type = $relation->get_instance()->getMorphType();
                    $flag           = explode("_", $morphable_type)[0];
                    $overview_attributes->push(new BakeryAttribute($this, $flag));
                }
            }
        }

        # Sort the attributes
        $overview_attributes = $this->perform_browse_sorting($overview_attributes);

        # Return the attributes we have selected
        return $overview_attributes;
    }

    private function perform_browse_sorting(Collection $collection)
    {
        $plucked_items = new Collection;
        foreach( $collection as $key => $data )
        {
            if( $data->get_name() == "created_at" or $data->get_name() == "updated_at" ) $plucked_items->push($collection->pull($key));
        }
        if( $plucked_items->count() > 0 )
        {
            foreach( $plucked_items as $plucked_item ) $collection->push($plucked_item);
        }
        return $collection;
    }

    public function get_read_attributes(BakeryEntry $entry)
    {
        $read_attributes = new Collection;

        # Grab the global & user set attribute exclusions
        $exclusions = array_merge(config('bakery.read_exclude_attributes'), $this->user_set_read_exclusions());

        # Special exclusions
        if( $this->has_relations() )
        {
            foreach( $this->relations as $relation )
            {
                if( $relation->get_type() == "MorphTo" )
                {
                    $exclusions = array_merge($exclusions, [
                        $relation->get_instance()->getForeignKey(),
                        $relation->get_instance()->getMorphType()
                    ]);
                }
            }
        }

        # User set read attributes
        $user_set_read_attributes = $this->user_set_read_attributes();

        # Grab all the attributes that are available to us
        $attributes = $this->get_attributes();

        # Regular attributes
        if( $attributes->count() > 0 )
        {
            # If the dev has explicitely told us which attributes to display
            if( $user_set_read_attributes )
            {
                foreach( $attributes as $attribute )
                {
                    if( in_array($attribute->get_name(), $user_set_read_attributes) )
                    {
                        $read_attributes->push([
                            'name'  => $attribute->get_name(true),
                            'value' => $this->get_read_attribute_value($entry, $attribute)
                        ]);
                    }
                }
            }
            # If the dev hasn't told us anything, we take the exclusions into account and display every attribute we can find
            else
            {
                foreach( $attributes as $attribute )
                {
                    if( ! in_array($attribute->get_name(), $exclusions) )
                    {
                        $read_attributes->push([
                            'name'  => $this->strip_id($attribute->get_name(true)),
                            'value' => $this->get_read_attribute_value($entry, $attribute)
                        ]);
                    }
                }
            }
        }

        # Check this model for relations which should be displayed on the read page's attribute table
        if( $this->has_relations() )
        {
            # Loop through all of the relations
            foreach( $this->relations as $relation )
            {
                $related_model = $relation->get_related_class();
                $title_field   = $related_model->get_title_field();

                switch( $relation->get_type() )
                {
                    # Has One should be displayed as if it was an attribute
                    case "HasOne":

                        $value      = null;
                        $foreignKey = $this->strip_foreign_key_table($relation->get_instance()->getForeignKey());
                        if( $related_model->has_entries() )
                        {
                            foreach( $related_model->get_entries() as $possible_related_entry )
                            {
                                if( $possible_related_entry->get_data()->$foreignKey == $entry->get_data()->id )
                                {
                                    $link = "<a href='".route('bakery.read', ['model_slug' => $related_model->get_slug(), 'identifier' => $entry->get_id()])."'>".$possible_related_entry->get_data()->$title_field."</a>";
                                    if( is_null($value) )
                                    {
                                        $value = $link;
                                    }
                                    else {
                                        $value += " ".$link;
                                    }
                                }
                            }
                        }

                        $read_attributes->push([
                            'name' => $this->strip_id($relation->get_method_name()),
                            'value' => is_null($value) ? "<i>Missing relationship</i>" : $value
                        ]);

                    break;
                    # BelongsToMany should also be displayed as if it was an attribute
                    case "BelongsToMany":

                        $value         = null;
                        $foreignKey    = $this->strip_foreign_key_table($relation->get_instance()->getForeignKey());
                        $otherKey      = $this->strip_foreign_key_table($relation->get_instance()->getOtherKey());
                        $relationships = Bakery::get_relationship_table($relation->get_instance()->getTable());
                        if( count($relationships) > 0 )
                        {
                            foreach( $relationships as $possible_relation )
                            {
                                if( $possible_relation->$foreignKey == $entry->get_data()->id )
                                {
                                    $related_entry_id = $possible_relation->$otherKey;
                                    foreach( $related_model->get_entries() as $possible_related_entry )
                                    {
                                        if( $possible_related_entry->get_data()->id == $related_entry_id )
                                        {
                                            $link = "<a href='".route('bakery.read', ['model_slug' => $related_model->get_slug(), 'identifier' => $possible_related_entry->get_data()->id])."'>".ucfirst($possible_related_entry->get_data()->$title_field)."</a>";
                                            if( is_null($value) )
                                            {
                                                $value = $link;
                                            }
                                            else {
                                                $value += " ".$link;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $read_attributes->push([
                            'name' => $this->strip_id($relation->get_method_name()),
                            'value' => is_null($value) ? "<i>Missing relationship</i>" : $value
                        ]);

                    break;
                    # Morph to many should also be displayed as if it was an attribute
                    // case "MorphToMany":
                    //
                    //     $value          = null;
                    //     $morphable_id   = $this->strip_foreign_key_table($relation->get_instance()->getForeignKey());
                    //     $morphable_type = $relation->get_instance()->getMorphType();
                    //     $other_key      = $this->strip_foreign_key_table($relation->get_instance()->getOtherKey());
                    //     $relationships  = Bakery::get_relationship_table($relation->get_instance()->getTable());
                    //
                    //     if( count($relationships) > 0 )
                    //     {
                    //         foreach( $relationships as $possible_relation )
                    //         {
                    //             if( $possible_relation->$morphable_type == $this->get_namespaced_classname() and $possible_relation->$morphable_id == $entry->get_data()->id )
                    //             {
                    //                 foreach( $related_model->get_entries() as $possible_related_entry )
                    //                 {
                    //                     if( $possible_related_entry->get_data()->id == $possible_relation->$other_key )
                    //                     {
                    //                         $link = "<a href='".route('bakery.read', ['model_slug' => $related_model->get_slug(), 'identifier' => $possible_related_entry->get_data()->id])."'>".$possible_related_entry->get_data()->$title_field."</a>";
                    //                         if( is_null($value) )
                    //                         {
                    //                             $value = $link;
                    //                         }
                    //                         else {
                    //                             $value += " ".$link;
                    //                         }
                    //                     }
                    //                 }
                    //             }
                    //         }
                    //     }
                    //
                    //     $read_attributes->push([
                    //         'name' => ucfirst($relation->get_method_name()),
                    //         'value' => $value
                    //     ]);
                    //
                    // break;
                    # As we mentioned before we might have instances in which we want to add some extra (custom) attributes. When we are displaying a model with
                    # a MorphTo relationship we are excluding the ***_type and ***_id fields but we want to display a direct link to whatever is being linked to the entry
                    case "MorphTo":

                        $value          = null;
                        $morphable_type = $relation->get_instance()->getMorphType();
                        $morphable_id   = $relation->get_instance()->getForeignKey();

                        $real_related_model = Bakery::get_model_by_namespaced_classname($entry->get_data()->$morphable_type);
                        $related_entry_id   = $entry->get_data()->$morphable_id;
                        if( $morphable_id != 0 and $real_related_model->has_entries() )
                        {
                            foreach( $real_related_model->get_entries() as $possible_related_entry )
                            {
                                if( $possible_related_entry->get_data()->id == $related_entry_id )
                                {
                                    $value = "<a href='".route('bakery.read', ['model_slug' => $real_related_model->get_slug(), 'identifier' => $related_entry_id])."'>".$possible_related_entry->get_data()->$title_field."</a>";
                                }
                            }
                        }

                        $read_attributes->push([
                            'name' => ucfirst(explode("_", $morphable_type)[0]),
                            'value' => is_null($value) ? "<i>Missing relationship</i>" : $value
                        ]);

                    break;
                    # We do not process the BelongsTo relationships since these have attributes set on the model and are therefor already processed
                }
            }
        }

        # Sort the attributes (placing the created_at and updated_at fields at the end of the list)
        $read_attributes = $this->perform_read_sorting($read_attributes);

        # Return our findings
        return $read_attributes;
    }

    private function get_read_attribute_value(BakeryEntry $entry, BakeryAttribute $attribute)
    {
        # Flags
        $is_an_image = false;
        $is_a_file   = false;

        # Grab the field's value
        $value = $entry->get_value($attribute->get_name());

        # Check if the user has set form configuration on the model
        if( isset($this->instance->form_configuration) )
        {
            foreach( $this->instance->form_configuration as $field => $settings )
            {
                if( $field == $attribute->get_name() and array_key_exists("type", $settings) )
                {
                    if( $settings["type"] == "image" )
                    {
                        $is_an_image = true;
                    }
                    elseif( $settings["type"] == "file" )
                    {
                        $is_a_file = true;
                    }
                }
            }
        }

        # If this is an image
        if( $is_an_image )
        {
            return $value != "<i>empty</i>" ? "<i class='fa fa-picture-o mar-r-5'></i><a href='".asset($value)."' target='_blank'>".$value."</a>" : $value;
        }
        # If this is a file
        elseif( $is_a_file )
        {
            return $value != "<i>empty</i>" ? "<i class='fa fa-file-o mar-r-5'></i><a href='".asset($value)."' target='_blank'>".$value."</a>" : $value;
        }
        # If this is a regular value
        else
        {
            return $value;
        }
    }

    private function perform_read_sorting(Collection $collection)
    {
        $plucked_items = new Collection;
        foreach( $collection as $key => $data )
        {
            if( $data['name'] == "Created at" or $data['name'] == "Updated at" ) $plucked_items->push( $collection->pull($key) );
        }
        if( $plucked_items->count() > 0 )
        {
            foreach( $plucked_items as $plucked_item ) $collection->push($plucked_item);
        }
        return $collection;
    }

    public function get_fillable_attributes()
    {
        $fillable_attributes = new Collection;
        $attributes = $this->get_attributes();
        if( $attributes->count() > 0 )
        {
            foreach( $attributes as $attribute )
            {
                if( $attribute->is_fillable() ) $fillable_attributes->push($attribute);
            }
        }
        return $fillable_attributes;
    }

    public function has_fillable_attributes()
    {
        return $this->get_fillable_attributes()->count() > 0;
    }

    public function has_attribute($attribute_name, $check_parts = false)
    {
        $attributes = $this->get_attributes();
        if( $attributes->count() > 0 )
        {
            foreach( $attributes as $attribute )
            {
                if( $check_parts )
                {
                    $parts = explode("_", $attribute->get_name());
                    foreach( $parts as $part )
                    {
                        if( $part == $attribute_name ) return true;
                    }
                }
                else
                {
                    if( $attribute->get_name() == $attribute_name ) return true;
                }
            }
        }
        return false;
    }

    #
    #
    # Functions related to user set variables which could be set on the model and therefor should be accesible through the saved instance
    #
    #

    public function user_set_title_field()
    {
        return isset($this->instance->title_field) ? $this->instance->title_field : false;
    }

    public function user_set_browse_exclusions()
    {
        return isset($this->instance->browse_exclude_attributes) ? $this->instance->browse_exclude_attributes : [];
    }

    public function user_set_browse_attributes()
    {
        return isset($this->instance->browse_attributes) ? $this->instance->browse_attributes : false;
    }

    public function user_set_read_exclusions()
    {
        return isset($this->instance->read_exclude_attributes) ? $this->instance->read_exclude_attributes : [];
    }

    public function user_set_read_attributes()
    {
        return isset($this->instance->read_attributes) ? $this->instance->read_attributes : false;
    }

    #
    #
    # Relationship related functions
    #
    #

    public function load_relations()
    {
        $relations = new Collection;
        # Create a new reflection class to help us analyze the model
        $reflector = new ReflectionClass($this->get_namespaced_classname());
        # Grab all of the methods the reflector has found
        $methods = $reflector->getMethods(ReflectionMethod::IS_PUBLIC);
        # Seperate the inherited methods from the ones we've declared in the class itself
        $methods = $this->get_written_functions($methods);

        # Loop through all of the functions we've written in the class
        if( $methods->count() > 0 )
        {
            foreach( $methods as $method )
            {
                # Save the method's name in a variable so we can use it to call the method later
                $method_name = $method->getName();
                # skip static methods and some specific methods implemented in BaseModel
                if ($method_name == 'getModelFieldValues' || $method->isStatic()) continue;
                # The amount of parameters the method takes
                $num_required_parameters = $method->getNumberOfRequiredParameters();
                # Make sure it takes only zero arguments, since relation methods take none
                if( $num_required_parameters == 0 )
                {
                    # Call the method and save whatever it returns
                    $returned_value = $this->instance->$method_name();
                    # If an object was returned
                    if( is_object($returned_value) )
                    {
                        # Grab the class of the returned object
                        $returned_classname = get_class($returned_value);
                        # If one of laravel's eloquent relationship classes was returned we have a match!
                        if( in_array($returned_classname, $this->relation_classes) )
                        {
                            # Add a new instance of a BakeryRelation object to the relations collection
                            $relations->push(new BakeryRelation($this, $method_name, $returned_value));
                        }
                    }
                }
            }
        }

        # Return all of the relations we've found
        return $relations;
    }

    public function get_relations()
    {
        return $this->relations;
    }

    private function get_written_functions($methods)
    {
        $functions = new Collection;
        foreach( $methods as $method )
        {
            if( $method->getDeclaringClass()->getName() == $this->get_namespaced_classname() ) $functions->push($method);
        }
        return $functions;
    }

    public function has_relations()
    {
        return $this->relations->count() > 0;
    }

    public function get_browseable_relations()
    {
        $browseable_relations = new Collection;
        $browseable_types     = ["HasMany", "MorphMany", "HasManyThrough", "MorphToMany"];
        if( $this->has_relations() )
        {
            foreach( $this->relations as $relation )
            {
                if( in_array($relation->get_type(), $browseable_types) ) $browseable_relations->push($relation);
            }
        }
        return $browseable_relations;
    }

    public function has_browseable_relations()
    {
        return $this->get_browseable_relations()->count() > 0;
    }

    #
    #
    # Entry related functions
    #
    #

    private function possibleRememberableClasses()
    {
        $output = [$this->get_namespaced_classname()];

        $parents = class_parents($this->get_namespaced_classname());
        if( count($parents) > 0 )
        {
            foreach( $parents as $key => $parent )
            {
                $output[] = $parent;
            }
        }

        return $output;
    }

    public function load_entries()
    {
        $entries = new Collection;

        $everything = $this->instance->all();
        if( $everything->count() > 0 )
        {
            foreach( $everything as $record )
            {
                $entries->push(new BakeryEntry($this, $record));
            }
        }

        return $entries;
    }

    public function get_entries()
    {
        return $this->entries;
    }

    public function has_entries()
    {
        return $this->entries->count() > 0;
    }

    public function get_entry($id)
    {
        $record = $this->instance->find($id);
        if (!$record) return false;
        
        return new BakeryEntry($this, $record);
    }

    public function get_title_field()
    {
        $user_set_title_field = $this->user_set_title_field();

        # If the developer has set a title field we simply return that
        if( $user_set_title_field ) return $user_set_title_field;

        # Check if this model has any of the preferred attributes
        $preferred_attribute = $this->find_preferred_title_attributes(["title", "name"]);

        # If we found a preferred attribute we return that
        if( $preferred_attribute ) return $preferred_attribute;

        # If we've reached this point we have no clue which attribute to pick so we pick the first string we find
        $attributes = $this->get_attributes();
        if( $attributes->count() > 0 )
        {
            foreach( $attributes as $attribute )
            {
                if( $attribute->get_type() == "string" ) return $attribute->get_name();
            }
        }

        # If we could not even find a string we just return the ID which all models should have
        return "id";
    }

    private function find_preferred_title_attributes($preferred_attributes)
    {
        $attributes = $this->get_attributes();
        if( $attributes->count() > 0 )
        {
            foreach( $attributes as $attribute )
            {
                $parts = explode("_", $attribute->get_name());
                foreach( $parts as $part )
                {
                    if( in_array($part, $preferred_attributes) ) return $attribute->get_name();
                }
            }
        }
        return false;
    }

    private function prettify($raw_text)
    {
        $out   = "";
        $text  = str_replace("_", " ", $raw_text);
        $parts = preg_split("/(?=[A-Z])/", $text);
        $last  = count($parts) - 1;
        for( $i = 0; $i < count($parts); $i++ )
        {
            if( $out == "" ) $out .= " ";
            $word = strtolower($parts[$i]);
            if( $i == 0 ) $word = ucfirst($word);
            if( $i == $last ) $word = str_plural($word);
            $out .= $word;
        }
        return $out;
    }

    private function strip_id($raw_text)
    {
        $parts = explode("_", $raw_text);
        $out = "";
        foreach( $parts as $part )
        {
            if( $part != "id" )
            {
                if( $out != "" ) $out .= " ";
                $out .= $part;
            }
        }
        return ucfirst($out);
    }

    private function strip_foreign_key_table($raw)
    {
        return explode(".", $raw)[1];
    }
}
