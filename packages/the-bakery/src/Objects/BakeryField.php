<?php

namespace Verheijen\Bakery\Objects;

class BakeryField {

    private $name;
    private $value;
    private $options;

    /*
     | Constructor
     |
     | @param       string          $name of the field (attribute name)
     | @param       string          $value of the field
     | @param       array           $options are optional, since some fields require extra data Fields
     | @return      void
    */
    public function __construct($name, $value = null, $options = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->options = $options;
    }

    public function get_name()
    {
        return $this->name;
    }

    public function get_value()
    {
        return $this->value;
    }

    public function get_options()
    {
        return $this->options;
    }

    public function get_title()
    {
        if( is_array($this->options) and array_key_exists('title', $this->options) )
        {
            return $this->options['title'];
        }
        else
        {
            $out = "";
            $parts = explode("_", $this->name);
            foreach($parts as $part)
            {
                if( $part != "id" )
                {
                    if( $out != "" ) $out .= " ";
                    $out .= $part;
                }
            }
            return ucfirst($out).":";
        }
    }

    public function get_description()
    {
        return is_array($this->options) and array_key_exists('description', $this->options) ? $this->options['description'] : false;
    }

    public function get_select_options()
    {
        if( is_array($this->options) and array_key_exists('select_options', $this->options) )
        {
            return $this->options['select_options'];
        }
        else
        {
            return false;
        }
    }

    public function get_prepend_addon()
    {
        return is_array($this->options) and array_key_exists('prepend_addon', $this->options) ? $this->options['prepend_addon'] : false;
    }

    public function get_append_addon()
    {
        return is_array($this->options) and array_key_exists('append_addon', $this->options) ? $this->options['append_addon'] : false;
    }

}
