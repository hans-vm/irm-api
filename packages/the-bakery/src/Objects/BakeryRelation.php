<?php

namespace Verheijen\Bakery\Objects;

use Bakery;
use Illuminate\Support\Collection;

class BakeryRelation {

    # @var Eloquent\Model - The original model this relationship was called from
    private $model;

    # @var String - The name of the method that was used to create this relationship
    private $method_name;

    # @var Relation Class - Instance of the relationship class that describes this relationship
    private $instance;

    /*
     | Constructor
     |
     | @param           BakeryModel             $model the relationship we're loading is set on
     | @param           String                  $method that has been set on the $model to define the relationship
     | @param           Eloquent\Model          $related model's class
     | @return          void
    */
    public function __construct(BakeryModel $model, $method_name, $related_class)
    {
        $this->model = $model;
        $this->method_name = $method_name;
        $this->instance = $related_class;
    }

    #
    # Getters
    #

    public function get_model()
    {
        return $this->model;
    }

    /*
     | Get method name
     |
     | @param       Boolean         Do we want to display a prettier version of the raw method name? Splits camelcase string and capitalizes the first letter of the final string we're returning
     | @return      String
    */
    public function get_method_name($pretty = false)
    {
        if( $pretty )
        {
            $parts = preg_split("/(?=[A-Z])/", $this->get_method_name());
            $last  = count($parts) - 1;
            $out   = "";
            for($i = 0; $i < count($parts); $i++)
            {
                if( $out != "" ) $out .= " ";
                if( $i == 0 )
                {
                    $out .= ucfirst($parts[$i]);
                }
                elseif( $i == $last )
                {
                    $out .= strtolower(str_plural($parts[$i]));
                }
                else
                {
                    $out .= strtolower($parts[$i]);
                }
            }
            $out = "<strong>".$out."</strong>";
            if( $this->is_has_many_through_relationship() )
            {
                $through_classname = strtolower(str_plural($this->strip_relation_classname(get_class($this->instance->getParent()))));
                $out .= " through <strong>".$through_classname."</strong>";
            }
            return $out;
        }
        return $this->method_name;
    }

    /*
     | Get instance
     |
     | @return      Eloquent\Relations\Object
    */
    public function get_instance()
    {
        return $this->instance;
    }

    /*
     | Get relationship type
     |
     | @return      String
    */
    public function get_type()
    {
        $classname_parts = explode("\\", get_class($this->instance));
        $last_part = count($classname_parts) - 1;
        return $classname_parts[$last_part];
    }

    /*
     | Grab a BakeryModel instance of the parent class
     |
     | @return      BakeryModel
    */
    public function get_parent_class()
    {
        return Bakery::get_model_by_namespaced_classname(get_class($this->instance->getParent()));
    }

    /*
     | Get the name of the related class
     |
     | @return      String
    */
    public function get_related_classname()
    {
        return get_class($this->instance->getRelated());
    }

    /*
     | Get a BakeryModel instance of the related class
     |
     | @return      BakeryModel
    */
    public function get_related_class()
    {
        return Bakery::get_model_by_namespaced_classname($this->get_related_classname());
    }

    /*
     | Get related records
     |
     | @param       BakeryEntry         entry we want to get the related records off
     | @return      Collection
    */
    public function get_related_records(BakeryEntry $entry)
    {
        $related_model = $this->get_related_class();
        $records = new Collection;

        switch( $this->get_type() )
        {
            case "HasMany":
                $foreignKey = $this->instance->getPlainForeignKey();
                foreach( $related_model->get_entries() as $possible_related_entry )
                {
                    if( $possible_related_entry->get_data()->$foreignKey == $entry->get_data()->id )
                    {
                        $records->push($possible_related_entry);
                    }
                }
            break;
            case "MorphToMany":
                $foreignKey = $this->strip_foreign_key_table($this->instance->getForeignKey());
                foreach( $related_model->get_entries() as $possible_related_entry )
                {
                    if( $possible_related_entry->get_data()->$foreignKey == $entry->get_data()->id )
                    {
                        $records->push($possible_related_entry);
                    }
                }
            break;
            default:
                dd("Unsupported relationship type in BakeryRelation::get_related_records - ".$this->get_type()." - contact Nick");
            break;
        }

        return $records;
    }

    /*
     | Has related records
     |
     | @param       BakeryEntry         entry we are checking for related records
     | @return      Boolean
    */
    public function has_related_records(BakeryEntry $entry)
    {
        return $this->get_related_records($entry)->count() > 0;
    }

    /*
     | Is has many through relationship
     |
     | @return      Boolean
    */
    private function is_has_many_through_relationship()
    {
        return get_class($this->instance) == "Illuminate\Database\Eloquent\Relations\HasManyThrough";
    }

    /*
     | Strip relation classname
     |
     |
     | @param       String          Fully namespaced classname which is stripped down to the classname
     | @return      String
    */
    private function strip_relation_classname($classname)
    {
        $parts = explode("\\", $classname);
        $last  = count($parts) - 1;
        return $parts[$last];
    }

    private function strip_foreign_key_table($raw)
    {
        return explode(".", $raw)[1];
    }

}
