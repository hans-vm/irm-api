<?php

namespace Verheijen\Bakery\Objects;

use Doctrine\DBAL\Schema\Column;

class BakeryAttribute {

    # Basic information we created this object with
    private $name;
    private $model;
    private $column;

    # Extracted information about our attribute
    private $type;
    private $length;
    private $default;
    private $fillable;
    private $unsigned;
    private $auto_increment;

    /*
     | Constructor
     |
     | @param           BakeryModel             $model of the attribute we're loading
     | @param           String                  $name of the attribute we're loading
     | @param           Column                  $column data, if null this means we've manually called the function
     | @return          void
    */
    public function __construct(BakeryModel $model, $attribute_name, Column $column = null)
    {
        # Save all the given parameters
        $this->model  = $model;
        $this->name   = $attribute_name;
        $this->column = $column;

        # Gather information we need
        if( ! is_null($column) )
        {
            $this->type = $this->column->getType()->getName();
            $this->length = $this->column->getLength();
            $this->default = $this->column->getDefault();
            $this->fillable = $this->determine_if_fillable();
            $this->unsigned = $this->column->getUnsigned();
            $this->auto_increment = $this->column->getAutoincrement();
        }
        # If no column data was given that means we are creating custom attributes which we want to (for instance) display on the read page
        # So we set what we can and leave the rest on null
        else
        {
            $this->type = "string";
            $this->length = 255;
            $this->default = null;
            $this->fillable = false;
            $this->unsigned = false;
            $this->auto_increment = false;
        }
    }

    #
    # Private functions
    #

    public function determine_if_fillable()
    {
        return in_array($this->name, $this->model->get_instance()->getFillable());
    }

    #
    # Getters
    #

    public function get_model()
    {
        return $this->model;
    }

    public function get_name($pretty = false)
    {
        if( $pretty )
        {
            if( $this->name == "id" )
            {
                return "#";
            }
            else
            {
                $output = "";
                $parts = explode("_", $this->name);
                foreach($parts as $part)
                {
                    if( $part != "id" )
                    {
                        if( $output != "" ) $output .= " ";
                        $output .= $part;
                    }
                }
                return ucfirst($output);
            }
        }
        else
        {
            return $this->name;
        }
    }

    public function get_column()
    {
        return $this->column;
    }

    public function get_type()
    {
        return $this->type;
    }

    public function get_length()
    {
        return $this->length;
    }

    public function get_default()
    {
        return $this->default;
    }

    public function get_unsigned()
    {
        return $this->unsigned;
    }

    public function get_auto_increment()
    {
        return $this->auto_increment;
    }

    public function get_relation()
    {
        if( $this->model->has_relations() )
        {
            foreach( $this->model->get_relations() as $relation )
            {
                switch( $relation->get_type() )
                {
                    case "HasOne":
                    case "BelongsTo":
                        if( $relation->get_instance()->getForeignKey() == $this->name ) return $relation;
                    break;
                }
            }
        }
    }

    #
    # Checks
    #

    public function is_fillable()
    {
        return $this->fillable;
    }

    private function looks_like_a_foreign_key()
    {
        $looks_the_part = false;
        # Check if it looks like a foreign key for the belongsTo and hasOne relationship types
        $name_parts = explode("_", $this->name);
        # If it is a foreign key it will be at least a 2 part attribute name since all foreign keys end with _id
        if( count($name_parts) >= 2 )
        {
            # Grab the index of the last part
            $last = count($name_parts) - 1;
            # And if it ends with id we flag it
            if( $name_parts[$last] == "id" ) $looks_the_part = true;
        }
        return $looks_the_part;
    }

    public function is_relational()
    {
        # If this attribute looks like a foreign key
        if( $this->looks_like_a_foreign_key() )
        {
            # Make sure our model has relations to begin with
            if( $this->model->has_relations() )
            {
                # Loop through each of them
                foreach( $this->model->get_relations() as $relation )
                {
                    # Switch between types of relations and pick out the ones we support (or rather which could be linked to an attribute on this model)
                    switch( $relation->get_type() )
                    {
                        case "BelongsTo":
                            # Check if the foreign key of the relationship object matches the attribute we're processing
                            if( $relation->get_instance()->getForeignKey() == $this->name ) return true;
                        break;
                    }
                }
            }
        }

        # If we've reached this point the attribute is in the clear
        return false;
    }

}
