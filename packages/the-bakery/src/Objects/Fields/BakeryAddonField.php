<?php

namespace Verheijen\Bakery\Objects\Fields;

use Verheijen\Bakery\Objects\BakeryField;
use Verheijen\Bakery\Contracts\BakeryFieldContract;

class BakeryAddonField extends BakeryField implements BakeryFieldContract {

    public function render()
    {
        return view("bakery::fields.addon", [
            "type"          => "text",
            "title"         => $this->get_title(),
            "description"   => $this->get_description(),
            "name"          => $this->get_name(),
            "value"         => $this->get_value(),
            "options"       => $this->get_options(),
            "prepend_addon" => $this->get_prepend_addon(),
            "append_addon"  => $this->get_append_addon(),
        ])->render();
    }

}
