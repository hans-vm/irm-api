<?php

namespace Verheijen\Bakery\Objects\Fields;

use Verheijen\Bakery\Objects\BakeryField;
use Verheijen\Bakery\Contracts\BakeryFieldContract;

class BakerySelectField extends BakeryField implements BakeryFieldContract {

    public function render()
    {
        // dd($this->get_select_options());
        return view("bakery::fields.select", [
            "type"           => "text",
            "title"          => $this->get_title(),
            "description"    => $this->get_description(),
            "name"           => $this->get_name(),
            "value"          => $this->get_value(),
            "options"        => $this->get_options(),
            "select_options" => $this->get_select_options()
        ])->render();
    }

}
