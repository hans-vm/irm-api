<?php

namespace Verheijen\Bakery\Objects\Fields;

use Verheijen\Bakery\Objects\BakeryFormField;

class BakeryBelongsToField extends BakeryFormField {
    
    public function render()
    {
        return view("bakery::fields.select-field", [
            "title" => $this->getTitle(),
            "name" => $this->getName(),
            "value" => $this->getValue(),
            "disabled" => isset($this->getOptions()["disabled"]) ? $this->getOptions()["disabled"] : false,
            "options" => $this->getOptions()["options"]
        ]);
    }
    
}