<?php

namespace Verheijen\Bakery\Objects\Fields;

use Verheijen\Bakery\Objects\BakeryField;
use Verheijen\Bakery\Contracts\BakeryFieldContract;

class BakeryEmailField extends BakeryField implements BakeryFieldContract {

    public function render()
    {
        return view("bakery::fields.email", [
            "title"         => $this->get_title(),
            "description"   => $this->get_description(),
            "name"          => $this->get_name(),
            "value"         => $this->get_value(),
            "options"       => $this->get_options()
        ])->render();
    }

}
