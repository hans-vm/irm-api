<?php

namespace Verheijen\Bakery\Objects\Fields;

use Verheijen\Bakery\Objects\BakeryFormField;
use Verheijen\Bakery\Contracts\BakeryFieldContract;

class BakeryFileUploadField extends BakeryFormField implements BakeryFieldContract {

    public function render()
    {
        return view("bakery::fields.file", [
            "title" => $this->get_title(),
            "description" => $this->get_description(),
            "name"  => $this->get_name(),
            "value" => $this->get_value(),
            "options" => $this->get_options()
        ]);
    }

}
