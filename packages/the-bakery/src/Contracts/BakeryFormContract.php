<?php

namespace Verheijen\Bakery\Contracts;

interface BakeryFormContract {

    public function get_model();
    public function get_fields();
    public function render();

}
