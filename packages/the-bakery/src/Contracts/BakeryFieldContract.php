<?php

namespace Verheijen\Bakery\Contracts;

use Verheijen\Bakery\Objects\BakeryModel;
use Verheijen\Bakery\Objects\BakeryEntry;

interface BakeryFieldContract {

    public function __construct($name, $value = null, $options = null);
    public function render();

}
