<?php

Route::group(['prefix' => 'bakery', 'middleware' => ['web']], function(){

    /*
     |
     | Dashboard
     |
    */

    Route::get('dashboard', ['as' => 'bakery.dashboard', 'uses' => 'Verheijen\Bakery\Controllers\BakeryController@dashboard']);

    /*
     |
     | Export as ...
     |
    */

    Route::get('export-as',             ['as' => 'bakery.export',       'uses' => 'Verheijen\Bakery\Controllers\BakeryController@export']);
    Route::post('export-as/{format}',   ['as' => 'bakery.export.as',    'uses' => 'Verheijen\Bakery\Controllers\BakeryController@exportPost']);
    
    /*
     |
     | B.R.E.A.D Routes
     |
    */

    Route::get('browse/{model_slug}',                   ['as' => 'bakery.browse',               'uses' => 'Verheijen\Bakery\Controllers\BakeryController@browse']);
    Route::get('read/{model_slug}/{identifier}',        ['as' => 'bakery.read',                 'uses' => 'Verheijen\Bakery\Controllers\BakeryController@read']);
    Route::get('edit/{model_slug}/{identifier}',        ['as' => 'bakery.edit',                 'uses' => 'Verheijen\Bakery\Controllers\BakeryController@edit']);
    Route::post('edit/{model_slug}/{identifier}',       ['as' => 'bakery.edit.post',            'uses' => 'Verheijen\Bakery\Controllers\BakeryController@editPost']);
    Route::get('add/{model_slug}',                      ['as' => 'bakery.add',                  'uses' => 'Verheijen\Bakery\Controllers\BakeryController@add']);
    Route::post('add/{model_slug}',                     ['as' => 'bakery.add.post',             'uses' => 'Verheijen\Bakery\Controllers\BakeryController@addPost']);
    Route::get('delete/{model_slug}/{identifier}',      ['as' => 'bakery.delete',               'uses' => 'Verheijen\Bakery\Controllers\BakeryController@delete']);
    Route::post('delete/{model_slug}/{identifier}',     ['as' => 'bakery.delete.post',          'uses' => 'Verheijen\Bakery\Controllers\BakeryController@deletePost']);

});
