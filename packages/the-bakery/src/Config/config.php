<?php

return [

    #
    # Authentication
    #

    // What middleware should be called in the controller
    'middleware' => [],

    #
    # Global bakery settings
    #

    // Which template should all our views extend? Set to our default basecamp template by default.
    'template' => 'basecamp::templates.lte',

    // In which folders should we look for models?
    'model_folders' => [
        'app',
    ],

    // What (namespaced) classes should we exclude from being processed?
    'model_exclusions' => [],

    #
    # B.R.E.A.D settings
    #

    // Which fields should always be excluded from the browse page
    'browse_exclude_attributes' => ['id'],

    // Max. amount of attributes to display on the browse page, set it to zero to display all attributes (not recommended, easily becomes a cluttered mess)
    'browse_max_attributes' => '6',

    // Which fields should always be excluded from the read page
    'read_exclude_attributes' => ['id'],

    #
    # Custom field mapping
    #

    // Here you can add your custom field types and map them to the relevant class
    'custom_fields' => [

        // For example:
        'datetime-local' => 'Verheijen\\Bakery\\Objects\\Fields\\CustomDateTimeLocalField'

    ],

    #
    # File uploading settings
    #

    // Default image upload path
    'default_image_upload_path' => 'assets/uploads/images',

    // Default file upload path
    'default_file_upload_path' => 'assets/uploads/files',

];
