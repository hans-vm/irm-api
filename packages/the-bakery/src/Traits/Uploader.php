<?php

namespace Verheijen\Bakery\Traits;

use Image;
use Exception;

trait Uploader {

    /*
     | Upload
     |
     | @param       String          Type of file we'reuploading
     | @param       UploadedFile    The file we've uploaded (directly from request->input()
     | @param       Array           Settings
     |                              accepts:    `upload_path`, `allowed_extensions`, `max_size`, (all types of files)
     |                                          `min_width`, `max_width`, `exact_width`, `min_height`, `max_height`, `exact_height` (images only)
     | @throws      Exception
     | @return      String          Full path to the uploaded file
    */
    public function upload($type, $file, $settings = [])
    {
        # Validate some more if necessary
        switch( $type )
        {
            case "image":
                if( count($settings) > 0 )
                {
                    $image = Image::make($file);
                    foreach( $settings as $setting => $value )
                    {
                        switch( $setting )
                        {
                            case 'min_width':
                                if( $image->width() < $value ) throw new Exception("The image's width is below the minimum of ".$value." pixels.");
                            break;
                            case 'max_width':
                                if( $image->width() > $value ) throw new Exception("The image's width exceeds the maximum of ".$value." pixels.");
                            break;
                            case 'min_height':
                                if( $image->height() < $value ) throw new Exception("The image's height is below the minimum of ".$value." pixels.");
                            break;
                            case 'max_height':
                                if( $image->height() > $value ) throw new Exception("The image's height exceeds the maxmimum of ".$value." pixels.");
                            break;
                            case 'exact_width':
                                if( $image->width() != $value ) throw new Exception("The image must have a width of ".$value." pixels.");
                            break;
                            case 'exact_height':
                                if( $image->height() != $value ) throw new Exception("The image must have a height of ".$value." pixels.");
                            break;
                        }
                    }
                }
            break;
        }

        # Gather some more information
        $random_string  = substr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50), 1).substr(md5(time()), 1);
        $filename       = $random_string . "." . $file->getClientOriginalExtension();
        $real_folder    = base_path()."\..\\".$settings['upload_path'];
        $relative_url   = $settings['upload_path'] . "/" . $filename;
        $absolute_url   = $real_folder . "/" . $filename;

        # Place the file where it belongs
        $file->move($real_folder, $filename);

        // Return the relative url
        return $relative_url;
    }

}
