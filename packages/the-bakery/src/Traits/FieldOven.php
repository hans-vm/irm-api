<?php

namespace Verheijen\Bakery\Traits;

use Verheijen\Bakery\Objects\BakeryEntry;
use Verheijen\Bakery\Objects\BakeryAttribute;

use Verheijen\Bakery\Objects\Fields\BakeryStringField;
use Verheijen\Bakery\Objects\Fields\BakeryNumberField;
use Verheijen\Bakery\Objects\Fields\BakeryTextField;
use Verheijen\Bakery\Objects\Fields\BakeryImageUploadField;
use Verheijen\Bakery\Objects\Fields\BakeryFileUploadField;
use Verheijen\Bakery\Objects\Fields\BakeryDateField;
use Verheijen\Bakery\Objects\Fields\BakeryBooleanField;
use Verheijen\Bakery\Objects\Fields\BakerySelectField;
use Verheijen\Bakery\Objects\Fields\BakeryEmailField;
use Verheijen\Bakery\Objects\Fields\BakeryPasswordField;
use Verheijen\Bakery\Objects\Fields\BakeryAddonField;
use Verheijen\Bakery\Objects\Fields\BakeryIpField;
use Verheijen\Bakery\Objects\Fields\BakeryColorpickerField;
use Verheijen\Bakery\Objects\Fields\BakeryWysihtmlField;
use Verheijen\Bakery\Objects\Fields\BakeryMultipleSelectField;
use Verheijen\Bakery\Objects\Fields\BakeryMorphToField;

trait FieldOven {

    /*
     | Bake a field
     | This functions takes a BakeryAttribute and determines which BakeryField implementation to use in the form we're generating.
     | If you want the field to already have a value the corresponding entry should be passed along also, but this is optional ofcourse.
     |
     | @param       BakeryAttribute             $attribute
     | @param       BakeryEntry                 $entry = optional
     | @param       Array                       User configuration, optional
     | @param       String                      Prefix the name of this field, optional
     | @return      Collection
    */
    public function bake_field(BakeryAttribute $attribute, BakeryEntry $entry = null, $user_configuration = null, $prefix = null)
    {
        # Determine the basic information we need for our field, which we can manipulate later (take user configuration into account for example)
        $field_name    = $attribute->get_name();
        $field_value   = is_null($entry) ? null : $entry->get_data()->$field_name;
        $field_options = [];

        # Prefix it if we need to
        if( !is_null($prefix) )
        {
            $field_name = $prefix . "_" . $field_name;
        }

        # Grab the user set configuration if there is any
        $user_set_configuration = $this->get_user_set_configuration($attribute);
        if( $user_set_configuration and array_key_exists($attribute->get_name(), $user_set_configuration) )
        {
            foreach( $user_set_configuration[$attribute->get_name()] as $key => $val )
            {
                switch( $key )
                {
                    case "type":
                    case "title":
                    case "decorators":
                    case "description":
                    case "select_options":
                    case "append_addon":
                    case "prepend_addon":
                        $field_options[$key] = $val;
                    break;
                }
            }
        }

        # If this attribute indicates a relationship
        if( $attribute->is_relational() )
        {
            # Grab the related foreign model
            $relation = $attribute->get_relation();

            # Switch between the possible relationships we could want to add to the add and edit form
            switch( $relation->get_type() )
            {
                case "BelongsTo":

                    # Gather some valuable intel
                    $title_field    = $relation->get_related_class()->get_title_field();
                    $select_options = [];
                    foreach( $relation->get_related_class()->get_instance()->all() as $related_record )
                    {
                        $select_options[$related_record->id] = $related_record->$title_field;
                    }
                    if( ! array_key_exists('select_options', $field_options) ) $field_options['select_options'] = $select_options;

                    return new BakerySelectField($field_name, $field_value, $field_options);

                break;
            }
        }
        # If we are dealing with a regular attribute
        else
        {
            return $this->bake($this->get_field_type($attribute, $user_set_configuration), $field_name, $field_value, $field_options);
        }

        # If for whatever reason we failed to bake the field
        return false;
    }

    /*
     | Bake a custom field
     |
     | @param       String                  "type" of field we want to bake
     | @param       String                  Name of the field we are baking
     | @param       String                  Value of the field, optional
     | @param       Array                   Options, optional
     | @return      Collection
    */
    public function bake_custom_field($type, $name, $value = null, $options = null)
    {
        return $this->bake($type, $name, $value, $options);
    }

    /*
     | Bake
     | Actually build and return a field object
     |
     | @param       String                  "type" of the field
     | @param       String                  Name of the field
     | @param       String                  Value of the field
     | @param       Array                   Options
     | @return      BakeryField implementation
    */
    private function bake($type, $field_name, $field_value, $field_options)
    {
        # Switch between possible field types and return a new instance of the corresponding BakeryField implementation
        switch( $type )
        {
            case "string":
                return new BakeryStringField($field_name, $field_value, $field_options);
            break;
            case "email":
                return new BakeryEmailField($field_name, $field_value, $field_options);
            break;
            case "image":
                return new BakeryImageUploadField($field_name, $field_value, $field_options);
            break;
            case "file":
                return new BakeryFileUploadField($field_name, $field_value, $field_options);
            break;
            case "number":
                return new BakeryNumberField($field_name, $field_value, $field_options);
            break;
            case "date":
                return new BakeryDateField($field_name, $field_value, $field_options);
            break;
            case "text":
                return new BakeryTextField($field_name, $field_value, $field_options);
            break;
            case "boolean":
                return new BakeryBooleanField($field_name, $field_value, $field_options);
            break;
            case "select":
                return new BakerySelectField($field_name, $field_value, $field_options);
            break;
            case "password":
                return new BakeryPasswordField($field_name, $field_value, $field_options);
            break;
            case "price":
                $field_options["prepend_addon"] = "&euro;";
                return new BakeryAddonField($field_name, $field_value, $field_options);
            break;
            case "ip":
                return new BakeryIpField($field_name, $field_value, $field_options);
            break;
            case "colorpicker":
                return new BakeryColorpickerField($field_name, $field_value, $field_options);
            break;
            case "wysiwyg":
                return new BakeryWysihtmlField($field_name, $field_value, $field_options);
            break;
            case "multiple":
                return new BakeryMultipleSelectField($field_name, $field_value, $field_options);
            break;
            case "morphTo":
                return new BakeryMorphToField($field_name, $field_value, $field_options);
            break;
            // If a type was given that is not natively supported
            default:

                // Check if it has been set as a custom field
                $custom_fields = config("bakery.custom_fields");
                if( is_array($custom_fields) and array_key_exists($type, $custom_fields) )
                {
                    $custom_field = $custom_fields[$type];
                    return new $custom_field($field_name, $field_value, $field_options);
                }

            break;
        }
    }

    /*
     | Get field type
     |
     | @param       BakeryAttribute         The attribute we want to know the field type of
     | @param       Array                   Any configuration the user has set for this field
     | @return      String                  The type of the attribute we are analyzing
    */
    private function get_field_type(BakeryAttribute $attribute, $user_set_configuration)
    {
        if( is_array($user_set_configuration) and array_key_exists($attribute->get_name(), $user_set_configuration) and array_key_exists('type', $user_set_configuration[$attribute->get_name()]) )
        {
            $type = $user_set_configuration[$attribute->get_name()]["type"];
        }
        else
        {
            $type = $attribute->get_type();
        }

        switch( $type )
        {
            case "string":
                if( $this->is_image_field($attribute) ){
                    return "image";
                } elseif( $this->is_file_field($attribute) ){
                    return "file";
                } elseif( $this->is_email_field($attribute) ){
                    return "email";
                } elseif( $this->is_password_field($attribute) ){
                    return "password";
                } elseif( $this->is_color_field($attribute) ){
                    return "colorpicker";
                } else {
                    return $type;
                }
            break;
            case "integer":
            case "float":
                if( $this->is_price_field($attribute) ){
                    return "price";
                } else {
                    return "number";
                }
            break;
            case "date":
            case "datetime":
                return "date";
            break;
            case "text":
            case "boolean":
            default:
                return $type;
            break;
        }
    }

    #
    # Special field detection methods
    #

    /*
     | Is image field
     |
     | @param       BakeryAttribute         The attribute we want to check
     | @return      Boolean
    */
    private function is_image_field(BakeryAttribute $attribute)
    {
        $parts = explode("_", $attribute->get_name());
        foreach( $parts as $part ) if( $part == "image" ) return true;
        return false;
    }

    /*
     | Is file field
     |
     | @param       BakeryAttribute         The attribute we want to check
     | @return      Boolean
    */
    private function is_file_field(BakeryAttribute $attribute)
    {
        $parts = explode("_", $attribute->get_name());
        foreach( $parts as $part ) if( $part == "file" ) return true;
        return false;
    }

    /*
     | Is price field
     |
     | @param       BakeryAttribute         The attribute we want to check
     | @return      Boolean
    */
    private function is_price_field(BakeryAttribute $attribute)
    {
        $parts = explode("_", $attribute->get_name());
        foreach( $parts as $part ) if( $part == "price" ) return true;
        return false;
    }

    /*
     | Is email field
     |
     | @param       BakeryAttribute         The attribute we want to check
     | @return      Boolean
    */
    private function is_email_field(BakeryAttribute $attribute)
    {
        $parts = explode("_", $attribute->get_name());
        foreach( $parts as $part ) if( $part == "email" ) return true;
        return false;
    }

    /*
     | Is password field
     |
     | @param       BakeryAttribute         The attribute we want to check
     | @return      Boolean
    */
    private function is_password_field(BakeryAttribute $attribute)
    {
        $parts = explode("_", $attribute->get_name());
        foreach( $parts as $part ) if( $part == "password" ) return true;
        return false;
    }

    /*
     | Is color field
     |
     | @param       BakeryAttribute         The attribute we want to check
     | @return      Boolean
    */
    private function is_color_field(BakeryAttribute $attribute)
    {
        $parts = explode("_", $attribute->get_name());
        foreach( $parts as $part ) if( $part == "color" ) return true;
        return false;
    }

    #
    # User configuration related methods
    #

    /*
     | Get user set configuration
     |
     | @param       BakeryAttribute         The attribute we want to check
     | @return      Array / False
    */
    private function get_user_set_configuration(BakeryAttribute $attribute)
    {
        $instance = $attribute->get_model()->get_instance();
        return isset($instance->form_configuration) ? $instance->form_configuration : false;
    }

}
