<?php

namespace Verheijen\Bakery\Traits;

trait FieldAnalyzer {

    /*
     | Is image upload field
     |
     | @param       String          Name of the field we want to check
     | @param       Collection      All the fields of the given model
     | @return      Boolean
    */
    public function is_image_upload_field($field_name, $fields)
    {
        foreach( $fields as $field )
        {
            if( $field->get_name() == $field_name )
            {
                $classname = $this->strip_namespace_from_classname($field);
                if( $classname == "BakeryImageUploadField" ) return true;
            }
        }
        return false;
    }

    /*
     | Is field upload field
     |
     | @param       String          Name of the field we want to check
     | @param       Collection      All the fields of the given model
     | @return      Boolean
    */
    public function is_file_upload_field($field_name, $fields)
    {
        foreach( $fields as $field )
        {
            if( $field->get_name() == $field_name )
            {
                $classname = $this->strip_namespace_from_classname($field);
                if( $classname == "BakeryFileUploadField" ) return true;
            }
        }
        return false;
    }

    /*
     | Strip namespace from classname
     |
     | @param       String          Namespaced classname
     | @return      String          Classname
    */
    private function strip_namespace_from_classname($classname)
    {
        $classname = get_class($classname);
        $parts     = explode("\\", $classname);
        $last      = count($parts) - 1;
        return $parts[$last];
    }

}
