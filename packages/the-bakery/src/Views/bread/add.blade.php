@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            Adding a new <b>{{ $model->get_add_title() }}</b> record
            <a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" class="btn btn-xs btn-primary pull-right mar-t-5"><i class="fa fa-arrow-left mar-r-5"></i>Go back to browsing</a>
        </h1>
        <!--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
        -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                @include('bakery::partials.form-errors')
                <div class="box box-success">
                    <form role="form" enctype="multipart/form-data" method="post" action="{{ route('bakery.add.post', ['model_slug' => $model->get_slug()]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="box-body pad-b-0">
                            {!! $form->render() !!}
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success mar-r-5">Create new record!</button>
                            <a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
