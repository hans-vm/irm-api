@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            Reading {{ $model->get_read_pre_title() }} <b>{{ $model->get_read_title() }}</b>
            <a href="{{ route('bakery.delete', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-xs btn-danger pull-right mar-t-5"><i class="fa fa-trash mar-r-5"></i>Delete record</a>
            <a href="{{ route('bakery.edit',   ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-xs btn-warning pull-right mar-t-5 mar-r-10"><i class="fa fa-cogs mar-r-5"></i>Edit record</a>
            <a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" class="btn btn-xs btn-primary pull-right mar-t-5 mar-r-10"><i class="fa fa-arrow-left mar-r-5"></i>Back to browsing</a>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Record data</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            @if( $model->get_read_attributes($entry)->count() == 0 )
                                <tr>
                                    <td class="text-center">This model does not have attributes it can display, check your settings if this is not the expected outcome. If it is, maybe you should think about excluding this model from the bakery.</td>
                                </tr>
                            @else
                                @foreach( $model->get_read_attributes($entry) as $attribute )
                                    <tr>
                                        <td width="25%"><strong>{{ $attribute['name'] }}</strong></td>
                                        <td width="75%">{!! $attribute['value'] !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if( $model->has_browseable_relations() )
            @foreach( $model->get_browseable_relations() as $relation )
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <div class="box-title">{!! $relation->get_method_name(true) !!}</div>
                                <div class="box-tools pull-right">
                                    <a href="#" class="btn btn-primary btn-xs mar-t-5 mar-r-5">Browse all {!! $relation->get_method_name(true) !!}</a>
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body table-table-responsive no-padding">
                                <table class="table table-hover">
                                    @if( ! $relation->has_related_records($entry) )
                                        <tr>
                                            <td>This {{ strtolower($entry->get_model()->get_classname()) }} does not have any {{ $relation->get_method_name() }}</td>
                                        </tr>
                                    @else
                                        <tr>
                                            @foreach( $relation->get_related_class()->get_overview_attributes() as $attribute )
                                                <th>{{ $attribute->get_name(true) }}</th>
                                            @endforeach
                                            <th class="text-right">Options</th>
                                        <tr>
                                        @foreach( $relation->get_related_records($entry) as $related_entry )
                                            <tr>
                                                @foreach( $relation->get_related_class()->get_overview_attributes() as $attribute )
                                                    <td>{!! $related_entry->get_value($attribute->get_name(), 50, true) !!}</td>
                                                @endforeach
                                                <td class="text-right">
                                                    <a href="{{ route('bakery.read',   ['model_slug' => $relation->get_related_class()->get_slug(), 'identifier' => $related_entry->get_id()]) }}" class="mar-r-5">Read</a>
                                                    <a href="{{ route('bakery.edit',   ['model_slug' => $relation->get_related_class()->get_slug(), 'identifier' => $related_entry->get_id()]) }}" class="text-yellow mar-r-5">Edit</a>
                                                    <a href="{{ route('bakery.delete', ['model_slug' => $relation->get_related_class()->get_slug(), 'identifier' => $related_entry->get_id()]) }}" class="text-red">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </section>

@stop
