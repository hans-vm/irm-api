@if( $fields->count() > 0 )
    @foreach( $fields as $field )
        {!! $field->render() !!}
    @endforeach
@endif
