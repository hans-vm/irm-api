@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            Browsing <b>{{ $model->get_browse_title() }}</b>
            <a href="{{ route('bakery.add', ['model_slug' => $model->get_slug()]) }}" class="btn btn-xs btn-success pull-right mar-t-5"><i class="fa fa-plus-circle mar-r-5"></i>Add a new record</a>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Records</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                @foreach( $model->get_overview_attributes() as $overview_attribute )
                                    <th>{{ $overview_attribute->get_name(true) }}</th>
                                @endforeach
                                <th class="text-right">Options</th>
                            </tr>
                            @if( $model->has_entries() )
                                @foreach( $model->get_entries() as $entry )
                                    <tr>
                                        @foreach( $model->get_overview_attributes() as $attribute )
                                            <td>{!! $entry->get_value($attribute->get_name(), 50) !!}</td>
                                        @endforeach
                                        <td class="text-right">
                                            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="mar-r-5">Read</a>
                                            <a href="{{ route('bakery.edit', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="text-yellow mar-r-5">Edit</a>
                                            <a href="{{ route('bakery.delete', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="text-red">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="{{ $model->get_overview_attributes()->count() + 1 }}">There are no {{ $model->get_browse_title() }} in the database yet, maybe you should create the first.</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
