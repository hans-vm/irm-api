@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            Editing an existing <b>{{ $model->get_edit_title() }}</b> record
            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_data()->id]) }}" class="btn btn-xs btn-primary pull-right mar-t-5 mar-r-5"><i class="fa fa-arrow-left mar-r-5"></i>Go back to reading this record</a>
        </h1>
        <!--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
        -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                @include('bakery::partials.form-errors')
                <div class="box box-warning">
                    <form action="{{ route('bakery.edit.post', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" method="post" enctype="multipart/form-data" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="box-body pad-b-0">
                            {!! $form->render() !!}
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-success mar-r-5">Save changes!</button>
                            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>

@stop
