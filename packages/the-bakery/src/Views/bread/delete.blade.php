@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            Deleting a record from <b>{{ $model->get_delete_title() }}</b>
        </h1>
        <!--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
        -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                @include('bakery::partials.form-errors')
                <form action="{{ route('bakery.delete.post', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Danger zone</h3>
                        </div>
                        <div class="box-body">
                            <p class="mar-b-0">You are about to record #{{ $entry->get_id() }} from your <strong>{{ $model->get_delete_title() }}</strong>, are you absolutely sure you want to delete this record?</p>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-danger mar-r-10">Yes, delete this record</button>
                            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-default">No, cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </section><!-- /.content -->

@stop
