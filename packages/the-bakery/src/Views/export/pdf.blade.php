@extends("bakery::templates.model-data-pdf")

@section("content")

    @foreach( $models as $model )

        <div style="padding-bottom: 10px;">
            <h1>{{ str_plural($model->get_classname()) }}</h1>
            <h2>{{ $model->get_entries()->count() }} records in the database</h2>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        @foreach( $model->get_attributes() as $attribute )
                            <th>{{ $attribute->get_name() }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @if( $model->has_entries() )
                        @foreach( $model->get_entries() as $entry )
                            <tr>
                                @foreach( $model->get_attributes() as $attribute )
                                    <td>{!! $entry->get_value($attribute->get_name()) !!}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="{{ $model->get_attributes()->count() }}">No {{ strtolower($model->get_classname()) }} records currently in the database</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    @endforeach

@stop
