<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <textarea id="{{ $name }}" class="form-control" name="{{ $name }}" rows="5">{{ old($name, $value) }}</textarea>
</div>
