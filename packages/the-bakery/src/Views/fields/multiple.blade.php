<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <select id="{{ $name }}" class="form-control select2" name="{{ $name }}[]" multiple="multiple" style="width: 100%; margin-left: 5px;">
        @if( is_array($select_options) and count($select_options) > 0 )
            @foreach( $select_options as $key => $val )
                <option value="{{ $key }}" @if( ! is_null($value) and $value == $val ) selected @endif>{{ $val }}</option>
            @endforeach
        @else
            <option value="0">There are no related records to choose from</option>
        @endif
    </select>
</div>
<script>
    $(document).ready(function(){

        var select = $("#{{ $name }}").select2({
            allowClear: true
        });

        @if( ! is_null($value) )
            var raw_values = "{{ $value }}";
            var values = raw_values.split(":");
            var select_values = [];
            for( var i = 0; i < values.length; i++ ){
                select_values.push(values[i]);
            }
            console.log("Select values: ");
            console.log(select_values);
            select.val(select_values).trigger("change");
        @endif
    });
</script>
