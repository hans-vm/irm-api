<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <select id="{{ $name }}" class="form-control select2" name="{{ $name }}" @if(count($select_options) == 0) disabled @endif style="padding: 0; margin-left: 0;">
        @if( is_array($select_options) and count($select_options) > 0 )
            @foreach( $select_options as $key => $val )
                <option value="{{ $key }}" @if( ! is_null($value) and $value == $val ) selected @endif>{{ $val }}</option>
            @endforeach
        @else
            <option value="0">There are no related records to choose from</option>
        @endif
    </select>
</div>
<script>
    $(document).ready(function(){
        var select = $("#{{ $name }}").select2();
        @if( ! is_null($value) )
            select.val({{ $value }}).trigger("change");
        @endif
    });
</script>
