<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <div class="input-group my-colorpicker">
        <div class="input-group-addon">
            <i></i>
        </div>
        <input type="text" id="{{ $name }}" class="form-control" name="{{ $name }}" value="{{ old($name, $value) }}" />
    </div>
</div>
<script>
    $(function(){
        $(".my-colorpicker").colorpicker();
    });
</script>
