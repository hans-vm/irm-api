<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <textarea id="{{ $name }}" class="textarea" name="{{ $name }}" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
        {{ old($name, $value) }}
    </textarea>
</div>
<script>
    $(function(){
        $(".textarea").wysihtml5();
    });
</script>
