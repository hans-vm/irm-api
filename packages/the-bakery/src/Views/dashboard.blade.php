@extends($template)

@section('content')

    <section class="content-header">
        <h1>Bakery Dashboard</h1>
    </section>

    <section class="content">

        <!-- Basic statistics -->
        <div class="row mar-b-10">
            <div class="col-lg-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-light-blue"><i class="fa fa-database"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Models found</span>
                        <span class="info-box-number">{{ $num_models }}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-purple"><i class="fa fa-hashtag"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Within Namespaces</span>
                        <span class="info-box-number">{{ $num_namespaces }}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-teal"><i class="fa fa-tasks"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Total Number Of Records</span>
                        <span class="info-box-number">{{ $num_records }}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-modx"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Relationships</span>
                        <span class="info-box-number">{{ $num_relations }}</span>
                    </div>
                </div>
            </div>
        </div>

        <!-- Area chart of new entries -->
        <div class="row mar-b-10">
            <div class="col-xs-12 col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistics regarding all of your <span id="month-title">database records</span></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <canvas id="barChart" style="height: 350px"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Division of records</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <canvas id="pieChart" style="height: 325px"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <input type="hidden" id="model_donut_data" value='{!! $donut_data !!}' />
    <input type="hidden" id="barchart_data" value='{!! $barchart_data !!}' />

    <script>
        $(document).ready(function(){

            /*
             |
             | Area chart
             |
            */

            var barchart_context    = $("#barChart").get(0).getContext("2d");
            var barchart            = new Chart(barchart_context);
            var barchart_data       = JSON.parse($("#barchart_data").val());

            barchart.Bar({
                labels: barchart_data.labels,
                datasets: [{
                    label: "One and only",
                    fillColor: "rgba(27, 150, 219, .75)",
                    strokeColor: "rgba(27, 150, 219, 1)",
                    highlightFill: "rgba(15, 108, 159, .75)",
                    highlightStroke: "rgba(15, 108, 159, 1)",
                    data: barchart_data.data
                }]
            }, {

            });

            console.log(barchart_data);


            /*
             |
             | Donut chart
             |
            */

            var donut_data = JSON.parse($("#model_donut_data").val());


            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = donut_data;

            var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: "#fff",
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 0, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: "easeOutBounce",
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: false,
                //String - A legend template
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Pie(PieData, pieOptions);




        });
    </script>

@stop
