@if( session('successMessages') )
    <div class="callout callout-success">
        <h4>It was a great success!</h4>
        <ul class="list-unstyled">
            @foreach( session('successMessages') as $message )
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
@endif
