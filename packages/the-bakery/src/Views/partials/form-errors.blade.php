@if( count($errors) > 0 )
    <div class="callout callout-danger">
        <h4>Something went wrong</h4>
        <ul class="list-unstyled">
            @foreach( $errors->all() as $error )
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
