<?php

namespace Verheijen\Bakery;

use DB;
use File;
use Basecamp;
use Validator;
use Carbon\Carbon;

use Verheijen\Bakery\Traits\Uploader;
use Verheijen\Bakery\Objects\BakeryModel;
use Verheijen\Bakery\Objects\BakeryEntry;
use Verheijen\Bakery\Objects\Forms\BakeryAddForm;
use Verheijen\Bakery\Objects\Forms\BakeryEditForm;
use Verheijen\Bakery\Objects\Forms\BakeryDeleteForm;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Bakery {

    use Uploader;

    private $models;
    private $relationship_tables;

    public function __construct()
    {
        $this->models = $this->load_models();
        $this->relationship_tables = $this->load_relationship_tables();
    }

    #
    # Loading data the smart way
    #

    private function load_models()
    {
        $models = new Collection;
        $folders = config("bakery.model_folders");
        if( count($folders) > 0 )
        {
            foreach( $folders as $folder )
            {
                $path = base_path() . "/" . $folder;
                if( is_dir($path) )
                {
                    $files = File::allFiles($path);
                    foreach( $files as $file )
                    {
                        $name_parts = explode(".", $file->getFilename());
                        $namespace = $this->folder_to_namespace($folder);
                        $namespaced_classname = $namespace . "\\" . $name_parts[0];
                        if( class_exists($namespaced_classname) and ! in_array($namespaced_classname, config("bakery.model_exclusions")) )
                        {
                            $models->push(new BakeryModel($namespaced_classname));
                        }
                    }
                }
            }
        }
        return $models;
    }

    private function load_relationship_tables()
    {
        $tables = new Collection;
        if( $this->models->count() > 0 )
        {
            foreach( $this->models as $model )
            {
                if( $model->has_relations() )
                {
                    foreach( $model->get_relations() as $relation )
                    {
                        switch( $relation->get_type() )
                        {
                            case "MorphToMany":
                            case "BelongsToMany":
                            if( ! $tables->has($relation->get_instance()->getTable()) )
                            {
                                $tables->put($relation->get_instance()->getTable(), DB::table($relation->get_instance()->getTable())->get());
                            }
                            break;
                        }
                    }
                }
            }
        }
        // dd($tables);
        return $tables;
    }

    #
    # Getters & checks
    #

    public function get_models()
    {
        return $this->models;
    }

    public function has_models()
    {
        return $this->models->count() > 0;
    }

    public function get_model_by_slug($model_slug)
    {
        foreach( $this->models as $model )
        {
            if( $model->get_slug() == $model_slug )
            {
                return $model;
            }
        }
        return false;
    }

    public function get_model_by_namespaced_classname($namespaced_classname)
    {
        foreach( $this->models as $model )
        {
            if( $model->get_namespaced_classname() == $namespaced_classname )
            {
                return $model;
            }
        }
        return false;
    }

    public function get_relationship_table($table)
    {
        if( $this->relationship_tables->has($table) )
        {
            return $this->relationship_tables->get($table);
        }
        else
        {
            return false;
        }
    }

    #
    # Dashboard related
    #

    public function get_number_of_models()
    {
        return $this->models->count();
    }

    public function get_number_of_namespaces()
    {
        $number = 0;
        $namespaces = [];
        if( $this->has_models() )
        {
            foreach( $this->models as $model )
            {
                if( ! in_array($model->get_namespace(), $namespaces) )
                {
                    $number++;
                    $namespaces[] = $model->get_namespace();
                }
            }
        }
        return $number;
    }

    public function get_number_of_records()
    {
        $number = 0;
        if( $this->has_models() )
        {
            foreach( $this->models as $model )
            {
                $number += $model->get_entries()->count();
            }
        }
        return $number;
    }

    public function get_number_of_relations()
    {
        $number = 0;
        if( $this->has_models() )
        {
            foreach( $this->models as $model )
            {
                if( $model->has_relations() )
                {
                    $number += $model->get_relations()->count();
                }
            }
        }
        return $number;
    }

    public function get_donut_data($return_json = false)
    {
        $output = [];
        if( $this->has_models() )
        {
            foreach( $this->models as $model )
            {
                $color = $this->random_color();
                $output[] = [
                    'value' => $model->get_entries()->count(),
                    'color' => $color,
                    'highlight' => $color,
                    'label' => str_plural($model->get_classname())
                ];
            }
        }
        return $return_json ? json_encode($output) : $output;
    }

    public function get_barchart_data($json = false)
    {
        $output = [
            'labels' => [],
            'data'   => []
        ];
        if( $this->has_models() )
        {
            foreach( $this->models as $model )
            {
                $output['labels'][] = str_plural($model->get_classname());//str_plural(ucfirst(strtolower($model->get_classname())));
                $output['data'][]   = $model->get_entries()->count();
            }
        }
        return $json ? json_encode($output) : $output;
    }

    #
    # Form related
    #

    public function generate_form($operation, BakeryModel $model, BakeryEntry $entry = null)
    {
        switch( $operation )
        {
            case "add":
                return new BakeryAddForm($model);
            break;
            case "edit":
                return new BakeryEditForm($model, $entry);
            break;
            case "delete":
                return new BakeryDeleteForm($model, $entry);
            break;
            default:
                return false;
            break;
        }
    }

    public function process_form($operation, Request $request, $model_slug, $identifier = null)
    {
        # Grab the model & entry & generate the form we are currently processing
        $model = $this->get_model_by_slug($model_slug);
        $entry = is_null($identifier) ? null : $model->get_entry($identifier);
        $form  = $this->generate_form($operation, $model, $entry);

        #
        # Validation
        #

        if( $operation == "add" or $operation == "edit" )
        {
            if( isset($model->get_instance()->form_configuration) )
            {
                $form_configuration = $model->get_instance()->form_configuration;
                $final_rules = [];
                foreach( $form->get_fields() as $field )
                {
                    if( array_key_exists($field->get_name(), $form_configuration) )
                    {
                        $final_rules[$field->get_name()] = $form_configuration[$field->get_name()]["rules"];
                    }
                }
                $validator = Validator::make($request->all(), $final_rules);
                if( $validator->fails() ) return redirect()->back()->withErrors($validator->errors())->withInput();
            }
        }

        #
        # Processing
        #

        # Switch between possible operations
        switch( $operation )
        {
            #
            # Add operation
            #
            case "add":

                # Array of data which we will use to create a new entry for the model we're processing
                $creation_data = [];

                # Loop through all of the form's fields
                foreach( $form->get_fields() as $field )
                {
                    # Grab the field name here so we don't constantly have to call the function
                    $field_name = $field->get_name();

                    # If this is a settable field and we've received input
                    if( $model->has_attribute($field_name) and ( $request->has($field_name) or $request->hasFile($field_name) ) )
                    {
                        # If we are processing an image upload field
                        if( $form->is_image_upload_field($field_name, $form->get_fields()) )
                        {
                            # Gather intelligence about the default settings
                            $upload_settings = ['upload_path' => config("bakery.default_image_upload_path")];

                            # Grab any settings the user might have set and process them
                            $user_set_config = $this->get_upload_config($model, $field_name);
                            if( $user_set_config and is_array($user_set_config) )
                            {
                                foreach( $user_set_config as $key => $setting )
                                {
                                    switch( $key )
                                    {
                                        case "upload_path":
                                        case "min_width":
                                        case "max_width":
                                        case "exact_width":
                                        case "min_height":
                                        case "max_height":
                                        case "exact_height":
                                            $upload_settings[$key] = $setting;
                                        break;
                                    }
                                }
                            }

                            # Try to upload the image
                            try
                            {
                                $uploaded_file_path = $this->upload("image", $request->file($field_name), $upload_settings);
                            }
                            catch( Exception $e )
                            {
                                throw new Exception($e->getMessage());
                            }

                            # Save the path to the uploaded file in the database
                            $creation_data[$field_name] = $uploaded_file_path;
                        }
                        # If we are processing a file upload field
                        elseif( $form->is_file_upload_field($field_name, $form->get_fields()) )
                        {
                            # Gather intelligence
                            $upload_settings = ['upload_path' => config('bakery.default_file_upload_path')];

                            # Grab any settings the user might have set and process them
                            $user_set_config = $this->get_upload_config($model, $field_name);
                            if( $user_set_config and is_array($user_set_config) )
                            {
                                foreach( $user_set_config as $key => $setting )
                                {
                                    switch( $key )
                                    {
                                        case "upload_path":
                                            $upload_settings[$key] = $setting;
                                        break;
                                    }
                                }
                            }

                            # Try to upload the file
                            try
                            {
                                $uploaded_file_path = $this->upload("file", $request->file($field_name), $upload_settings);
                            }
                            catch(Exception $e)
                            {
                                throw new Exception($e->getMessage());
                            }

                            # Save the path to the uploaded file in the database
                            $creation_data[$field_name] = $uploaded_file_path;
                        }
                        # If we are processing any other field
                        else
                        {
                            # Simply add the value to the creation data
                            $creation_data[$field_name] = $request->input($field_name);
                        }
                    }
                }

                # Create the entry, all the data that could be set on the model should have by now
                $created_entry = $model->get_instance()->create($creation_data);

                # If the model has relationships
                if( $model->has_relations() )
                {
                    # Loop through all of them
                    foreach( $model->get_relations() as $relation )
                    {
                        # Switch between relation types
                        switch( $relation->get_type() )
                        {
                            # Has one
                            # ( when a has one relationship is present all the fillable attributes of the relative are added and should be processed )
                            # The attributes are set + an identifier of the created entry on a new relative entry
                            case "HasOne":
                                # Grab some information we need
                                $method_name         = $relation->get_method_name();
                                $related_class       = $relation->get_related_class();
                                $relative_attributes = $related_class->get_fillable_attributes();
                                # This array will hold all the values of the new relative we are creating
                                $values = [$relation->get_instance()->getPlainForeignKey() => $created_entry->id];
                                # Fill our values array with some more if we found any fillable attributes
                                if( $relative_attributes->count() > 0 )
                                {
                                    foreach( $relative_attributes as $relative_attribute )
                                    {
                                        $input_name = $method_name.'_'.$relative_attribute->get_name();
                                        $db_name    = $relative_attribute->get_name();
                                        if( $request->has($input_name) )
                                        {
                                            $values[$db_name] = $request->input($input_name);
                                        }
                                    }
                                }
                                # Create the new relative with all the data we've gathered
                                $new_relative = $related_class->get_instance()->create($values);
                            break;
                            # Morph to many
                            # ( Roles are used as an example in the proof of concept )
                            # A seperate table is used for these kind of relationships
                            case "MorphToMany":
                                # Grab some valuable information
                                $name     = $relation->get_method_name();
                                $relative = $relation->get_related_class();
                                # Create a collection for our related entries
                                $related_entries = [];
                                # If the form data has been set
                                if( $request->has($name) )
                                {
                                    $ids = $request->input($name);
                                    # Process the related entries
                                    foreach( $ids as $id ) $related_entries[] = $relative->get_instance()->find($id);
                                    # Attach the relatives to the entry we just created
                                    $created_entry->$name()->saveMany($related_entries);
                                }
                            break;
                            # Morph to
                            # ( 2 (or actually probably more) select elements take care of this relationship )
                            # The attributes are set on the created entry
                            case "MorphTo":
                                # Grab some valuable information
                                $method     = $relation->get_method_name();
                                $type_field = $relation->get_instance()->getMorphType();
                                $id_field   = $relation->get_instance()->getForeignKey();

                                if( $request->has($type_field) )
                                {
                                    $selected_type_classname    = $request->input($type_field);
                                    $selected_type_model        = $this->get_model_by_namespaced_classname($selected_type_classname);
                                    $child_input_field          = strtolower($selected_type_model->get_classname());
                                    $child_id                   = $request->input($child_input_field);
                                    $created_entry->$type_field = $selected_type_classname;
                                    $created_entry->$id_field   = $child_id;
                                    $created_entry->save();
                                }
                            break;
                            # Belongs to many
                            # ( a multiselect takes care of the belongs to many relationship )
                            # A seperate table is used for these kind of relationships
                            case "BelongsToMany":
                                # Grab some valuable information
                                $method = $relation->get_method_name();
                                # If the user has set
                                if( $request->has($method) )
                                {
                                    $data = [];
                                    foreach( $request->input($method) as $id )
                                    {
                                        $data[] = $relation->get_related_class()->get_instance()->find($id);
                                    }
                                    $created_entry->$method()->saveMany($data);
                                }
                            break;
                        }
                    }
                }

                # Redirect
                return redirect()->route('bakery.read', ['model_slug' => $model_slug, 'identifier' => $created_entry->id])->with('successMessages', ['You have created a new record']);

            break;
            #
            # Edit operation
            #
            case "edit":

                # Record we are modifying
                $record = $entry->get_data();

                # Loop through all of the form's fields
                foreach( $form->get_fields() as $field )
                {
                    # Grab the field name here so we don't constantly have to call the function
                    $field_name = $field->get_name();

                    # If this is a settable field and we've received input
                    if( $model->has_attribute($field_name) and ( $request->has($field_name) or $request->hasFile($field_name) ) )
                    {
                        # If we are processing an image upload field
                        if( $form->is_image_upload_field($field_name, $form->get_fields()) )
                        {
                            # Gather intelligence about the default settings
                            $upload_settings = ['upload_path' => config("bakery.default_image_upload_path")];

                            # Grab any settings the user might have set and process them
                            $user_set_config = $this->get_upload_config($model, $field_name);
                            if( $user_set_config and is_array($user_set_config) )
                            {
                                foreach( $user_set_config as $key => $setting )
                                {
                                    switch( $key )
                                    {
                                        case "upload_path":
                                        case "max_size":
                                        case "min_width":
                                        case "max_width":
                                        case "exact_width":
                                        case "min_height":
                                        case "max_height":
                                        case "exact_height":
                                            $upload_settings[$key] = $setting;
                                        break;
                                    }
                                }
                            }

                            # Try to upload the image
                            try
                            {
                                $uploaded_file_path = $this->upload("image", $request->file($field_name), $upload_settings);
                            }
                            catch( Exception $e )
                            {
                                throw new Exception($e->getMessage());
                            }

                            # Save the new image path on the record
                            $record->$field_name = $uploaded_file_path;
                        }
                        # If we are processing a file upload field
                        elseif( $form->is_file_upload_field($field_name, $form->get_fields()) )
                        {
                            # Gather intelligence
                            $upload_settings = ['upload_path' => config('bakery.default_file_upload_path')];

                            # Grab any settings the user might have set and process them
                            $user_set_config = $this->get_upload_config($model, $field_name);
                            if( $user_set_config and is_array($user_set_config) )
                            {
                                foreach( $user_set_config as $key => $setting )
                                {
                                    switch( $key )
                                    {
                                        case "upload_path":
                                            $upload_settings[$key] = $setting;
                                        break;
                                    }
                                }
                            }

                            # Try to upload the image
                            try
                            {
                                $uploaded_file_path = $this->upload("file", $request->file($field_name), $upload_settings);
                            }
                            catch(Exception $e)
                            {
                                throw new Exception($e->getMessage());
                            }

                            # Save the new image path on the record
                            $record->$field_name = $uploaded_file_path;
                        }
                        # If we are processing any other field
                        else
                        {
                            # Simply add the value to the creation data
                            $record->$field_name = $request->input($field_name);
                        }
                    }
                }

                # Save the changes we've made so far
                $record->save();

                # Check if the model has relationships that could have added some special fields to the edit form
                if( $model->has_relations() )
                {
                    foreach( $model->get_relations() as $relation )
                    {
                        switch( $relation->get_type() )
                        {
                            # Has one
                            # All fillable attributes of relative
                            case "HasOne":
                                # Grab some information we need
                                $method_name         = $relation->get_method_name();
                                $related_class       = $relation->get_related_class();
                                $foreign_key         = $relation->get_instance()->getPlainForeignKey();
                                $related_record      = $related_class->get_instance()->where($foreign_key, $record->id)->first();
                                # If the record was found and it has fillable attributes
                                if( $related_record and $related_class->get_fillable_attributes() )
                                {
                                    # Grab the fillable attributes of the related record
                                    $fillable_attributes = $related_class->get_fillable_attributes();
                                    # Loop through all of these fillable attributes
                                    foreach( $fillable_attributes as $fillable_attribute )
                                    {
                                        # Determine what the attribute name in our database is
                                        $db_name = $fillable_attribute->get_name();
                                        # Determine what we called the corresponding form input
                                        $input_name = $method_name."_".$db_name;
                                        # Check if we received a value for this attribute
                                        if( $request->has($input_name) )
                                        {
                                            # Save the given value on the related record
                                            $related_record->$db_name = $request->input($input_name);
                                        }
                                    }
                                }
                                # Save the changes we (might have) made
                                $related_record->save();
                            break;
                            # Morph to many
                            # Seperate table
                            case "MorphToMany":
                                # Gather important information
                                $method             = $relation->get_method_name();
                                $relatives          = $record->$method;
                                $related_model      = $relation->get_related_class();
                                # If data was posted
                                if( $request->has($method) )
                                {
                                    # Detach all current relatives
                                    foreach( $relatives as $relative ) $record->$method()->detach($relative->id);
                                    # Grab the id's of the new relatives
                                    $new_relative_ids = $request->input($method);
                                    # Array of instances of all the new relatives
                                    $new_relatives = [];
                                    # Save instances of all the new relatives
                                    foreach( $new_relative_ids as $id ) $new_relatives[] = $related_model->get_instance()->find($id);
                                    # Attach all the new relatives to the record
                                    $record->$method()->saveMany($new_relatives);
                                }
                            break;
                            # Morph to
                            # 2+ multiselects
                            case "MorphTo":
                                # Gather intel
                                $method     = $relation->get_method_name();
                                $type_field = $relation->get_instance()->getMorphType();
                                $id_field   = $relation->get_instance()->getForeignKey();
                                # If data was posted
                                if( $request->has($type_field) )
                                {
                                    # Gather some more intel
                                    $selected_type_classname    = $request->input($type_field);
                                    $selected_type_model        = $this->get_model_by_namespaced_classname($selected_type_classname);
                                    $child_input_field          = strtolower($selected_type_model->get_classname());
                                    $child_id                   = $request->input($child_input_field);
                                    $type                       = $request->input($type_field);
                                    # Save the new relationship data
                                    $record->$type_field = $type;
                                    $record->$id_field   = $child_id;
                                    $record->save();
                                }

                            break;
                            # Belongs to many
                            # A single multiselect (roles)
                            case "BelongsToMany":
                                # Gather intel
                                $method         = $relation->get_method_name();
                                $relatives      = $record->$method;
                                $related_model  = $relation->get_related_class();
                                # If data was posted
                                if( $request->has($method) )
                                {
                                    # Detach all the current relatives
                                    foreach( $relatives as $relative ) $record->$method()->detach($relative->id);
                                    # Grab the id's of the new relatives
                                    $new_relative_ids = $request->input($method);
                                    # Array with instances of all the new relative records
                                    $new_relatives = [];
                                    # Save instances of all the new relatives
                                    foreach( $new_relative_ids as $id ) $new_relatives[] = $related_model->get_instance()->find($id);
                                    # Attach all the new relatives to the record
                                    $record->$method()->saveMany($new_relatives);
                                }
                            break;
                        }
                    }
                }

                # Redirect
                return redirect()->route('bakery.read', ['model_slug' => $model_slug, 'identifier' => $record->id])->with('successMessages', ['You have edited the record']);

            break;
            #
            # Delete operation
            #
            case "delete":

                # Simply delete the record
                $entry->get_data()->delete();

                # And redirect
                return redirect()->route('bakery.browse', ['model_slug' => $model_slug])->with('successMessages', ['You have deleted the record']);

            break;
        }
    }

    #
    # Private functions
    #

    private function get_upload_config($model, $field_name)
    {
        if( isset($model->get_instance()->form_configuration) )
        {
            if( array_key_exists($field_name, $model->get_instance()->form_configuration) )
            {
                return $model->get_instance()->form_configuration[$field_name];
            }
        }
        return false;
    }

    private function folder_to_namespace($folder)
    {
        $namespace = "";
        $folders = explode("/", $folder);
        foreach( $folders as $folder )
        {
            if( $namespace != "" ) $namespace .= "\\";
            $namespace .= ucfirst($folder);
        }
        return $namespace;
    }

    private function random_color()
    {
        $output  = "#";
        for($i = 0; $i < 3; $i++){
            $output .= str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
        }
        return $output;
    }

}
