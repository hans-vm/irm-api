<?php

namespace Verheijen\Bakery\Controllers;

use Excel;
use Bakery;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Verheijen\Basecamp\Controllers\BasecampController;

/*
 | Bakery Controller
 | This is the controller that's responsible for all of the B.R.E.A.D operation pages
*/
class BakeryController extends BasecampController {

    # The template we are using for all our views
    private $basecamp_template;

    /*
     | Constructor
    */
    public function __construct()
    {
        # Grab & save the template from the bakery's config file
        $this->basecamp_template = config('bakery.template');

        # If any middleware have been set call them
        $middleware = config('bakery.middleware');
        if( is_array($middleware) and count($middleware) > 0 ) $this->middleware($middleware);
    }

    /*
     | Dashboard
     |
     | @return      View
    */
    public function dashboard()
    {
        return view('bakery::dashboard', [
            'template'       => $this->basecamp_template,
            'models'         => Bakery::get_models(),
            'num_models'     => Bakery::get_number_of_models(),
            'num_namespaces' => Bakery::get_number_of_namespaces(),
            'num_relations'  => Bakery::get_number_of_relations(),
            'num_records'    => Bakery::get_number_of_records(),
            'barchart_data'  => Bakery::get_barchart_data(true),
            'donut_data'     => Bakery::get_donut_data(true)
        ]);
    }

    /*
     | Export
     |
     | @return      View
    */
    public function export()
    {
        return view("bakery::export", [
            'template'  => $this->basecamp_template,
            'models'    => Bakery::get_models()
        ]);
    }

    /*
     | Export post handler
     |
     | @param       Request         The request that got us here
     | @param       String          Format we want to export the data as
     | @return      Redirect
    */
    public function exportPost(Request $request, $format)
    {
        # If no models were selected
        if( ! $request->has("models") ) return redirect()->back()->withErrors(['error' => 'You need to at least select one model']);

        # Switch between the possible format
        switch( $format )
        {
            case "pdf":
                return $this->exportPdf($request);
            break;
            case "excel":
                return $this->exportExcel($request);
            break;
            default:
                # We should never reach this point but if someone tries to be smart, redirect them back
                return redirect()->back();
            break;
        }
    }

    /*
     | Export as PDF
     |
     | @param       Request         The request that got us here
     | @return      Redirect (download)
    */
    private function exportPdf(Request $request)
    {
        #
        # Deprecated since the plugins are unstable
        #
    }

    /*
     | Export as Excel
     |
     | @param       Request         The request that got us here
     | @return      Redirect (download)
    */
    private function exportExcel(Request $request)
    {
        # Convert all model slugs to BakeryModel instances
        $models = new Collection;
        foreach( $request->input("models") as $slug ) $models->push(Bakery::get_model_by_slug($slug));

        # Generate an excel file
        Excel::create('bakery-data-export', function($excel) use($models) {
            foreach( $models as $model )
            {
                $excel->sheet(str_plural($model->get_classname()), function($sheet) use($model) {
                    $sheet->fromArray($model->get_instance()->all());
                });
            }
        })->export('xls');
    }

    /*
     | Browse
     |
     | @param       String          Slug of the model we want to browse
     | @return      View
    */
    public function browse($model_slug)
    {
        //dd(Bakery::get_model_by_slug($model_slug)->get_overview_attributes());
        return view('bakery::bread.browse', [
            'template'  => $this->basecamp_template,
            'model'     => Bakery::get_model_by_slug($model_slug)
        ]);
    }

    /*
     | Read
     |
     | @param       String          Slug of the model the entry we want to read belongs to
     | @param       Integer         ID of the entry we want to read
     | @return      View
    */
    public function read($model_slug, $identifier)
    {
        $model = Bakery::get_model_by_slug($model_slug);

        return view('bakery::bread.read', [
            'template'  => $this->basecamp_template,
            'model'     => $model,
            'entry'     => $model->get_entry($identifier)
        ]);
    }

    /*
     | Add
     |
     | @param       String          Slug of the model we want to add a new record to
     | @return      View
    */
    public function add($model_slug)
    {
        $model = Bakery::get_model_by_slug($model_slug);
        $form  = Bakery::generate_form('add', $model);

        // dd($form->get_fields());

        return view('bakery::bread.add', [
            'template'  => $this->basecamp_template,
            'model'     => $model,
            'form'      => $form
        ]);
    }

    /*
     | Add post handler
     |
     | @param       Request         The request that got us here
     | @param       String          The slug of the model we are adding a new record to
     | @return      Redirect
    */
    public function addPost(Request $request, $model_slug)
    {
        try
        {
            return Bakery::process_form('add', $request, $model_slug);
        }
        catch( Exception $e )
        {
            return redirect()->back()->withErrors(['errors' => $e->getMessage()])->withInput();
        }
    }

    /*
     | Edit
     |
     | @param       String          Slug of the model we want to add a new record to
     | @param       Integer         ID of the record we want to edit
     | @return      View
    */
    public function edit($model_slug, $identifier)
    {
        $model = Bakery::get_model_by_slug($model_slug);
        $entry = $model->get_entry($identifier);
        if( ! $entry ) return redirect()->back()->withErrors(['error' => 'The related record could not be found, seems like this relationship is broken']);
        $form  = Bakery::generate_form('edit', $model, $entry);

        return view('bakery::bread.edit', [
            'template'  => $this->basecamp_template,
            'model'     => $model,
            'entry'     => $entry,
            'form'      => $form
        ]);
    }

    /*
     | Edit post handler
     |
     | @param       Request         Request that got us here
     | @param       String          Slug of the model we want to add a new record to
     | @param       Integer         ID of the record we want to edit
     | @return      Redirect
    */
    public function editPost(Request $request, $model_slug, $identifier)
    {
        try
        {
            return Bakery::process_form('edit', $request, $model_slug, $identifier);
        }
        catch( Exception $e )
        {
            return redirect()->back()->withErrors(['errors' => $e->getMessage()])->withInput();
        }
    }

    /*
     | Delete
     |
     | @param       String          Slug of the model we want to add a new record to
     | @param       Integer         ID of the record we want to delete
     | @return      View
    */
    public function delete($model_slug, $identifier)
    {
        $model = Bakery::get_model_by_slug($model_slug);
        $entry = $model->get_entry($identifier);
        $form  = Bakery::generate_form('delete', $model, $entry);

        return view('bakery::bread.delete', [
            'template'  => $this->basecamp_template,
            'model'     => $model,
            'entry'     => $entry,
            'form'      => $form
        ]);
    }

    /*
     | Delete post handler
     |
     | @param       Request         The request that got us here
     | @param       String          Slug of the model we want to add a new record to
     | @param       Integer         ID of the record we are deleting
     | @return      Redirect
    */
    public function deletePost(Request $request, $model_slug, $identifier)
    {
        try
        {
            return Bakery::process_form('delete', $request, $model_slug, $identifier);
        }
        catch( Exception $e )
        {
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);
        }
    }

}
