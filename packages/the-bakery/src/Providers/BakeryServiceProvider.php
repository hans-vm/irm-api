<?php

namespace Verheijen\Bakery\Providers;

use Verheijen\Bakery\Bakery;
use Illuminate\Support\ServiceProvider;

class BakeryServiceProvider extends ServiceProvider
{
    public function boot()
    {
        # Dashboard link
        $this->app->basecamp->add_link_to_menu("NAVIGATION", "Dashboard", route("bakery.dashboard"), "dashboard");

        # Model links
        $children = [];
        if( $this->app->bakery->has_models() )
        {
            foreach( $this->app->bakery->get_models() as $model )
            {
                $children[] = [
                    'icon' => 'circle-o',
                    'text' => $model->get_menu_title(),
                    'url'  => route('bakery.browse', $model->get_slug())
                ];
            }
        }
        $this->app->basecamp->add_link_to_menu("DATASOURCE", "Models", "#", "database", $children);

        # Export data link
        $this->app->basecamp->add_link_to_menu("DATASOURCE", "Export", route("bakery.export"), "download");


    }

    public function register()
    {
        # Bind a singleton instance of the Bakery library to the IoC container
        $this->app->singleton('bakery', function(){
            return new Bakery;
        });

        # Setup the views
		$this->loadViewsFrom(__DIR__.'/../Views', 'bakery');

        # Setup the config file
		$this->mergeConfigFrom(__DIR__.'/../Config/config.php', 'bakery');

        # Setup config publishing
        $this->publishes([__DIR__.'/../Config/config.php' => config_path('bakery.php')], 'config');

        # Setup view publishing
        $this->publishes([__DIR__.'/../Views' => base_path('resources/views/vendor/bakery')], 'views');

        # Include the routes of this application
        require __DIR__.'/../Config/routes.php';
    }
}
