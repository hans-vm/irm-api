<?php

namespace Verheijen\Basecamp\Facades;

use Illuminate\Support\Facades\Facade;

class Basecamp extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'basecamp';
	}
}
