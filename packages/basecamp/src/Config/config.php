<?php

return [

    // Page title
    'head_title' => 'Basecamp',

    // Website title
    'title' => '<b>Base</b>camp',

    // Website title > small version
    'title_small' => '<b>B</b>C',

    // Text to be displayed in the footer
    'footer_text' => '<strong>Copyright &copy; 2016-2017 <a href="http://www.nickverheijen.nl">Verheijen Webdevelopment</a>.</strong> All rights reserved.',

    // The AdminLTE skin we are using
    // Options: `blue`, `red`, `green`, `yellow`, `purple`, `black`, `blue-light`, `black-light`, `purple-light`, `green-light`, `red-light`, `yellow-light`
    'skin' => 'black-light',

    // Custom middleware which should be called in the bakery's controller
    'middleware' => []

];
