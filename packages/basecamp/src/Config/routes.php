<?php

Route::group(['prefix' => 'basecamp', 'middleware' => ['web']], function(){

    Route::get('/', ['as' => 'basecamp.dashboard', 'uses' => 'Verheijen\Bakery\Controllers\BakeryController@dashboard']);

});
