<?php

namespace Verheijen\Basecamp\Providers;

use View;
use Verheijen\Basecamp\Basecamp;
use Illuminate\Support\ServiceProvider;

class BasecampServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        # Register the Basecamp class to the IoC container
        $this->app->singleton('basecamp', function(){
            return new Basecamp;
        });

        # Setup the view loader
		$this->loadViewsFrom(__DIR__.'/../Views', 'basecamp');

        # Setup asset publishing
        $this->publishes([
            __DIR__.'/../Assets' => public_path('assets/basecamp')
        ], 'assets');

        # Setup the config file
		$this->mergeConfigFrom(__DIR__.'/../Config/config.php', 'basecamp');

        # Setup publishing of the config file
        $this->publishes([__DIR__.'/../Config/config.php' => config_path('basecamp.php')], 'config');

        # Setup publishing of the Views
        $this->publishes([__DIR__.'/../Views' => base_path('resources/views/vendor/basecamp')], 'views');

        # Include the package's routes
        require __DIR__.'/../Config/routes.php';

        # Setup a view composer to add some variables from the config file
        View::composer('basecamp::templates.lte', function($view){
            $view->with('head_title',       config('basecamp.head_title'));
            $view->with('title',            config('basecamp.title'));
            $view->with('title_small',      config('basecamp.title_small'));
            $view->with('footer_text',      config('basecamp.footer_text'));
            $view->with('skin',             config('basecamp.skin'));
        });
    }
}
