<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ $head_title }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/bower_components/select2/dist/css/select2.min.css') }}">
        <!-- Colorpicker -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/js/colorpicker/bootstrap-colorpicker.min.css') }}">
        <!-- Bootstrap WYSIWYG -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/js/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <!-- Charts -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/js/morris/morris.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/css/AdminLTE.css') }}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/css/skins/_all-skins.min.css') }}">
        <!-- Helpers for inline customizations -->
        <link rel="stylesheet" href="{{ asset('assets/basecamp/css/helpers.css')}}">
        <!-- Custom CSS files that were added -->
        {!! Basecamp::render_css_includes() !!}

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery 2.1.4 -->
        <script src="{{ asset('assets/basecamp/bower_components/jquery/dist/jquery.js') }}"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="{{ asset('assets/basecamp/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- SlimScroll -->
        <script src="{{ asset('assets/basecamp/js/slimScroll/jquery.slimscroll.min.js') }}"></script>
        <!-- FastClick -->
        <script src="{{ asset('assets/basecamp/js/fastclick/fastclick.min.js') }}"></script>
        <!-- Select2 -->
        <script src="{{ asset('assets/basecamp/bower_components/select2/dist/js/select2.min.js') }}"></script>
        <!-- Input mask -->
        <script src="{{ asset('assets/basecamp/js/input-mask/jquery.inputmask.js') }}"></script>
        <script src="{{ asset('assets/basecamp/js/input-mask/jquery.inputmask.numeric.extensions.js') }}"></script>
        <script src="{{ asset('assets/basecamp/js/input-mask/jquery.inputmask.phone.extensions.js') }}"></script>
        <script src="{{ asset('assets/basecamp/js/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
        <script src="{{ asset('assets/basecamp/js/input-mask/jquery.inputmask.extensions.js') }}"></script>
        <!-- Colorpicker -->
        <script src="{{ asset('assets/basecamp/js/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
        <!-- WYSIWYG -->
        <script src="{{ asset('assets/basecamp/js/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <!-- Charts & stuffs -->
        <script src="{{ asset('assets/basecamp/js/chartjs/Chart.min.js') }}"></script>
        <script src="{{ asset('assets/basecamp/js/raphael/raphael-min.js') }}"></script>
        <script src="{{ asset('assets/basecamp/js/morris/morris.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('assets/basecamp/js/AdminLteApp.min.js') }}"></script>
        <script src="{{ asset('assets/basecamp/js/AdminLTE.js') }}"></script>
        <!-- Custom JS files that were added -->
        {!! Basecamp::render_js_includes() !!}
    </head>
    <body class="hold-transition skin-{{ $skin }} sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="{{ route('basecamp.dashboard') }}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">{!! $title_small !!}</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">{!! $title !!}</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="//irm-client.angelus.io/">
                                    <i class="fa fa-home mar-r-5"></i>
                                    Client
                                </a>
                            </li>
                              <li>
                                  <a href="//irm-dashboard.angelus.io/">
                                      <i class="fa fa-home mar-r-5"></i>
                                      Admin
                                  </a>
                              </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <section class="sidebar">
                    {!! Basecamp::render_menu() !!}
                </section>
            </aside>
            <!-- End of left side column -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="content body">
                    @yield('content')
                </div>
            </div>
            <!-- End of content wrapper -->

            <!-- Footer
            <footer class="main-footer" style="background:transparent; border:0px;display:none;">
                {!! $footer_text !!}
            </footer>-->
            <!-- End of footer -->
        </div>
        <!-- End of wrapper -->
    </body>
</html>
