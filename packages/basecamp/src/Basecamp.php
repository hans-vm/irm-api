<?php

namespace Verheijen\Basecamp;

use Request;

class Basecamp {

    private $js_files;
    private $css_files;
    private $menu_items;

    /*
     | Constructor
     | Initializes all the private variables
     |
     | @return          void
    */
    public function __construct()
    {
        $this->js_files = [];
        $this->css_files = [];
        $this->menu_items = [];
    }

    /*
     | Add a link to our collection
     |
     | @param           String          Text that will be displayed
     | @param           String          URL the link should point to
     | @param           String          Font awesome icon name
     | @param           Array           Array of sublinks with the same four parameters/properties that this function takes
     | @return          void
    */
    public function add_link_to_menu($category, $text, $url, $icon = null, $children = null)
    {
        if( ! array_key_exists($category, $this->menu_items) )
        {
            $this->menu_items[$category] = [];
        }

        $this->menu_items[$category][] = [
            'text' => $text,
            'url' => $url,
            'icon' => $icon,
            'children' => $children
        ];
    }

    /*
     | Add a JS file to be loaded
     | This saves you the trouble of editing the template's HTML
     |
     | @param           String          URL of the JS file
     | @return          void
    */
    public function add_js_file($path)
    {
        $this->js_files[] = $path;
    }

    /*
     | Add a CSS file to be loaded
     | This saves you the trouble of editing the template's HTML
     |
     | @param           String
     | @return          void
    */
    public function add_css_file($path)
    {
        $this->css_files[] = $path;
    }

    /*
     | Render menu
     |
     | @return          html
    */
    public function render_menu()
    {
        // Open the menu
        $html = "<ul class='sidebar-menu'>";
        // Loop through all of the menu items
        foreach( $this->menu_items as $category => $data )
        {
            // Render the header
            $html .= "<li class='header'>".$category."</li>";
            // Loop through all of the menu items
            foreach( $data as $item )
            {
                // If this menu item does not have any children (so it's a regular link)
                if(is_null($item['children']))
                {
                    // Determine if the opening tag should have the active class or not
                    $opening_tag = $this->is_current_page($item['url']) ? "<li class='active'>" : "<li>";
                    // Render the opening tag
                    $html .= $opening_tag;
                    // Open the link
                    $html .= "<a href='".$item['url']."'>";
                        // If an icon was set render it
                        if(!is_null($item['icon'])) $html .= "<i class='fa fa-".$item['icon']."'></i> ";
                        // Render the anchor's text
                        $html .= "<span>".$item['text']."</span>";
                    // Close the anchor and list item
                    $html .= "</a></li>";
                }
                // If this menu does have children (so it's a dropdown link)
                else
                {
                    // Determine which class(es) the list item tag should have
                    $class = $this->in_the_bakery() ? "treeview active" : "treeview";
                    // Render the list item opening tag
                    $html .= "<li class='".$class."'>";
                    // Open the anchor tag
                    $html .= "<a href='".$item['url']."'>";
                        // If an icon was set render it
                        if(!is_null($item['icon'])) $html .= "<i class='fa fa-".$item['icon']."'></i> ";
                        // Render the anchor's text
                        $html .= "<span>".$item['text']."</span>";
                        // Render the carrot icon
                        $html .= "<i class='fa fa-angle-left pull-right'></i>";
                    // Close the anchor tag
                    $html .= "</a>";
                    // Render the dropdown list
                    $html .= "<ul class='treeview-menu'>";
                        // Loop through all of the link's children
                        foreach( $item['children'] as $child )
                        {
                            // Render the list item and the child's anchor
                            $html .= "<li><a href='".$child['url']."'><i class='fa fa-".$child['icon']."'></i>".$child['text']."</a></li>";
                        }
                    // Close the treeview list and the link's list item
                    $html .= "</ul></li>";
                }
            }
        }
        // Close the menu's list
        $html .= "</ul>";

        // And finally return the rendered html
        return $html;
    }

    /*
     | Render the js file include html
     |
     | @return          html
    */
    public function render_js_includes()
    {
        $html = "";
        if( count($this->js_files) > 0 )
        {
            foreach( $this->js_files as $js_file )
            {
                $html .= "<script src='".$js_file."'></script>";
            }
        }
        return $html;
    }

    /*
     | Render the css file include html
     |
     | @return          html
    */
    public function render_css_includes()
    {
        $html = "";
        if( count($this->css_files) > 0 )
        {
            foreach( $this->css_files as $css_file )
            {
                $html .= "<link rel='stylesheet' href='".$css_file."' />";
            }
        }
        return $html;
    }

    /*
     | In the bakery
     | Are we currently on one of the B.R.E.A.D pages?
     |
     | @return          boolean
    */
    public function in_the_bakery()
    {
        $segment_one = Request::segment(1);
        $segment_two = Request::segment(2);
        if( $segment_one == "bakery" )
        {
            $pages = ["browse", "read", "add", "edit", "delete"];
            if( in_array($segment_two, $pages) )
            {
                return true;
            }
        }
        return false;
    }

    /*
     | Is on link's page
     | Are we currently on the page the link is referring to?
     |
     | @return          boolean
    */
    public function is_current_page($url)
    {
        return Request::url() == $url;
    }

}
