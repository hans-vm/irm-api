<?php

namespace Verheijen\Basecamp\Controllers;

use Auth;
use Basecamp;

use App\Http\Controllers\Controller;

/*
 | Basecamp controller
 | This is the controller that all of the controllers that act within your basecamp should extend
*/
class BasecampController extends Controller {

    public function __construct()
    {
        # If any middleware have been set call them
        $middleware = config('basecamp.middleware');
        if( is_array($middleware) and count($middleware) > 0 ) $this->middleware($middleware);
    }

}
