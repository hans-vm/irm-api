<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Cache;

class IRMFlushCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'irm:flushcache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush all laravel cache.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Cache::flush();
        $this->info("Cache flushed.");
    }
}
