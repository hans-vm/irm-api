<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use JWTAuth;
use App\Models\User;

class UserRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role role names separated by | symbol
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        // get current authenticated user
        $user = JWTAuth::parseToken()->authenticate();
        if (!$user) {
            return response()->json(['code' => 401, 'message' => 'Not authorized'], 401);
        }
        
        // if admin, give all permissions
        if ($user->isAdmin()) {
            return $next($request);
        }

        $userRoleName = $user->getRoleName();
        $roles = explode('|', $role);

        // matching role is found
        if (in_array($userRoleName, $roles)) {
            return $next($request);
        }

        // if there's a referring user (i.e. used loginAs), check referrer roles also.
        if ( $refId = JWTAuth::getPayload()->get('referrer') ) {
            $referrer = User::find($refId);
            if ($referrer) {
                // if admin, give all permissions
                if ($referrer->isAdmin()) {
                    return $next($request);
                }

                $referrerRoleName = $referrer->getRoleName();

                // matching role is found
                if (in_array($referrerRoleName, $roles)) {
                    return $next($request);
                }
            }
        }
        
        return response()->json(['code' => 401, 'message' => 'Not authorized', 'user'=>$user->toArray()], 401);
    }
}
