<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\UserSettings;
use App\Models\SettingsField;
use App\Models\SettingsFieldType;
use App\Models\SettingsFieldGroup;

class SettingsController extends BaseAPIController
{

    /**
     * Returns model information
     *
     * @return \Illuminate\Http\Response
     */
    public function info() {

        try {
            // TODO: should implement caching
            // Settings Groups grouped by category
            $allGroups = SettingsFieldGroup::all()->groupBy('category')->map(function ($groups) {
                return [
                    'groups' => SettingsFieldGroup::GetModelCollectionValues($groups)
                ];
            })->toArray();
            
            // Settings fields grouped by category
            $allFields = SettingsField::all()->groupBy('group.category')->map(function ($fields) {
                return [
                    'fields' => SettingsField::GetModelCollectionValues($fields)
                ];
            })->toArray();

            return $this->sendSuccess(array_merge_recursive(['name' => 'settings'], $allGroups, $allFields));

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        // Retrieve all user settings and group it by category
        $settings = $authUser->getSettings()->groupBy('group.category')->map(function ($fields, $category) {
            return $fields->map(function ($field) {
                return [
                    'id'    =>  $field->id,
                    'value' =>  isset($field->pivot) ? $field->pivot->value : ""
                ];
            });
        })->toArray();

        // manually populate login/email field
        $settings['login'] = [ 'email' => $authUser->email ];

        return $this->sendSuccess($settings);
    }

    /**
     * Update the User Settings
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        $data = $request->only('data');

        // Validate request parameters
        $validator = \Validator::make($data, [
            'data'             => 'required',
            'data.*.id'        => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        try {
            // TODO: should validate each settings field to check if it really falls under user category
            // iterate through data and update accordingly
            foreach ($data['data'] as $dataToUpdate) {
                $authUser->saveSetting($dataToUpdate['id'], isset($dataToUpdate['value']) ? $dataToUpdate['value'] : "");
            }

            return $this->sendSuccess();

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }
    }

    /**
     * Update the Communication Settings
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int id
     * @return \Illuminate\Http\Response
     */
    public function updateCommunication(Request $request, $id)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        $data = $request->only('value');

        // Validate request parameters
        $validator = \Validator::make($data, [
            'value'             => 'required|boolean',
        ]);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        try {
            // TODO: should validate field id to check if it really falls under communication category
            $authUser->saveSetting($id, $data['value']);

            return $this->sendSuccess();

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }
    }

    /**
     * Update the Login Settings
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateLogin(Request $request)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        $userData = $request->only('email', 'password');

        $validationList = [
            'email'         =>  'email|unique:irm_users,email,' . $authUser->id,
            'password'      =>  'required|min:6'
        ];

        // Validate request parameters
        $validator = \Validator::make($userData, $validationList);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        try {
        
            // Update user email & password
            if (isset($userData['email'])) {
                $authUser->email = $userData['email'];
            }
            $authUser->password = bcrypt($userData['password']);
            $authUser->save();

            // all good
            return $this->sendSuccess();   

        } catch (JWTException $e) {
            return $this->sendServerError('Could not update login settings');
        }

    }
}
