<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Ticket;

class TicketController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Ticket';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();
        $tickets = $authUser->tickets;

        return $this->sendSuccess(Ticket::GetModelCollectionValues($tickets));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        $ticket = $authUser->tickets()->find($id);

        if (!$ticket) {
            return $this->sendNotFound('Ticket is not found.');
        }

        return $this->sendSuccess($ticket->getModelFieldValues(true));
    }

    /**
     * Display contact form information
     *
     * @return \Illuminate\Http\Response
     */
    public function contactInfo()
    {
        $authUser = $this->checkAuth();

        return $this->sendSuccess([
                'name' => 'contact',
                'ticket' => [
                    'email' => $authUser->email,
                    'phone' => $authUser->getSetting('phone_mobile')
                ],
                'information' => [
                    'email' => \Config::get('app.SupportEmailAddress'),
                    'phone' => \Config::get('app.SupportPhoneNumber'),
                ]
            ]);
    }

    /**
     * Get form values from POST data and returns as array
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array form data 
     */
    protected function getFormPostData(Request $request) {

        // get fields list
        $formData = parent::getFormPostData($request);

        $authUser = $this->checkAuth();
        $formData['user_id'] = $authUser->id;

        return $formData;
    }

    /**
     * Create a ticket
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        // get authenticated user
        $authUser = $this->checkAuth();

        // get form data
        $formData = $this->getFormPostData($request);

        // get validator
        $formValidator = $this->formDataValidator($request, $formData);

        // validate input
        if ($formValidator->fails()) {
            return $this->sendInvalidRequest($formValidator->errors()->first());
        }

        // Validate ticketables input
        if (isset($request['ticketables'])) {

            // gather ticketable data
            $ticketables = $request['ticketables'];

            // ticketables validation list
            $validationList = [
                '*.type'    =>  'required|in:' . implode(',', Ticket::$TICKETABLE_TYPES),
                '*.id'      =>  'required|numeric'
            ];

            // ticketables validator
            $ticketableValidator = \Validator::make($ticketables, $validationList);

            if ($ticketableValidator->fails()) {
                return $this->sendInvalidRequest($ticketableValidator->errors()->first());
            }
        }
        
        try {

            // create new object or update with form data
            $object = new Ticket;
            $object->updateModelWithFormData($formData);

            // if ticketables are provided, associate them with the newly created object
            if (!empty($ticketables)) {

                // Group by `type` for optimization
                collect($ticketables)->groupBy('type')->each(function($ticketables, $type) use ($object) {
                    // Associate ids
                    $object->linkOfType($type)->sync($ticketables->pluck('id')->toArray());
                });
            }

            // TODO: Should send out support email
                        
            return $this->sendSuccess($object->getModelFieldValues());

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }

    }

}
