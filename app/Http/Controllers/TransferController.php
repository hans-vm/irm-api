<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Transfer;
use App\Models\AccountTransaction;
use App\Models\InvestmentTransaction;

use DB;

class TransferController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Transfer';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        // TODO: Should return relevant transfers only
        $transfers = Transfer::all();
        
        return $this->sendSuccess(Transfer::GetModelCollectionValues($transfers));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();

        // TODO: Should check if relevant
        $transfer = Transfer::find($id);

        if (!$transfer) {
            return $this->sendNotFound('Transfer is not found.');
        }

        return $this->sendSuccess($transfer->getModelFieldValues(true));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        // get form data
        $formData = $this->getFormPostData($request);

        // get validator
        $formValidator = $this->formDataValidator($request, $formData);

        // validate input
        if ($formValidator->fails()) {
            return $this->sendInvalidRequest($formValidator->errors()->first());
        }

        // Create new Transfer object
        $object = new Transfer;
        $object->setAttributesWithFormData($formData);

        $source = $object->source;
        if (!$source) {
            return $this->sendInvalidRequest("Source not found.");
        }

        $destination = $object->destination;
        if (!$destination) {
            return $this->sendInvalidRequest("Destination not found.");
        }

        $amount = $formData['amount'];
        if ($source->getBalance() < $amount) {
            return $this->sendInvalidRequest("Insufficient funds.");    
        }

        // Create a Transfer, Source Transaction, Destination Transaction
        try {

            DB::beginTransaction();
            
            $sourceTransaction = $source->addTransaction( $source->getObjectDescription(), 'Transfer', 'Completed', -$amount );

            $destinationTransaction = $destination->addTransaction( $destination->getObjectDescription(), 'Transfer', 'Completed', $amount );
            
            $object->save();

            // Associate the source & destination transactions
            // TODO: Move this logic to Models?
            if ($sourceTransaction instanceof AccountTransaction) $accountTransaction = $sourceTransaction;
            if ($sourceTransaction instanceof InvestmentTransaction) $investmentTransaction = $sourceTransaction;
            if ($destinationTransaction instanceof AccountTransaction) $accountTransaction = $destinationTransaction;
            if ($destinationTransaction instanceof InvestmentTransaction) $investmentTransaction = $destinationTransaction;

            if (isset($accountTransaction) && isset($investmentTransaction)) {
                $investmentTransaction->accountTransaction()->associate($accountTransaction);
                $investmentTransaction->save();
            }

            DB::commit();
            
        } catch (\Exception $ex) {

            DB::rollBack();

            return $this->sendServerError($ex->getMessage());
        }
       
        return $this->sendSuccess($object->getModelFieldValues());
    }
}
