<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\ActivityLog;

class ActivityLogController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\ActivityLog';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();
        
        $logs = array();

        if ($authUser->isAdmin()) {
            $logs = ActivityLog::with('user')->get();
        } else {
            $logs = ActivityLog::with('user')->where('user_id', $authUser->id)->get();
        }
        
        return $this->sendSuccess(ActivityLog::GetModelCollectionValues($logs));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->sendNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        $log = ActivityLog::with('user')->find($id);

        if (!$log) {
            return $this->sendNotFound('Activity log is not found.');
        }

        return $this->sendSuccess($log->getModelFieldValues());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->sendNotImplemented();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->sendNotImplemented();
    }

}
