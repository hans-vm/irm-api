<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Project;

class ProjectController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Project';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        $projects = Project::all();
        
        return $this->sendSuccess(Project::GetModelCollectionValues($projects));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        
        $project = Project::find($id);

        if (!$project) {
            return $this->sendNotFound('Project is not found.');
        }

        return $this->sendSuccess($project->getModelFieldValues(true));
    }
}
