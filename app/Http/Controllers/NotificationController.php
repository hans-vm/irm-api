<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Notification;
use DB;

class NotificationController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Notification';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        return $this->sendSuccess(Notification::GetModelCollectionValues($authUser->notifications));
    }

    /**
     * Display a listing of the alert notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function alert()
    {
        $authUser = $this->checkAuth();

        $notifications = $authUser->notifications()->where('display_mode_alert', 1)->get();

        return $this->sendSuccess(Notification::GetModelCollectionValues($notifications));
    }

    /**
     * Display a listing of the menu notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function menu()
    {
        $authUser = $this->checkAuth();

        $notifications = $authUser->notifications()->where('display_mode_menu', 1)->get();

        return $this->sendSuccess(Notification::GetModelCollectionValues($notifications));
    }

    /**
     * Display a listing of the sidebar notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function sidebar()
    {
        $authUser = $this->checkAuth();

        $notifications = $authUser->notifications()->where('display_mode_sidebar', 1)->get();

        return $this->sendSuccess(Notification::GetModelCollectionValues($notifications));
    }

    /**
     * Display a listing of the email notifications.
     *
     * @return \Illuminate\Http\Response
     */
    public function email()
    {
        $authUser = $this->checkAuth();

        $notifications = $authUser->notifications()->where('display_mode_email', 1)->get();

        return $this->sendSuccess(Notification::GetModelCollectionValues($notifications));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        $notification = $authUser->notifications()->find($id);

        if (!$notification) {
            return $this->sendNotFound('Notification is not found.');
        }

        return $this->sendSuccess($notification->getModelFieldValues(true));
    }

    /**
     * Get form values from POST data and returns as array
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array form data 
     */
    protected function getFormPostData(Request $request) {

        // get fields list
        $formData = parent::getFormPostData($request);

        $authUser = $this->checkAuth();
        $formData['created_by'] = $authUser->id;

        return $formData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        // get form data
        $formData = $this->getFormPostData($request);

        // get validator
        $formValidator = $this->formDataValidator($request, $formData);

        // validate input
        if ($formValidator->fails()) {
            return $this->sendInvalidRequest($formValidator->errors()->first());
        }

        // Create new Notification object
        $object = new Notification;
        $object->setAttributesWithFormData($formData);
        
        $notifiable = $object->notifiable;
        if (!$notifiable) {
            return $this->sendInvalidRequest("Notifiable not found.");
        }
      
        // Create a Notification and attach related users
        try {

            DB::beginTransaction();
            
            $object->save();

            // Attach users to the notification
            switch ($object->notifiable_type) {
                case 'User':   
                    $object->users()->attach($notifiable);
                    break;
                case 'Investor':   
                    $object->users()->attach($notifiable->user);
                    break;
                case 'Account':   
                case 'Investment':   
                    $object->users()->attach($notifiable->investor->user);
                    break;
                case 'Project':
                case 'Update':
                    $object->users()->attach($notifiable->getAllRelatedUserIds());
                    break;

                default:
                    throw new \Exception("Invalid notifiable type");
                    break;
            }

            DB::commit();
            
        } catch (\Exception $ex) {

            DB::rollBack();

            return $this->sendServerError($ex->getMessage());
        }
       
        return $this->sendSuccess($object->getModelFieldValues());
    }

    /**
     * Mark specified Notification as read by user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function markAsRead($id)
    {        
        return $this->markAs($id, true);
    }

    /**
     * Mark specified Notification as unread by user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function markAsUnread($id)
    {
        return $this->markAs($id, false);
    }

    /**
     * Mark specified Notification as read/unread
     * @param  int  $id   Notification id
     * @param  boolean $read Read/Unread
     * @return Response
     */
    private function markAs($id, $read = false) 
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        try {

            // Update is_read field on the pivot table
            $authUser->notifications()->updateExistingPivot($id, ['is_read' => $read]);

            return $this->sendSuccess();

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }
    }
}
