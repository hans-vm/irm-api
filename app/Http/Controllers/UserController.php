<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\User;
use App\Models\ActivityLog;

use JWTAuth;
use JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Contracts\Auth\Guard;

class UserController extends BaseAPIController
{

    use ResetsPasswords;

    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\User';


    public function __construct(Guard $auth, PasswordBroker $passwords) {
        $this->auth = $auth;
        $this->passwords = $passwords;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();
        $users = User::with('role')->get();
        return $this->sendSuccess(User::GetModelCollectionValues($users));
    }

    /**
     * Create new user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        return $this->createNewUser($request, false);
    }

    /**
     * Register new user from front-end
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {
        return $this->createNewUser($request, true);
    }


    /**
     * Register new user to the database
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function createNewUser(Request $request, $frontEnd)
    {

        $userData = $request->only('first_name', 'last_name', 'email', 'password', 'phone', 'street', 'city', 'zip', 'country', 'agree_terms', 'role');

        $validationList = [
            'email'         => 'required|email',
            'password'      => 'required|min:6'
        ];

        if ($frontEnd) {
            $validationList['agree_terms'] = 'required';
        }

        // Validate request parameters
        $validator = \Validator::make($userData, $validationList);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        if ($frontEnd) {
            if ($userData['agree_terms'] != '1') {
                return $this->sendInvalidRequest("You must agree to our Terms of Use.");
            }
        }

        try {


            // check if such email address is already existing
            $dupUser = User::GetUserByEmail($userData['email'], true);
            if ($dupUser) {
                return $this->sendInvalidRequest("E-mail address is already in use.");
            }

            // create new user 
            $newUser = User::create([
                'first_name'        => $userData['first_name'],
                'last_name'         => $userData['last_name'],
                'email'             => $userData['email'],
                'password'          => bcrypt($userData['password']),
                'user_role_id'      => empty($userData['role']) ? User::GetUserRoleIDByName('Client') : $userData['role'],
                'enabled'           => 0                // default - always disable this user
            ]);

            return $this->sendSuccess($newUser->getModelFieldValues());

        } catch (JWTException $e) {
            return $this->sendServerError('Could not create token');
        } catch (\Exception $e) {
            return $this->sendServerError($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        $user = User::with('role')->find($id);

        if (!$user) {
            return $this->sendNotFound('User is not found.');
        }

        return $this->sendSuccess($user->getModelFieldValues(true));
    }

    /**
     * Returns the currently logged in user information
     *
     * @return \Illuminate\Http\Response
     */
    public function self()
    {
        $authUser = $this->checkAuth();
        return $this->sendSuccess($authUser->getModelFieldValues());
    }

    /**
     * Authenticate user and returnes new JWT Token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        // Validate request parameters
        $validator = \Validator::make($credentials, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->sendNotAuthorized('Invalid credentials.');
            }
        } catch (JWTException $e) {
            return $this->sendServerError('Could not create token');
        }

        // log user login
        $user = JWTAuth::toUser($token);
        ActivityLog::log($user, ActivityLog::$LOG_ACTION_LOGIN);

        // all good so return the token
        return $this->sendSuccess(compact('token'));
    }


    /**
     * Send user password reset link email
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function requestPasswordReset(Request $request) {

        $emails = $request->only('email', 'confirm_email');

        // Validate request parameters
        $validator = \Validator::make($emails, [
            'email'             => 'required|email'
        ]);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        $response = $this->passwords->sendResetLink($request->only('email'), function($message) {
            $message->subject('Password Reminder');
            $message->from(\Config::get('app.SupportEmailAddress'));
        });

        switch ($response) {
            case PasswordBroker::RESET_LINK_SENT: 
                return $this->sendSuccess();
            case PasswordBroker::INVALID_USER:
                return $this->sendNotFound('Such user does not exist in our database.');
            default:
                return $this->sendServerError('Email can not be sent.');
        }

        return $this->sendServerError('Unknown error.');
    }


    /**
     * Logout user by invalidating token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {

        // log user logout
        $user = JWTAuth::toUser();
        ActivityLog::log($user, ActivityLog::$LOG_ACTION_LOGOUT);

        // store referrer id before invalidating token
        $refId = JWTAuth::getPayload()->get('referrer');

        JWTAuth::invalidate(JWTAuth::getToken());

        if ($refId) {
            // if there's a referrer, create a new token for the referrer and return it.
            if ( $referrer = User::find($refId) ) {
                if ( $token = JWTAuth::fromUser($referrer, ['referrer' => '']) ) {
                    return $this->sendSuccess(compact('token'));
                }
            }
        }
        
        return $this->sendSuccess();

    }


    /**
     * Login as specified User
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id     User id
     * @return \Illuminate\Http\Response
     */
    public function loginAs(Request $request, $id) {

        $user = User::find($id);

        if (!$user) {
            return $this->sendNotFound('Such user does not exist in our database.');
        }

        try {
            // keep the referrer id if there's any already.
            $refId = JWTAuth::getPayload()->get('referrer');
            if (!$refId) {
                // or keep the current user id
                if ( $currentUser = $this->checkAuth() ) {
                    $refId = $currentUser->id;
                }    
            }

            // attempt to create a token for the user
            if (! $token = JWTAuth::fromUser($user, ['referrer' => $refId]) ) {
                return $this->sendServerError('Could not create token');
            }
        } catch (JWTException $e) {
            return $this->sendServerError('Could not create token');
        }

        // all good so return the token
        return $this->sendSuccess(compact('token'));
    }

    /**
     * Returns the time remaining in milliseconds till the token expiration
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ping(Request $request) {

        $authUser = $this->checkAuth();

        // Get expiration time from the token
        $expirationTime = JWTAuth::getPayload()->get("exp");

        // Get time to live in milliseconds
        $ttl = ($expirationTime - time()) * 1000;

        return $this->sendSuccess(compact('ttl'));
    }

    /**
     * Refreshes the token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function refreshToken(Request $request) {

        $authUser = $this->checkAuth();

        // Refreshes the token
        $token = JWTAuth::refresh();

        return $this->sendSuccess(compact('token'));
    }

    /**
     * Reset password - For Admin Only
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id     User id
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request, $id) {

        $user = User::find($id);

        if (!$user) {
            return $this->sendNotFound('Such user does not exist in our database.');
        }

        $userData = $request->only('email', 'password');

        $validationList = [
            'email'         =>  'email|unique:irm_users,email,' . $id,
            'password'      =>  'required|min:6'
        ];

        // Validate request parameters
        $validator = \Validator::make($userData, $validationList);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        try {
        
            // Update user email & password
            if (isset($userData['email'])) {
                $user->email = $userData['email'];
            }
            $user->password = bcrypt($userData['password']);
            $user->save();

            // TODO: Should send the user a password changed notification mail
            
            // all good
            return $this->sendSuccess();   

        } catch (JWTException $e) {
            return $this->sendServerError('Could not reset password');
        }
    }
}
