<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Models\Investor;
use App\Models\InvestorDocument;

use File;
use Storage;
use Uuid;

class InvestorDocumentController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\InvestorDocument';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        // find all documents related to the investor
        $documents = InvestorDocument::get();

        return $this->sendSuccess(Investor::GetModelCollectionValues($documents));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        $document = InvestorDocument::find($id);

        // document should be of the investor specified by investor_id
        if (!$document) {
            return $this->sendNotFound('Document not found.');
        }

        return $this->sendSuccess($document->getModelFieldValues(true));
    }

    /**
     * Download the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $authUser = $this->checkAuth();
        $document = InvestorDocument::find($id);

        // document should be of the investor specified by investor_id
        if (!$document) {
            return new Response("", 404);
        }

        // get file contents from storage
        $file = Storage::disk($document->disk)->get($document->filename);

        // write file contents to the output with appropriate headers
        // to force browsers recognize this content as a downloadable file
        return (new Response($file, 200))
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-Disposition', 'attachment; filename="' . $document->name . '"')
            ->header('Expires', '0')
            ->header('Cache-Control', 'must-revalidate')
            ->header('Pragma', 'public')
            ->header('Content-Length', $document->size);
    }

    /**
     * Store the uploaded document
     *
     * @param  Request  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('file', 'investor_id');

        $validationList = [
            'file'         => 'required',
            'investor_id'  => 'required'
        ];

        // Validate request parameters
        $validator = \Validator::make($data, $validationList);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        // UploadedFile
        $file = $request->file('file');

        // Original file name
        $name = $file->getClientOriginalName();

        // Original file extension
        $extension = $file->getClientOriginalExtension();
        
        // Name to be saved on the server
        $fileName = $file->getFilename() . '.' . $extension;

        // File type
        $type = $file->getClientOriginalExtension();
        if ($type) {
            $type = $file->guessExtension();
        }
        $type = $type ? strtoupper($type) : "N/A";

        // File size
        $size = $file->getClientSize();

        // Current storage disk option
        $disk = config('filesystems.default');
        
        // Save file
        if (!Storage::disk($disk)->put($fileName, File::get($file))) {
            return $this->sendServerError("Could not save file.");
        }

        try {

            // Save InvestorDocument
            $document = InvestorDocument::create([
                'investor_id'   =>  $data['investor_id'],
                'name'          =>  $name,
                'filename'      =>  $fileName,
                'disk'          =>  $disk,
                'type'          =>  $type,
                'size'          =>  $size,
                'uuid'          =>  Uuid::uuid1()
            ]);

            return $this->sendSuccess($document->getModelFieldValues());

        } catch (\Exception $e) {
            return $this->sendServerError($e->getMessage());
        }

    }

}
