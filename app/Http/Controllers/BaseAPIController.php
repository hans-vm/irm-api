<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class BaseAPIController extends Controller
{
 
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\BaseModel';

    /**
     * Send JSON Response with Status code
     *
     * @param int $status       HTTP Status Code
     * @param array $data       JSON Data to send
     * @return \Illuminate\Http\Response
     */
    protected function _sendJSON($status, $data) {
        return response()->json($data, $status);
    }


    /**
     * Send success response with JSON data
     *
     * @param array $data       Data payload to send
     * @return \Illuminate\Http\Response
     */
    protected function sendSuccess($data = null) {
        $jsonData = ['code' => 0, 'message' => 'success'];
        if ($data) {
            $jsonData['data'] = $data;
        }
        return $this->_sendJSON(200, $jsonData);
    }


    /**
     * Send JSON data with status code 201 -- (For PUT requests) object was created
     *
     * @param array $data       Data payload to send
     * @return \Illuminate\Http\Response
     */
    protected function sendCreated($data) {
        return $this->_sendJSON(201, ['code' => 0, 'message' => 'success', 'data' => $data]);
    }

    /**
     * Send JSON data with status code 204 -- NO Content
     *
     * @return \Illuminate\Http\Response
     */
    protected function sendNoContent() {
        return $this->_sendJSON(204, []);
    }


    /**
     * Send HTTP Status code 400 - Parameters or body is missing required data (Invalid Request)
     *
     * @param string $msg       Custom message to send
     * @return \Illuminate\Http\Response
     */
    protected function sendInvalidRequest($msg = '') {
        if (empty($msg)) $msg = "Invalid Request.";
        return $this->_sendJSON(400, ['code' => 400, 'message' => $msg]);
    }


    /**
     * Send HTTP Status code 404 - Object not found
     *
     * @param string $msg       Custom message to send
     * @return \Illuminate\Http\Response
     */
    protected function sendNotFound($msg = '') {
        if (empty($msg)) $msg = "Object is not found.";
        return $this->_sendJSON(404, ['code' => 404, 'message' => $msg]);
    }


    /**
     * Send HTTP Status code 401 - Not Authorized
     *
     * @param string $msg       Custom message to send
     * @return \Illuminate\Http\Response
     */
    protected function sendNotAuthorized($msg = '') {
        if (empty($msg)) $msg = "You are not authorized to perform this operation.";
        return $this->_sendJSON(401, ['code' => 401, 'message' => $msg]);
    }


    /**
     * Send HTTP Status code 500 - Server Error
     *
     * @param string $msg       Custom message to send
     * @return \Illuminate\Http\Response
     */
    protected function sendServerError($msg = '') {
        if (empty($msg)) $msg = "Server error has occurred.";
        return $this->_sendJSON(500, ['code' => 500, 'message' => $msg]);
    }


    /**
     * Send HTTP Status code 501 - Not Implemented
     *
     * @param string $msg       Custom message to send
     * @return \Illuminate\Http\Response
     */
    protected function sendNotImplemented($msg = '') {
        if (empty($msg)) $msg = "This function is not implemented yet.";
        return $this->_sendJSON(501, ['code' => 501, 'message' => $msg]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->sendNotImplemented();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->sendNotImplemented();
    }

    /**
     * Get JWT Authenticated User
     *
     * @return \App\User | null
     * @throws \Exception
     */
    protected function getAuthenticatedUser() {
        $user = JWTAuth::parseToken()->authenticate();
        return $user;
    }


    /**
     * Check whether user is authenticated or not
     *
     * @return \App\User    Authenticated user object
     * @throws \App\Exceptions\NotAuthenticatedException
     */
    protected function checkAuth() {
        $user = $this->getAuthenticatedUser();
        if (!$user) {
            throw new \App\Exceptions\NotAuthenticatedException();
        }
        return $user;
    }


    /**
     * Returns model information
     *
     * @return \Illuminate\Http\Response
     */
    public function info() {

        try {
            $modelInfo = call_user_func(static::$baseModelClass . '::GetModelStructure');
            return $this->sendSuccess($modelInfo);
        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }

    }


    /**
     * Get form values from POST data and returns as array
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array form data 
     */
    protected function getFormPostData(Request $request) {

        // get fields list
        $fields = call_user_func(static::$baseModelClass . '::GetFormPostFieldsList');

        $formData = $request->only($fields);

        // Populate default values for 'select' typed fields from Model Info
        $modelInfo = call_user_func(static::$baseModelClass . '::GetModelStructure');
            
        // get form fields
        $fieldsList = isset($modelInfo['fields']) ? $modelInfo['fields'] : array();

        foreach ($fieldsList as $fieldName => $fieldInfo) {          
            
            // if the field is of 'select' type and it's not set in the $formData, use 'default' value in field info
            if (!empty($fieldInfo) && $fieldInfo['type'] == 'select' && !empty($fieldInfo['values']) && empty($formData[$fieldName])) {

                $formData[$fieldName] = $fieldInfo['default'];
            }
        }

        return $formData;
    }
 

    /**
     * Validate form data
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array $formData  null | Form data array to validate
     * @return \Validator validator having all requirements
     */
    protected function formDataValidator(Request $request, $formData = null) {

        // if formdata is missing, extract them
        if (!$formData) {
            $formData = $this->getFormPostData($request);
        }

        // get form validation list for this model
        $validationList = call_user_func(static::$baseModelClass . '::GetValidationList');

        return \Validator::make($formData, $validationList);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->updateOrCreate($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        // load object by id
        $object = call_user_func(static::$baseModelClass . '::find', $id);

        if (!$object) {
            return $this->sendNotFound();
        }

        return $this->updateOrCreate($request, $object);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        // load object by id
        $object = call_user_func(static::$baseModelClass . '::find', $id);

        if (!$object) {
            return $this->sendNotFound();
        }        

        $object->delete();

        return $this->sendSuccess();
    }

    /**
     * Create or Update resource based on the form data
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Database\Eloquent\Model $object Object to update, null if it should be created
     * @return \Illuminate\Http\Response
     */
    protected function updateOrCreate(Request $request, $object = null) {

        // get authenticated user
        $authUser = $this->checkAuth();

        // get form data
        $formData = $this->getFormPostData($request);

        // get validator
        $formValidator = $this->formDataValidator($request, $formData);

        // validate input
        if ($formValidator->fails()) {
            return $this->sendInvalidRequest($formValidator->errors()->first());
        }

        try {

            // create new object or update with form data
            if (!$object) {
                $object = new static::$baseModelClass;
            }
            $object->updateModelWithFormData($formData);
            return $this->sendSuccess($object->getModelFieldValues());

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }

    }
    
}
