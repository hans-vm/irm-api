<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Account;

class AccountController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Account';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        if ($authUser->isAdmin()) {
            $accounts = Account::with('transactions')->get();    
        } else {
            $accounts = $authUser->hasManyThrough("App\Models\Account", "App\Models\Investor")
                                ->with('transactions')
                                ->get();
        }
        
        return $this->sendSuccess(Account::GetModelCollectionValues($accounts));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();

        if ($authUser->isAdmin()) {
            $account = Account::with('transactions')->find($id);
        } else {
            $account = $authUser->hasManyThrough("App\Models\Account", "App\Models\Investor")
                                ->with('transactions')
                                ->find($id);    
        }
        
        if (!$account) {
            return $this->sendNotFound('Account is not found.');
        }

        return $this->sendSuccess($account->getModelFieldValues(true));
    }
}
