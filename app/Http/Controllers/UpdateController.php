<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Update;
use App\Models\UpdateUserStatus;
use DB;

class UpdateController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Update';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();
        $updates = $authUser->updates;

        return $this->sendSuccess(Update::GetModelCollectionValues($updates));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        $update = $authUser->updates()->find($id);

        if (!$update) {
            return $this->sendNotFound('Update is not found.');
        }

        return $this->sendSuccess($update->getModelFieldValues(true));
    }

    /**
     * Get form values from POST data and returns as array
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array form data 
     */
    protected function getFormPostData(Request $request) {

        // get fields list
        $formData = parent::getFormPostData($request);

        $authUser = $this->checkAuth();
        $formData['publisher_id'] = $authUser->id;

        return $formData;
    }

    /**
     * Mark specified Update as read by user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function markAsRead($id)
    {        
        return $this->markAs($id, true);
    }

    /**
     * Mark specified Update as unread by user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function markAsUnread($id)
    {
        return $this->markAs($id, false);
    }

    /**
     * Mark specified Update as read/unread
     * @param  int  $id   Update id
     * @param  boolean $read Read/Unread
     * @return Response
     */
    private function markAs($id, $read = false) 
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        try {
            
            // Update is_read field on the pivot table
            $authUser->updates()->updateExistingPivot($id, ['is_read' => $read]);

            return $this->sendSuccess();

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }
    }

    /**
     * Link tag with the Update
     * 
     * @param  Request $request Request
     * @param  int  $id         Update Id
     * @return Response         Response
     */
    public function linkTag(Request $request, $id) 
    {
        return $this->handleTag($request, $id, "link");
    }

    /**
     * Unlink tag
     *     
     * @param  Request $request Request
     * @param  int     $id      Update Id
     * @return Response         Response Object
     */
    public function unlinkTag(Request $request, $id) 
    {
        return $this->handleTag($request, $id, "unlink");
    }

    /**
     * Handles the tag related requests
     * 
     * @param  Request $request Request Object
     * @param  int  $id         Update Id
     * @param  string  $action  Action - link/unlink
     * @return Response         Response Object
     */
    private function handleTag(Request $request, $id, $action)
    {
        // get authenticated user
        $authUser = $this->checkAuth();

        $tagData = $request->only('type', 'id');

        $validationList = [
            'type'         => 'required|in:' . implode(',', Update::$UPDATABLE_TYPES),
            'id'           => 'required'
        ];

        // Validate request parameters
        $validator = \Validator::make($tagData, $validationList);

        if ($validator->fails()) {
            return $this->sendInvalidRequest($validator->errors()->first());
        }

        $update = Update::find($id);

        if (!$update) {
            return $this->sendNotFound('Update is not found.');
        }

        try {
            
            if ($action == "link")                 
                $update->linkOfType($tagData['type'])->attach($tagData['id']);
            else if ($action == "unlink")
                $update->linkOfType($tagData['type'])->detach($tagData['id']);

            $update->syncUsers();
            
            return $this->sendSuccess();

        } catch (\Exception $ex) {
            return $this->sendServerError($ex->getMessage());
        }
    }
}
