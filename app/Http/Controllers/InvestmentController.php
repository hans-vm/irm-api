<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Investment;

class InvestmentController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Investment';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        if ($authUser->isAdmin()) {
            $investments = Investment::with('transactions')->get();    
        } else {
            $investments = $authUser->hasManyThrough("App\Models\Investment", "App\Models\Investor")
                                    ->with('transactions')
                                    ->get();
        }
        
        return $this->sendSuccess(Investment::GetModelCollectionValues($investments));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();

        if ($authUser->isAdmin()) {
            $investment = Investment::with('transactions')->find($id);    
        } else {
            $investment = $authUser->hasManyThrough("App\Models\Investment", "App\Models\Investor")
                                ->with('transactions')
                                ->find($id);
        }

        if (!$investment) {
            return $this->sendNotFound('Investment is not found.');
        }

        return $this->sendSuccess($investment->getModelFieldValues(true));
    }
}
