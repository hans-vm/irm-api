<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Investor;

class InvestorController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\Investor';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = $this->checkAuth();

        if ($authUser->isAdmin()) {
            $investors = Investor::with('accounts')->with('investments')->get();
        } else {
            $investors = $authUser->investors()->with('accounts')->with('investments')->get();    
        }
        
        return $this->sendSuccess(Investor::GetModelCollectionValues($investors));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();

        if ($authUser->isAdmin()) {
            $investor = Investor::with('accounts')->with('investments')->find($id);    
        } else {
            $investor = $authUser->investors()->with('accounts')->with('investments')->find($id);
        }

        if (!$investor) {
            return $this->sendNotFound('Investor not found.');
        }

        return $this->sendSuccess($investor->getModelFieldValues(true));
    }
}
