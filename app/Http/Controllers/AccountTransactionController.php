<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\AccountTransaction;

class AccountTransactionController extends BaseAPIController
{
    /** 
     * Base model class name this controller handles
     * 
     */
    protected static $baseModelClass = '\App\Models\AccountTransaction';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->sendNotImplemented();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = $this->checkAuth();
        $accountTransaction = AccountTransaction::with('status')->with('type')->find($id);

        if (!$accountTransaction) {
            return $this->sendNotFound('Transaction is not found.');
        }

        return $this->sendSuccess($accountTransaction->getModelFieldValues());
    }

    /**
     * Get form values from POST data and returns as array
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array form data 
     */
    protected function getFormPostData(Request $request) {

        $formPostData = parent::getFormPostData($request);

        // add one more post field
        $formPostData['account_id'] = $request->input('account_id');

        if (empty($formPostData['amount'])) $formPostData['amount'] = 0;

        return $formPostData;
    }
}
