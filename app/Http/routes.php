<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return response()->json(['code' => 401, 'message' => 'Not authorized'], 401);
});


/**
 * API V1.0 Routes
 *
 */
$API_PREFIX_V1 = "v1/";

Route::group(['prefix' => $API_PREFIX_V1, 'middleware' => 'cors'], function(){

	Route::get('/', function () {
		return response()->json(['code' => 401, 'message' => 'Not authorized'], 401);
	});

	/**
 	 * Routes requiring authentication
 	 *
 	 */
	Route::group(['middleware' => 'jwt-api'], function() {

		/**
		 * Routes for User logout and self
		 *
		 */
		Route::post('user/logout',	'UserController@logout');					// User logout
		Route::get('user/self', 	'UserController@self');						// Get current logged-in user details
		Route::get('user/info',		'UserController@info');						// Get User model info
		
		/**
		 * Routes for Refresh tokens
		 */
		Route::get('user/ping',							'UserController@ping');
		Route::get('user/refresh',						'UserController@refreshToken');

		/**
		 * Routes for Account & Transactions
		 *
		 */
		Route::get('account/info',	'AccountController@info');					// Get account model info
		Route::resource('account', 	'AccountController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);	

		/**
		 * Routes for Account Operation - Account Transactions
		 *
		 */
		Route::group(['prefix' => 'account'], function() {
			Route::get('transaction/info',	'AccountTransactionController@info');		// Get transaction model info							
			Route::resource('transaction', 	'AccountTransactionController', ['only' => ['show', 'store', 'update', 'destroy']]);	// Transaction Operation endpoints
			
		});

		/**
		 * Routes for Investment & Transactions
		 *
		 */
		Route::get('investment/info',	'InvestmentController@info');					// Get investment model info
		Route::resource('investment', 	'InvestmentController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);	

		/**
		 * Routes for Investment Operation - Investment Transactions
		 *
		 */
		Route::group(['prefix' => 'investment'], function() {
			Route::get('transaction/info',	'InvestmentTransactionController@info');		// Get transaction model info							
			Route::resource('transaction', 	'InvestmentTransactionController', ['only' => ['show', 'store', 'update', 'destroy']]);	// Transaction Operation endpoints
			
		});

		/**
		 * Routes for Investors
		 *
		 */
		Route::get('investor/info',	'InvestorController@info');					// Get investor model info
		Route::resource('investor', 	'InvestorController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);	

		/**
		 * Routes for Investor Documents
		 *
		 */
		Route::group(['prefix' => 'investor'], function() {

			Route::get('document/info',	'InvestorDocumentController@info');		// Get investor document model info
			Route::get('document/{id}/download',	'InvestorDocumentController@download');
			
			Route::resource('document', 	'InvestorDocumentController', ['only' => ['index', 'show', 'store', 'destroy']]);	
			
		});
		
		/**
		 * Routes for Security - Activity log
		 *
		 */
		Route::get('activitylog/info',	'ActivityLogController@info');			// Get Activitylog model info
		Route::resource('activitylog', 	'ActivityLogController', ['only' => ['index', 'show']]);			// Activitylog details

		/**
		 * Routes for Project
		 *
		 */
		Route::get('project/info',	'ProjectController@info');					// Get project model info
		Route::resource('project', 	'ProjectController', ['only' => ['index', 'show']]);	

		/**
		 * Routes for Update
		 *
		 */
		Route::get('update/info',	'UpdateController@info');					// Get update model info
		Route::put('update/{id}/markAsRead',	'UpdateController@markAsRead');
		Route::put('update/{id}/markAsUnread',	'UpdateController@markAsUnread');
		Route::resource('update', 	'UpdateController', ['only' => ['index', 'show']]);	

		/**
		 * Routes for Notifications
		 *
		 */
		Route::get('notification/info',	'NotificationController@info');					// Get notification model info
		Route::get('notification/alert',	'NotificationController@alert');					// Get alert notifications
		Route::get('notification/menu',		'NotificationController@menu');						// Get menu notifications
		Route::get('notification/sidebar',	'NotificationController@sidebar');					// Get sidebar notifications
		Route::get('notification/email',	'NotificationController@email');					// Get email notifications
		Route::put('notification/{id}/markAsRead',		'NotificationController@markAsRead');
		Route::put('notification/{id}/markAsUnread',	'NotificationController@markAsUnread');
		Route::resource('notification', 	'NotificationController', ['only' => ['index', 'show']]);	

		/**
		 * Routes for Transfers
		 *
		 */
		Route::get('transfer/info',	'TransferController@info');					// Get transfer model info
		Route::resource('transfer', 	'TransferController', ['only' => ['index', 'show', 'store']]);	

		/**
		 * Routes for Settings
		 *
		 */
		Route::get('settings/info',	'SettingsController@info');					// Get settings model info
		Route::get('settings', 	'SettingsController@index');					// Get all settings data
		Route::put('settings/user', 	'SettingsController@updateUser');		// Update user related settings
		Route::put('settings/communication/{id}', 	'SettingsController@updateCommunication');	// Update communication related settings
		Route::put('settings/login', 	'SettingsController@updateLogin');		// Update login related settings

		/**
		 * Routes for Support
		 *
		 */
		Route::group(['prefix' => 'support'], function() {

			Route::get('ticket/info',	'TicketController@info');		// Get ticket model info		
			Route::resource('ticket', 	'TicketController', ['only' => ['index', 'show', 'store']]);	// Ticket endpoints
			Route::get('contact', 		'TicketController@contactInfo');	// Get all contact form info
			Route::post('contact', 		'TicketController@store');			// Create ticket
			
		});

		/**
		 * Routes for Admin Users  ( Middleware - auth.role )
		 *
		 */
		Route::group(['middleware' => 'auth.role:' . \App\Models\UserRole::$ADMIN_ROLE_NAME ], function(){

			Route::resource('user', 	'UserController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);			// User RESTFul Endpoints

			Route::resource('project', 	'ProjectController', ['only' => ['store', 'update', 'destroy']]);			// Project C/U/D Endpoints (Only permitted to admin)

			Route::post('update/{id}/link',	'UpdateController@linkTag');
			Route::post('update/{id}/unlink',	'UpdateController@unlinkTag');
			Route::resource('update', 	'UpdateController', ['only' => ['store', 'update', 'destroy']]);			// Update C/U/D Endpoints (Only permitted to admin)
		
			Route::resource('notification', 	'NotificationController', ['only' => ['store', 'update', 'destroy']]);			// Notification C/U/D Endpoints (Only permitted to admin)
			Route::post('admin/loginas/{id}', 		'UserController@loginAs');		// Login as user Route
			Route::post('admin/resetPassword/{id}',		'UserController@resetPassword');   // Reset password for the specific user
		});
		

	});


	/**
 	 * Routes for Non-Auth
 	 *
 	 */
	Route::post('user/auth', 						'UserController@authenticate');			// User Login
	Route::post('user/register', 					'UserController@register');				// User SignUp
	Route::post('user/requestPasswordReset', 		'UserController@requestPasswordReset');	// User Password Reset
	
});
