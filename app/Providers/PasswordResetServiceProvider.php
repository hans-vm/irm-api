<?php

namespace App\Providers;

use App\Auth\Passwords\PasswordBroker;
use Illuminate\Auth\Passwords\PasswordResetServiceProvider as IlluminatePasswordResetServiceProvider;
use App\Auth\Passwords\PasswordBrokerManager;


class PasswordResetServiceProvider extends IlluminatePasswordResetServiceProvider
{
	/**
     * Register the password broker instance.
     *
     * @return void
     */
    protected function registerPasswordBroker()
    {
        $this->app->singleton('auth.password', function ($app) {
            return new PasswordBrokerManager($app);
        });

        $this->app->bind('auth.password.broker', function ($app) {
            return $app->make('auth.password')->broker();
        });
    }
}