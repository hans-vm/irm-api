<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class SqliteServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Create SQLite Database if it's not exist
        if (DB::getDriverName() === 'sqlite') {
            $dbPath = DB::getConfig('database');
            if (!file_exists($dbPath) && is_dir(dirname($dbPath))) {
                touch($dbPath);
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
