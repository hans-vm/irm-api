<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // force HTTPS
        if (!\App::environment('local')) {
            \URL::forceSchema('https');    
        }

        Relation::morphMap([
            'User'          =>  \App\Models\User::class,
            'Investor'      =>  \App\Models\Investor::class,
            'Account'       =>  \App\Models\Account::class,
            'Investment'    =>  \App\Models\Investment::class,
            'Project'       =>  \App\Models\Project::class,
            'Update'        =>  \App\Models\Update::class,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
