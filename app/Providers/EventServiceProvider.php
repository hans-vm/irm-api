<?php

namespace App\Providers;

use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\ActivityLog;
use App\Models\Investment;
use App\Models\InvestmentTransaction;
use App\Models\Investor;
use App\Models\Notification;
use App\Models\Project;
use App\Models\SettingsField;
use App\Models\Ticket;
use App\Models\Transfer;
use App\Models\Update;
use App\Models\User;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Uuid;
use \Illuminate\Http\Response;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        // Register JWT Related Events
        $events->listen("tymon.jwt.absent", function () {
            return response()->json(['code' => 400, 'message' => 'token_not_provided'], 400);
        });

        $events->listen("tymon.jwt.expired", function () {
            return response()->json(['code' => 500, 'message' => 'token_expired'], 500);
        });

        $events->listen("tymon.jwt.invalid", function () {
            return response()->json(['code' => 500, 'message' => 'token_invalid'], 500);
        });

        $events->listen("tymon.jwt.user_not_found", function () {
            return response()->json(['code' => 401, 'message' => 'user_not_found'], 401);
        });

        // Set default settings when creating user
        User::created(function($user) {
            $user->settings()->sync(SettingsField::defaults());
        });

        // Register Account UUID Related Events
        Account::creating(function($account) {
            if (empty($account->uuid)) $account->uuid = Uuid::uuid1();
            if (empty($account->account_date)) $account->account_date = time();
        });

        AccountTransaction::creating(function($accountTransaction) {
            if (empty($accountTransaction->uuid)) $accountTransaction->uuid = Uuid::uuid1();
            if (empty($accountTransaction->transaction_date)) $accountTransaction->transaction_date = time();
        });

        // Register Investment UUID Related Events
        Investment::creating(function($investment) {
            if (empty($investment->uuid)) $investment->uuid = Uuid::uuid1();
            if (empty($investment->investment_date)) $investment->investment_date = time();
        });

        InvestmentTransaction::creating(function($investmentTransaction) {
            if (empty($investmentTransaction->uuid)) $investmentTransaction->uuid = Uuid::uuid1();
            if (empty($investmentTransaction->transaction_date)) $investmentTransaction->transaction_date = time();
        });

        // Register Investor UUID Related Events
        Investor::creating(function($investor) {
            if (empty($investor->uuid)) $investor->uuid = Uuid::uuid1();
        });

        // Register Activity Log UUID Related Events
        ActivityLog::creating(function($activityLog) {
            if (empty($activityLog->uuid)) $activityLog->uuid = Uuid::uuid1();
        });

        // Register Update UUID Related Events
        Update::creating(function($update) {
            if (empty($update->uuid)) $update->uuid = Uuid::uuid1();
        });

        // Register Transfer UUID Related Events
        Transfer::creating(function($transfer) {
            if (empty($transfer->uuid)) $transfer->uuid = Uuid::uuid1();
            if (empty($transfer->transfer_date)) $transfer->transfer_date = time();
        });

        // Register Notification UUID Related Events
        Notification::creating(function($notification) {
            if (empty($notification->uuid)) $notification->uuid = Uuid::uuid1();
            if (empty($notification->notification_date)) $notification->notification_date = time();
        });

        // Register Ticket UUID Related Events
        Ticket::creating(function($ticket) {
            if (empty($ticket->uuid)) $ticket->uuid = Uuid::uuid1();
        });
    }
}
