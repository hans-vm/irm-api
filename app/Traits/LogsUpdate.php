<?php

namespace App\Traits;

trait LogsUpdate
{
    
    /**
     * Relationship to Update
     *
     */
    public function updates() {
        return $this->morphToMany('App\Models\Update', 'updatable', 'irm_updatables');
    }

}
