<?php

namespace App\Traits;

use App\Models\BaseModel;
use Artisan;

trait FlushesModelCache
{
    protected static function bootFlushesModelCache()
    {
        foreach (static::getRecordActivityEvents() as $eventName) {
            static::$eventName(function (BaseModel $model) use ($eventName) {
                Artisan::call('irm:flushcache');
            });
        }
    }
    

    /**
     * Set the default events to trigger the model cache flushes if the $triggerEvents
     * property does not exist on the model.
     *
     * @return array
     */
    protected static function getRecordActivityEvents()
    {
        if (isset(static::$triggerEvents)) {
            return static::$triggerEvents;
        }

        return [
            'created', 'updated', 'deleted',
        ];
    }
}
