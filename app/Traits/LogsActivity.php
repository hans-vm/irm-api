<?php

namespace App\Traits;

use App\Models\BaseModel;
use App\Models\ActivityLog;
use App\Models\ActivityLogType;

trait LogsActivity
{
    protected static function bootLogsActivity()
    {
        foreach (static::getRecordActivityEvents() as $eventName) {
            static::$eventName(function (BaseModel $model) use ($eventName) {
                ActivityLog::log($model, $eventName);
            });
        }
    }
    

    /**
     * Set the default events to be recorded if the $recordEvents
     * property does not exist on the model.
     *
     * @return array
     */
    protected static function getRecordActivityEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created', 'updated', 'deleted',
        ];
    }
}
