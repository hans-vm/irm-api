<?php

namespace App\Traits;

use App\Models\BaseModel;

trait LocalizesId
{

    /**
     * Boot
     *     
     * @return void
     */
    protected static function bootLocalizesId()
    {
        static::saving(function (BaseModel $model) {

            // If no local id is set yet
            if (!isset($model->local_id) || $model->local_id == 0) {

                // Get the secondary key field name
                $secondaryKey = static::getModelSecondaryKeyField();
                
                // If secondary key has set
                if ($model->$secondaryKey) {

                    // Get the last row based on secondary key
                    $lastRow = static::withTrashed()
                                    ->where($secondaryKey, $model->$secondaryKey)
                                    ->orderBy('local_id', 'desc')
                                    ->first();

                    // Increment the local id
                    $model->local_id = $lastRow ? ($lastRow->local_id + 1) : 1;        
                }
            }
            
            return true;
        });
    }
    

    /**
     * Get the secondary key field on which the locallized Id should be based.
     *
     * @return string
     */
    protected static function getModelSecondaryKeyField()
    {
        if (isset(static::$_model_secondary_key_field) && static::$_model_secondary_key_field != '') {
            return static::$_model_secondary_key_field;
        }

        return 'user_id';
    }
}
