<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'user_role_id', 'enabled'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_users';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['deleted_at', 'updated_at', 'remember_token', 'user_role_id'];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'created_at'        =>  'Register Date',
        'first_name'        =>  'First Name',
        'last_name'         =>  'Last Name'
    ];

    /**
     * The attributes that are not editable
     *
     * @var array
     */
    protected static $_model_info_uneditable_fields =  [
        'created_at'
    ];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

        'password'          => 'password'
    ];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'first_name'                 => [],
        'last_name'                  => [],
        'email'                      => [],
        'created_at'                 => [],
        'enabled'                    => [],
        'role'                       => []
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'first_name'            => [],
        'last_name'             => [],
        'email'                 => [ 'required' => true ],
        'password'              => [ 'required' => true ],
        'role'                  => [ 'required' => true ],
    ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["id", "role", "email", "created_at", "enabled"],
        'renders'       => ['created_at' => 'date', 'enabled' => array('function' => 'label', 'bind' => array(1 => 'success', 0 => 'danger')) ],
        'order'         => ['field' => 'created_at', 'sort' => -1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Users'
    ];

    /**
     * The attributes info for gridFilter
     *
     * @var array
     */
    protected static $_model_info_gridFilter =  [

        'fields'        => ["email", "created_at", "role"],
        'controls'      => [
                                "email"         => ['type' => 'text', 'ico' => 'fa-bookmark-o'],
                                "created_at"    => ['type' => 'datepicker-range', 'fromIco' => 'fa-calendar-minus-o', 'toIco' => 'fa-calendar-plus-o'],
                                "role"          => ['type' => 'select', 'ico' => 'fa-list-alt']
                            ],
        'defaultIco'    => 'fa-bookmark-o'
    ];

    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
    protected static $_model_list_value_field = 'email';

    /**
     * Relationship between User:UserRole
     *
     */
    public function role()
    {
        return $this->belongsTo("App\Models\UserRole", "user_role_id");
    }

    /**
     * Relationship between User:Investor
     *
     */
    public function investors()
    {
        return $this->hasMany("App\Models\Investor", "user_id");
    }

    /** 
     * Relationship between User:Notifiaction
     *
     */
    public function notifications()
    {
        return $this->belongsToMany("App\Models\Notification", 'irm_user_notification')->withPivot('is_read');
    }

    /** 
     * Relationship between User:Update
     *
     */
    public function updates()
    {
        return $this->belongsToMany("App\Models\Update", 'irm_update_user_status')
                    ->whereNull('irm_update_user_status.deleted_at')
                    ->withPivot('local_id', 'is_read');
    }

    /** 
     * Relationship between User:SettingsField
     *
     */
    public function settings()
    {
        return $this->belongsToMany("App\Models\SettingsField", 'irm_user_settings')->withPivot('value');
    }

    /**
     * Relationship between User:Ticket
     *
     */
    public function tickets()
    {
        return $this->hasMany("App\Models\Ticket", "user_id");
    }

    /**
     * Get User by email address
     *
     * @param  string  $email   user email address
     * @param  bool    $checkTrash  whether we should check the deleted records
     * @return \App\User 
     */
    public static function GetUserByEmail($email, $checkTrash = false) {
        if ($checkTrash) {
            return User::withTrashed()->where('email', $email)->first();
        }
        return User::where('email', $email)->first();
    }

    /**
     * Get User Role ID By Value
     *
     * @param string $value Role Name
     * @return id  User role ID
     */
    public static function GetUserRoleIDByName($value) {

        $userRoles = static::GetSelectFieldValueList('role');

        foreach ($userRoles as $id => $val) {
            if ($val == $value) return $id;
        }

        return null;
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        $userName = empty($this->first_name) && empty($this->last_name) ? $this->email : $this->first_name . ' ' . $this->last_name;

        return 'User "' . $userName . '"';
    }

    /**
     * Get current user role name
     *
     * @return string 
     */
    public function getRoleName() {

        $role = $this->role;
        if (empty($role)) return '';
        return $role->role_name;

    }

    /**
     * Check if current user is admin
     *
     * @return bool
     */
    public function isAdmin() {
        return $this->getRoleName() == UserRole::$ADMIN_ROLE_NAME;
    }

    /**
     * Check if current user is manager
     *
     * @return bool
     */
    public function isManager() {
        return $this->getRoleName() == UserRole::$MANAGER_ROLE_NAME;
    }

    /**
     * Check if current user is client
     *
     * @return bool
     */
    public function isClient() {
        return $this->getRoleName() == UserRole::$CLIENT_ROLE_NAME;
    }

    /**
     * Get setting value of the User by SettingsField
     * 
     * @param  mixed  $field  numeric - id 
     *                        string - name 
     *                        SettingsField - settings field itself
     *         
     * @return mixed          Settings Value
     */
    public function getSetting($field) {
        // find SettingsField object if `$field` is not of SettingsField itself.
        if (!$field instanceof SettingsField) {
            $field = SettingsField::findByIdOrName($field);
        }
        
        // bail out if field not found
        if (!$field) return null;

        // if the SettingsField has `linked_field`, lookup User object
        if (!empty($field->linked_field)) {
            return $this->getAttributeValue($field->linked_field);
        } else {
            // or find the SettingsField again with pivot
            $field = $this->settings()->find($field->id);
            if (isset($field->pivot)) {
                return $field->pivot->value;
            }    
        }
        
        return null;
    }

    /**
     * Get setting value of the User by SettingsField
     * 
     * @param  mixed  $field  numeric - id 
     *                        string - name 
     *                        SettingsField - settings field itself
     */
    public function saveSetting($field, $value) {
        // find SettingsField object if `$field` is not of SettingsField itself.
        if (!$field instanceof SettingsField) {
            $field = SettingsField::findByIdOrName($field);
        }
        
        // bail out if field not found
        if (!$field) return null;

        // if the SettingsField has `linked_field`, update User object
        if (!empty($field->linked_field)) {
            $this->setAttribute($field->linked_field, $value);
            $this->save();
        } else {
            // or update the SettingsField with pivot
            $this->settings()->sync([$field->id => ['value' => $value]], false);
        }
    }

    /**
     * Returns settings of the user taking `linked_field` into account
     * 
     * @return collection Eloquent Collection following the format 
     *                    returned by `settings` dynamic property
     */
    public function getSettings() {
        // TODO: Should optimize
        return SettingsField::all()->map(function ($field) {
            $field->pivot = (object) ['value' => $this->getSetting($field)];
            return $field;
        });
    }

    /**
     * Saves user settings
     *     
     * @param  array $settings  Array that has SettingsField id as key and pivot as value
     *                          Follows the format BelongsToMany::sync() accepts
     */
    public function saveSettings($settings) {
        // TODO: Should optimize
        foreach ($settings as $key => $value) {
            $this->saveSetting($key, isset($value['value']) ? $value['value'] : null);
        }
    }
}
