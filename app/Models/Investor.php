<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LocalizesId;

class Investor extends BaseModel
{
	use SoftDeletes;
    use LocalizesId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_investors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'user_id', 'investor_status_id', 'investor_type_id'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'local_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['status', 'type'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'user_id', 'investor_status_id', 'investor_type_id', 'updates'];

    /**
     * The relationship attributes that should not be populated
     *
     * @var array
     */
    protected static $_model_info_simplify_columns =  ['user'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'local_id'          =>  'Id'
    ];

    /**
     * Computed Columns
     *
     * @var array
     */
    protected static $_model_info_computed_columns =  [

    ];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'name'          => [],
        'type'          => [],
        'status'        => []
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [
 
        'name'          => ['required' => true],
        'type'          => ['required' => true],
        'status'        => []
    ];

    /**
     * The list of sub model attributes for simple fetch
     *
     * @var array
     */
    protected static $_submodel_list_for_simple_fetch =  [ 'accounts', 'investments' ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["local_id", "name", "type", "status"],
        'renders'       => ['status' => array('function' => 'label', 'bind' => array(1 => 'success', 2 => 'info', 3 => 'danger')) ],
        'order'         => ['field' => 'name', 'sort' => 1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Investors'
    ];

    /**
     * The attributes info for gridFilter
     *
     * @var array
     */
    protected static $_model_info_gridFilter =  [

        'fields'        => ["name", "type", "status"],
        'controls'      => [
                                "name"          => ['type' => 'text', 'ico' => 'fa-bookmark-o'],
                                "type"          => ['type' => 'select', 'ico' => 'fa-list-alt'],
                                "status"        => ['type' => 'checkbox', 'default' => false, 'label' => 'Active', 'value' => 1 ]
                            ],
        'defaultIco'    => 'fa-bookmark-o'
    ];

    /**
     * The list of additional validation rules for form post
     *
     * @var array
     */
    protected static $_model_form_post_extra_validation =  [

    ];
    
    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
    protected static $_model_list_value_field = 'name';

    /**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
    protected static $_model_secondary_key_field = 'user_id';

    /**
     * Relationship between Investor:User
     *
     */
    public function user()
    {
        return $this->belongsTo("App\Models\User", "user_id");
    }

    /**
     * Relationship between Investor:InvestorType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\InvestorType", "investor_type_id");
    }

    /**
     * Relationship between Investor:InvestorStatus
     *
     */
    public function status()
    {
        return $this->belongsTo("App\Models\InvestorStatus", "investor_status_id");
    }

    /**
     * Relationship between Investor:Account
     *
     */
    public function accounts()
    {
        return $this->hasMany("App\Models\Account", "investor_id");
    }

    /**
     * Relationship between Investor:Investment
     *
     */
    public function investments()
    {
        return $this->hasMany("App\Models\Investment", "investor_id");
    }

    /**
     * Relationship between Investor:InvestorDocument
     *
     */
    public function documents()
    {
        return $this->hasMany("App\Models\InvestorDocument", "investor_id");
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Investor "' . $this->name . '"';
    }
}
