<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends BaseModel
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'project_status_id', 'project_type_id', 'raise_amount', 'cap_amount', 'project_market_id', 'start_date', 'end_date'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'start_date', 'end_date'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['status', 'type', 'market'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'project_market_id', 'project_status_id', 'project_type_id', 'updates'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'name'          =>  'Name',
        'raise_amount'  =>  'Raise Amount',
        'cap_amount'    =>  'Capital Amount',
        'start_date'    =>  'Start Date',
        'end_date'      =>  'Exit Date'
    ];

    /**
     * Computed Columns
     *
     * @var array
     */
    protected static $_model_info_computed_columns =  [ 'balance' ];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'name'          => [],
        'type'          => [],
        'raise_amount'  => [ 'render' => 'money'],
        'cap_amount'    => [ 'render' => 'money'],
        'status'        => [],
        'market'        => [],
        'balance'       => [ 'render' => 'money'],
        'start_date'    => [],
        'end_date'      => []
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'name'          => ['required' => true],
        'type'          => ['required' => true],
        'market'        => ['required' => true],
        'raise_amount'  => [],
        'cap_amount'    => [],
        'status'        => [],
        'start_date'    => [],
        'end_date'      => []
    ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["id", "name", "type", "market", "raise_amount", "cap_amount", "status"],
        'renders'       => ['raise_amount' => 'money', 'cap_amount' => 'money', 'status' => array('function' => 'label', 'bind' => array(1 => 'success', 2 => 'info', 3 => 'danger')) ],
        'order'         => ['field' => 'id', 'sort' => 1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Projects'
    ];

    /**
     * The attributes info for gridFilter
     *
     * @var array
     */
    protected static $_model_info_gridFilter =  [

        'fields'        => ["name", "type", "market", "raise_amount", "cap_amount", "status"],
        'controls'      => [
                                "name"          => ['type' => 'text', 'ico' => 'fa-bookmark-o'],
                                "type"          => ['type' => 'select', 'ico' => 'fa-list-alt'],
                                "market"        => ['type' => 'select', 'ico' => 'fa-list-alt'],
                                "raise_amount"  => ['type' => 'slider'],
                                "cap_amount"    => ['type' => 'slider'],
                                "status"        => ['type' => 'checkbox', 'default' => false, 'label' => 'Active', 'value' => 1 ]
                            ],
        'defaultIco'    => 'fa-bookmark-o'
    ];
    
    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
    protected static $_model_list_value_field = 'name';

    /**
     * Relationship between Project:ProjectType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\ProjectType", "project_type_id");
    }

    /**
     * Relationship between Project:ProjectStatus
     *
     */
    public function status()
    {
        return $this->belongsTo("App\Models\ProjectStatus", "project_status_id");
    }

    /**
     * Relationship between Project:ProjectMarket
     *
     */
    public function market()
    {
        return $this->belongsTo("App\Models\ProjectMarket", "project_market_id");
    }

    /**
     * Relationship between Project:Investment
     *
     */
    public function investments()
    {
        return $this->hasMany("App\Models\Investment", "project_id");
    }

    /**
     * Get all users related the the Update according to the linked
     * Investor/Account/Investment/Project
     * 
     * @return array Array of User Ids
     */
    public function getAllRelatedUserIds() {

        // Get all users related with the linked investments
        if ($this->investments) {
            return $this->investments->map(function ($investment) {
                return $investment->investor->user_id;
            })->all();
        }

        return [];
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Project "' . $this->name . '"';
    }
}
