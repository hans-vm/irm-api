<?php

namespace App\Models;

class ActivityLogType extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_activity_log_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'target', 'action', 'template'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
	protected static $_model_list_value_field = 'name';

    /**
     * Returns the type object based on the target and action
     *
     * @param string    $target     target model table name
     * @param string    $action     event name
     * @return activity log type object
     */
    public static function GetByTargetAndAction($target, $action) {

        return ActivityLogType::where('target', $target)
                              ->where('action', $action)
                              ->first();
    }
}
