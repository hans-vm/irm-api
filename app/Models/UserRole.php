<?php

namespace App\Models;

class UserRole extends BaseModel
{

    /**
     * User Roles
     * 
     * @var string
     */
    public static $ADMIN_ROLE_NAME            =  'Admin';
    public static $MANAGER_ROLE_NAME          =  'Manager';
    public static $CLIENT_ROLE_NAME           =  'Client';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_user_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role_name', 'enabled'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
	protected static $_model_list_value_field = 'role_name';
}
