<?php

namespace App\Models;

class TransferType extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_transfer_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
	protected static $_model_list_value_field = 'name';
}
