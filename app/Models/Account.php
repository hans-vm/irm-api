<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LocalizesId;

class Account extends BaseModel
{
	use SoftDeletes;
    use LocalizesId;
        
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'investor_id', 'account_status_id', 'account_type_id', 'note', 'account_date'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'local_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'account_date'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['status', 'type'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'investor_id', 'account_status_id', 'account_type_id', 'updates'];

    /**
     * The relationship attributes that should not be populated
     *
     * @var array
     */
    protected static $_model_info_simplify_columns =  ['investor'];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'account_date'        =>  'Date'
    ];

    /**
     * Computed Columns
     *
     * @var array
     */
    protected static $_model_info_computed_columns =  ['balance'];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'title'         => [],
        'account_date'  => [],
        'type'          => [],
        'status'        => [],
        'balance'       => [ 'render' => 'money']

    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'investor'      => ['required' => true],
        'title'         => ['required' => true],
        'type'          => ['required' => true],
        'status'        => []

    ];

    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
    protected static $_model_list_value_field = 'title';

    /**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
    protected static $_model_secondary_key_field = 'investor_id';

    /**
     * Relationship between Account:Investor
     *
     */
    public function investor()
    {
        return $this->belongsTo("App\Models\Investor", "investor_id");
    }

    /**
     * Relationship between Account:AccountType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\AccountType", "account_type_id");
    }

    /**
     * Relationship between Account:AccountStatus
     *
     */
    public function status()
    {
        return $this->belongsTo("App\Models\AccountStatus", "account_status_id");
    }

    /**
     * Relationship between Account:AccountTransactions
     *
     */
    public function transactions() {
        return $this->hasMany('App\Models\AccountTransaction', 'account_id');
    }

    /**
     * Create and Add a Account Transaction to the Account
     * 
     * @param string $title  Title
     * @param string $type   Type
     * @param string $status Status
     * @param number $amount Amount
     * @param string $note   Note. Optional.
     * 
     * @return AccountTransaction Account Transaction created.
     */
    public function addTransaction($title, $type, $status, $amount, $note = null) {
        $transaction = new AccountTransaction;

        // Set title. Is title absolutely required?
        if (!$title) return null;
        $transaction->title = $title;

        // Associate AccountTransactionType object with its value
        $typeObj = AccountTransactionType::GetObjectByValueField($type);
        if (!$typeObj) return null;
        $transaction->type()->associate($typeObj);

        // Associate AccountTransactionStatus object with its value
        $statusObj = AccountTransactionStatus::GetObjectByValueField($status);
        if (!$statusObj) return null;
        $transaction->status()->associate($statusObj);

        // Set amount. We don't need a transaction with amount 0?
        if (empty($amount)) return null;
        $transaction->amount = $amount;

        $transaction->note = $note;

        // Add the newly created transaction to the Account
        return $this->transactions()->save($transaction);
    }

    /**
     * Get account balance
     * Calculated by summing up `amount` of all transactions 
     * of which `status` is not 'Canceled'.
     * TODO: Should create a `balance` field on Account table 
     * instead of calculating everytime.
     * 
     * @return float Account balance
     */
    public function getBalance() {

        return $this->transactions()
                    ->with('status')
                    ->get()
                    ->filter(function ($transaction) {
                        return $transaction->status->name != 'Canceled';
                    })
                    ->sum('amount');
                    
    }

    /**
     * Restructure model attributes based on model structure
     *
     * @return array model attribute values
     */
    public function getModelFieldValues($usePaginationForHasMany = false, $fetchSubModels = true) {

        $values = parent::getModelFieldValues($usePaginationForHasMany, $fetchSubModels);

        // calculate balance field.
        $values['balance'] = $this->getBalance();

        return $values;
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Account "' . $this->title . '"';
    }
}
