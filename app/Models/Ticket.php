<?php

namespace App\Models;

use App\Traits\LocalizesId;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends BaseModel
{
	use SoftDeletes;
    use LocalizesId;

    /**
     * Models that can be associated with Tickets
     * 
     * @var array
     */
    public static $TICKETABLE_TYPES = ['Investor', 'Account', 'Investment', 'Project', 'Update'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_tickets';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'phone', 'message', 'user_id'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'local_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['deleted_at', 'updated_at', 'user_id', 'user'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'local_id'      =>  'Id',
        'created_at'    =>  'Date',
    ];

    /**
     * Extra Columns
     *
     * @var array
     */
    protected static $_model_info_additional_columns =  [
        
    ];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'email'          => [],
        'phone'          => [],
        'message'        => [],
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'email'          => ['required' => true],
        'message'        => ['required' => true],
        'phone'          => [],
    ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["local_id", "email", "message", "created_at"],
        'renders'       => [],
        'order'         => ['field' => 'created_at', 'sort' => -1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Tickets'
    ];

    /**
     * The attributes info for gridFilter
     *
     * @var array
     */
    protected static $_model_info_gridFilter =  [

        'fields'        => ["email", "phone", "message"],
        'controls'      => [
                                "email"         => ['type' => 'text', 'ico' => 'fa-bookmark-o'],
                                "phone"         => ['type' => 'text', 'ico' => 'fa-list-alt'],
                                "message"       => ['type' => 'text']
                            ],
        'defaultIco'    => 'fa-bookmark-o'
    ];

    /**
     * The list of sub model attributes for simple fetch
     *
     * @var array
     */
    protected static $_submodel_list_for_simple_fetch =  ['investors', 'accounts', 'investments', 'projects', 'updates'];
    
    /**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
    protected static $_model_secondary_key_field = 'user_id';

    /**
     * Relationship between Ticket:Account
     *
     */
    public function accounts()
    {
        return $this->morphedByMany("App\Models\Account", 'ticketable', 'irm_ticketables');
    }

    /**
     * Relationship between Ticket:Investor
     *
     */
    public function investors()
    {
        return $this->morphedByMany("App\Models\Investor", 'ticketable', 'irm_ticketables');
    }

    /**
     * Relationship between Ticket:Investment
     *
     */
    public function investments()
    {
        return $this->morphedByMany("App\Models\Investment", 'ticketable', 'irm_ticketables');
    }

    /**
     * Relationship between Ticket:Project
     *
     */
    public function projects()
    {
        return $this->morphedByMany("App\Models\Project", 'ticketable', 'irm_ticketables');
    }

    /**
     * Relationship between Ticket:Update
     *
     */
    public function updates()
    {
        return $this->morphedByMany("App\Models\Update", 'ticketable', 'irm_ticketables');
    }
    
    /** 
     * Relationship between Ticket:User
     *
     */
    public function user()
    {
        return $this->belongsTo("App\Models\User", 'user_id');
    }

    /** 
     * Polymorphic relationship between Ticket and Investor/Account/Investment/Project/Update
     *
     */
    public function linkOfType($type)
    {
        switch ($type) {
            case 'Account':
                return $this->accounts();
            case 'Investor':
                return $this->investors();
            case 'Investment':
                return $this->investments();
            case 'Project':
                return $this->projects();
            case 'Update':
                return $this->updates();        
            default:
                return null;
        }
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Ticket "' . $this->local_id . '"';
    }
}
