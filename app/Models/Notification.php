<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends BaseModel
{
	use SoftDeletes;

    /**
     * Models that can be associated with Notification
     * 
     * @var array
     */
    public static $NOTIFIABLE_TYPES = ['User', 'Investor', 'Account', 'Investment', 'Project', 'Update'];

    /**
     * Display Mode Field Names
     * 
     * @var string
     */
    public static $FIELD_DISPLAY_MODE_ALERT     = 'display_mode_alert';
    public static $FIELD_DISPLAY_MODE_MENU      = 'display_mode_menu';
    public static $FIELD_DISPLAY_MODE_SIDEBAR   = 'display_mode_sidebar';
    public static $FIELD_DISPLAY_MODE_EMAIL     = 'display_mode_email';

    /**
     * Display Modes Available
     * 
     * @var array
     */
    public static $DISPLAY_MODES = [
        [
            'label'     =>  'Alert',
            'value'     =>  'display_mode_alert'
        ],
        [
            'label'     =>  'Menu',
            'value'     =>  'display_mode_menu'
        ],
        [
            'label'     =>  'Sidebar',
            'value'     =>  'display_mode_sidebar'
        ],
        [
            'label'     =>  'Email',
            'value'     =>  'display_mode_email'
        ],  
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'message', 'action', 'created_by', 'notifiable_id', 'notifiable_type', 'display_mode_alert', 'display_mode_menu', 'display_mode_sidebar', 'display_mode_email', 'notification_type_id', 'notification_date'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'notification_date'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['type', 'notifiable'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'creator', 'notification_type_id', 'display_mode_alert', 'display_mode_menu', 'display_mode_sidebar', 'display_mode_email'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

        'message'   =>  'longtext'
    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'notification_date' => 'Date'
    ];

    /**
     * Extra Columns
     *
     * @var array
     */
    protected static $_model_info_additional_columns =  [
        
        'is_read'   => 'boolean',
        'display'   => 'checkbox-set'
    ];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'title'         => [],
        'message'       => [],
        'action'        => [],
        'type'          => [],
        'notification_date' => [],
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'title'                 => ['required' => true],
        'message'               => ['required' => true],
        'action'                => [],
        'type'                  => [],
        'display'               => ['required' => true]
    ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["id", "title", "type"],
        'renders'       => ['type' => array('function' => 'label', 'bind' => array(1 => 'default', 2 => 'primary', 3 => 'success', 4 => 'info', 5 => 'warning', 6 => 'danger')) ],
        'order'         => ['field' => 'id', 'sort' => -1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Notifications'
    ];

    /**
     * The list of sub model attributes for simple fetch
     *
     * @var array
     */
    protected static $_submodel_list_for_simple_fetch =  [];
    
    /**
     * The list of additional validation rules for form post
     *
     * @var array
     */
    protected static $_model_form_post_extra_validation =  [

        'notifiable_type'   =>  'required',
        'notifiable_id'     =>  'required'
    ];

    /** 
     * Polymorphic relationship between Notification:Notifiable
     *
     * User/Investor/Account/Investment/Project/Update
     */
    public function notifiable()
    {
        return $this->morphTo();
    }

    /** 
     * Relationship between Notification:User
     *
     */
    public function users()
    {
        return $this->belongsToMany("App\Models\User", 'irm_user_notification')->withPivot('is_read');
    }

    /** 
     * Relationship between Notification:User
     *
     */
    public function creator()
    {
        return $this->belongsTo("App\Models\User", 'created_by');
    }


    /** 
     * Relationship between Notification:NotificationType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\NotificationType", 'notification_type_id');
    }


    /**
     * Gets fields information as array
     *
     * @param  Verheijen\Bakery\Objects\BakeryModel $model      Bakery Model object 
     * @return array model fields array
     */
    protected static function _getFieldsInfo($model)
    {
        $fieldInfo = parent::_getFieldsInfo($model);
        
        // Manually add display field info
        $fieldInfo['display'] = [
            'type'      =>  'checkbox-set',
            'values'    =>  static::$DISPLAY_MODES,
            'default'   =>  []
        ];

        return $fieldInfo;
    }


    /**
     * Restructure model attributes based on model structure
     *
     * @return array model attribute values
     */
    public function getModelFieldValues($usePaginationForHasMany = false, $fetchSubModels = true) {

        $values = parent::getModelFieldValues($usePaginationForHasMany, $fetchSubModels);

        // get is_read field
        $values['is_read'] = isset($this->pivot) ? $this->pivot->is_read : false;

        // get display field
        $display = [];
        foreach (static::$DISPLAY_MODES as $mode) {
            $value = $mode['value'];
            if (isset($this->$value) && $this->$value) $display[] = $value;
        }
        $values['display'] = $display;

        return $values;
    }

    /**
     * Explicit setter for attributes in _model_info_additional_columns
     * 
     * @param string $name  Name of the attribute
     * @param mixed $value  Value of the attribute
     */
    public function setAdditionalAttribute($name, $value) {
        if ($name == 'display') {
            if (isset($value)) {
                foreach (static::$DISPLAY_MODES as $displayMode) {
                    $mode = $displayMode['value'];
                    $this->$mode = in_array($mode, $value) ? 1 : 0;
                }
            }        
        }
    }   

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Notification "' . $this->title . '"';
    }
}
