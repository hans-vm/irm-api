<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class SettingsField extends BaseModel
{
        
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_settings_fields';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'title', 'settings_field_group_id', 'settings_field_type_id', 'sort_order', 'meta_data', 'linked_field', 'default_value'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['type', 'group'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'name', 'settings_field_type_id', 'settings_field_group_id', 'linked_field', 'default_value'];

    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
    protected static $_model_list_value_field = 'title';

    /**
     * Relationship between SettingsField:SettingsFieldType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\SettingsFieldType", "settings_field_type_id");
    }

    /**
     * Relationship between SettingsField:SettingsFieldGroup
     *
     */
    public function group()
    {
        return $this->belongsTo("App\Models\SettingsFieldGroup", "settings_field_group_id");
    }

    /**
     * Restructure model attributes based on model structure
     *
     * @return array model attribute values
     */
    public function getModelFieldValues($usePaginationForHasMany = false, $fetchSubModels = true) {
        
        // Get values
        $values = parent::getModelFieldValues($usePaginationForHasMany, $fetchSubModels);

        // Override type field to `name` field of SettingsFieldType, instead of its `id`
        $values['type'] = $this->type->name;

        // Decode meta_data
        $values['meta_data'] = json_decode($this->meta_data);

        return $values;
    }

    /**
     * Returns default id/value map
     * 
     * @return array Array to be used in BelongsToMany::sync()
     */
    public static function defaults() {
        return static::whereNotNull('default_value')->get()->reduce(function ($carry, $field) {
            return $carry + array($field->id  =>  [ 'value' => $field->default_value ]);
        }, []);
    }

    /**
     * Find SettingsField by Id or Name
     * 
     * @param  mixed         $id numeric - id
     *                           string - name
     * @return SettingsField     SettingsField object or null, if not found
     */
    public static function findByIdOrName($id) {
        if (empty($id))     return null;
        else if (is_numeric($id)) return static::find($id);
        else return SettingsField::where('name', $id)->first();
    }
}
