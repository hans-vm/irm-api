<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Update extends BaseModel
{
	use SoftDeletes;

    /**
     * Models that can be associated with Update
     * 
     * @var array
     */
    public static $UPDATABLE_TYPES = ['Investor', 'Account', 'Investment', 'Project'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_updates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'update_type_id', 'update_status_id', 'publisher_id', 'published_at', 'content'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'published_at'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['status', 'type'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'publisher_id', 'update_status_id', 'update_type_id', 'uuid'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'local_id'      =>  'Id',
        'published_at'  =>  'Date',
    ];

    /**
     * Extra Columns
     *
     * @var array
     */
    protected static $_model_info_additional_columns =  [
        
        'is_read'   => 'boolean',
        'local_id'  => 'bigint'
    ];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'title'         => [],
        'type'          => [],
        'status'        => [],
        'published_at'  => [],
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'title'         => ['required' => true],
        'type'          => ['required' => true],
        'status'        => [],
        'published_at'  => [],
        'content'       => []
    ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["local_id", "title", "published_at", "type", "status"],
        'renders'       => ['title' => 'updateLink', 'published_at' => 'date', 'status' => array('function' => 'label', 'bind' => array(1 => 'success', 2 => 'danger')) ],
        'order'         => ['field' => 'published_at', 'sort' => -1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Updates'
    ];

    /**
     * The attributes info for gridFilter
     *
     * @var array
     */
    protected static $_model_info_gridFilter =  [

        'fields'        => ["title", "type", "status"],
        'controls'      => [
                                "title"         => ['type' => 'text', 'ico' => 'fa-bookmark-o'],
                                "type"          => ['type' => 'select', 'ico' => 'fa-list-alt'],
                                "status"        => ['type' => 'checkbox', 'default' => false, 'label' => 'Read', 'value' => 1 ]
                            ],
        'defaultIco'    => 'fa-bookmark-o'
    ];

    /**
     * The list of sub model attributes for simple fetch
     *
     * @var array
     */
    protected static $_submodel_list_for_simple_fetch =  ['accounts', 'investors', 'investments', 'projects'];
    
    /**
     * Relationship between Update:UpdateType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\UpdateType", "update_type_id");
    }

    /**
     * Relationship between Update:UpdateStatus
     *
     */
    public function status()
    {
        return $this->belongsTo("App\Models\UpdateStatus", "update_status_id");
    }

    /**
     * Relationship between Update:Account
     *
     */
    public function accounts()
    {
        return $this->morphedByMany("App\Models\Account", 'updatable', 'irm_updatables');
    }

    /**
     * Relationship between Update:Investor
     *
     */
    public function investors()
    {
        return $this->morphedByMany("App\Models\Investor", 'updatable', 'irm_updatables');
    }

    /**
     * Relationship between Update:Investment
     *
     */
    public function investments()
    {
        return $this->morphedByMany("App\Models\Investment", 'updatable', 'irm_updatables');
    }
    
    /** 
     * Relationship between Update:Project
     *
     */
    public function projects()
    {
        return $this->morphedByMany("App\Models\Project", 'updatable', 'irm_updatables');
    }

    /** 
     * Relationship between Update:User
     *
     */
    public function publisher()
    {
        return $this->belongsTo("App\Models\User", 'publisher_id');
    }

    /** 
     * Relationship between Update:User
     *
     */
    public function users()
    {
        return $this->belongsToMany("App\Models\User", 'irm_update_user_status')
                    ->whereNull('irm_update_user_status.deleted_at')
                    ->withPivot('local_id', 'is_read');
    }

    /** 
     * Polymorphic relationship between Update and Investor/Account/Investment/Project
     *
     */
    public function linkOfType($type)
    {
        if ($type == 'Account') return $this->accounts();
        else if ($type == 'Investor') return $this->investors();
        else if ($type == 'Investment') return $this->investments();
        else if ($type == 'Project') return $this->projects();
        else return null;
    }

    /**
     * Restructure model attributes based on model structure
     *
     * @return array model attribute values
     */
    public function getModelFieldValues($usePaginationForHasMany = false, $fetchSubModels = true) 
    {

        $values = parent::getModelFieldValues($usePaginationForHasMany, $fetchSubModels);

        // normalize is_read field
        $values['is_read'] = isset($this->pivot) ? $this->pivot->is_read : false;
        $values['local_id'] = isset($this->pivot) ? $this->pivot->local_id : 0;

        return $values;
    }

    /**
     * Get all users related the the Update according to the linked
     * Investor/Account/Investment/Project
     * 
     * @return array Array of User Ids
     */
    public function getAllRelatedUserIds() {
        
        $users = collect([]);

        // Get all users related with the linked investors
        if ($this->investors) {
            $users = $users->merge($this->investors->map(function ($investor) {
                return $investor->user_id;
            }));
        }
                            
        // Get all users related with the linked accounts
        if ($this->accounts) {
            $users = $users->merge($this->accounts->map(function ($account) {
                return $account->investor->user_id;
            }));
        }
        
        // Get all users related with the linked investments
        if ($this->investments) {
            $users = $users->merge($this->investments->map(function ($investment) {
                return $investment->investor->user_id;
            }));
        }

        // Get all users related with the linked projects
        if ($this->projects) {
            $users = $users->merge($this->projects->map(function ($project) {
                return $project->getAllRelatedUserIds();
            })->flatten(1));
        }

        return $users->unique()->values()->all();
    }

    /**
     * Sync all users related to the update.
     * 
     * Manually manipulates pivot table as attach/detach/sync 
     * methods don't fire events on the pivot table.
     * 
     * @return void
     */
    public function syncUsers()
    {
        // User Ids to Sync
        $userIds = $this->getAllRelatedUserIds();

        // User Ids currently related to the Update
        $currentUserIds = UpdateUserStatus::where('update_id', $this->id)
                                        ->get()
                                        ->pluck('user_id')
                                        ->all();

        // User Ids to detach
        $detach = array_diff($currentUserIds, $userIds);

        if (count($detach) > 0) {
            // Delete the pivot records
            UpdateUserStatus::where('update_id', $this->id)
                            ->whereIn('user_id', $detach)
                            ->delete();
        }

        // User Ids to newly attach
        $attach = array_diff($userIds, $currentUserIds);

        foreach ($attach as $userId) {
            // Create new pivot record
            $record = new UpdateUserStatus;
            $record->update_id = $this->id;
            $record->user_id = $userId;
            $record->save();
        }
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Update "' . $this->title . '"';
    }
}
