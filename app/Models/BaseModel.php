<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Verheijen\Bakery\Objects\BakeryModel;
use App\Traits\LogsActivity;

use Cache;

class BaseModel extends Model
{
	use LogsActivity;

	protected static $_default_pagination_per_page = 100;

	/**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
	protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at'];

	/**
     * The relationship attributes that should not be populated
     *
     * @var array
     */
	protected static $_model_info_simplify_columns =  [

	];

	/**
     * Field type mapping between Front-end and Back-end model
     *
     * @var array
     */
	protected static $_field_types_backend2frontend =  [

		'string'			=> 'text',
		'datetime'			=> 'date',
		'float'				=> 'number'

	];


	/**
     * Specific field type mapping for model
     *
     * @var array
     */
	protected static $_field_type_custom =  [

		
	];


	/**
     * Custom fields' titles
     *
     * @var array
     */
	protected static $_model_info_fields_title =  [

	];

	/**
     * The attributes that are not editable
     *
     * @var array
     */
	protected static $_model_info_uneditable_fields =  [

	];

	/**
     * Computed Columns
     *
     * @var array
     */
	protected static $_model_info_computed_columns =  [];

	/**
     * Extra Columns
     *
     * @var array
     */
	protected static $_model_info_additional_columns =  [];

	/**
     * The attributes that should be included in preview
     *
     * @var array
     */
	protected static $_model_info_preview_fields =  [];


	/**
     * The attributes that should be included in form
     *
     * @var array
     */
	protected static $_model_info_form_fields =  [];

	/**
     * The attributes info for grid
     *
     * @var array
     */
	protected static $_model_info_grid =  [];

	/**
     * The attributes info for gridFilter
     *
     * @var array
     */
	protected static $_model_info_gridFilter =  [];


	/**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
	protected static $_model_secondary_key_field = '';


	/**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
	protected static $_model_list_value_field = '';


	/**
     * The list of additional validation rules for form post
     *
     * @var array
     */
	protected static $_model_form_post_extra_validation =  [];


	/**
     * The list of sub model attributes for simple fetch
     *
     * @var array
     */
	protected static $_submodel_list_for_simple_fetch =  [];

	/**
     * Returns model structure
     *
     * @return array model structure information 
     */
	public static function GetModelStructure() {

		// load model structure from cache
		$modelStructureCacheKey = get_called_class() . "_MODEL_STRUCTURE";
		$cachedModelStructure = Cache::get($modelStructureCacheKey);

		// if not found from cache, build it
		if (!$cachedModelStructure) {

			$structureInfo = array();

			$className = get_called_class();
			$bakeryModel = new BakeryModel($className);

			// model name 
			$structureInfo['name'] = camel_case($bakeryModel->get_classname());
			// fields info
			$structureInfo['fields'] = static::_getFieldsInfo($bakeryModel);
			// preview info
			$structureInfo['preview'] = static::_getPreviewInfo($bakeryModel);
			if (!count($structureInfo['preview'])) unset($structureInfo['preview']);
			// form info 
			$structureInfo['form'] = static::_getFormInfo($bakeryModel);
			if (!count($structureInfo['form'])) unset($structureInfo['form']);

			// grid info
			if (count(static::$_model_info_grid)) {
				$structureInfo['grid'] = static::$_model_info_grid;
			}

			// grid filter info
			if (count(static::$_model_info_gridFilter)) {
				$structureInfo['gridFilter'] = static::$_model_info_gridFilter;
			}

			// store this into cache - with 1 day expiration
			Cache::put($modelStructureCacheKey, $structureInfo, 60 * 24);
			$cachedModelStructure = $structureInfo;
		}

		return $cachedModelStructure;
	}


	/**
     * Returns model relation structure
     *
     * @return array model relation structure information as array
     */
	protected static function GetModelRelationStructure() {

		// load model relation structure from cache
		$modelRelationStructureCacheKey = get_called_class() . "_MODEL_RELATION_STRUCTURE";
		$cachedModelRelationStructure = Cache::get($modelRelationStructureCacheKey);

		// if not found from cache, build it
		if (!$cachedModelRelationStructure) {

			$structureInfo = array();

			$className = get_called_class();
			$bakeryModel = new BakeryModel($className);

			// looking for relations to add more information
			foreach ($bakeryModel->get_relations() as $relation) {

				$foreignKeyColumn = $relation->get_instance()->getForeignKey();
				$relatedClass = $relation->get_related_class();
				$relatedClassName = camel_case($relatedClass->get_classname());
				$relationMethodName = $relation->get_method_name();
				$relationType = $relation->get_type();

				$structureInfo[$relationMethodName] = array(
					'foreignKeyColumn'   => $foreignKeyColumn,
					'relatedClassName' 	 => $relatedClassName,
					'relationMethodName' => $relationMethodName,
					'relationType' 		 => $relationType
				);
			}

			// store this into cache - with 1 day expiration
			Cache::put($modelRelationStructureCacheKey, $structureInfo, 60 * 24);
			$cachedModelRelationStructure = $structureInfo;
		}

		return $cachedModelRelationStructure;
	}

	/**
     * Gets fields information as array
     *
     * @param  Verheijen\Bakery\Objects\BakeryModel $model 		Bakery Model object 
     * @return array model fields array
     */
	protected static function _getFieldsInfo($model) {

		$fieldsInfo = array();

		// add fields information
		foreach ($model->get_attributes() as $attribute) {

			$attributeName = $attribute->get_name();

			// skip excluded fields
			if (in_array($attributeName, static::$_model_info_exclude_columns)) continue;

			$fieldsInfo[$attributeName] = static::_getFieldTypeInfo($model, $attributeName, $attribute->get_type());
		}

		// looking for relations to add more information
		foreach ($model->get_relations() as $relation) {

			$foreignKeyColumn = $relation->get_instance()->getForeignKey();
			$relatedClass = $relation->get_related_class();
			$relatedClassName = camel_case($relatedClass->get_classname());
			$relationMethodName = $relation->get_method_name();
			$relationType = $relation->get_type();

			// skip excluded fields
			if (in_array($relationMethodName, static::$_model_info_exclude_columns)) continue;

			// set field information based on the relation type
			if ($relationType == 'BelongsTo') { 	// select columns

				// get related model's value field
				$relatedModelValueField = $relatedClass->get_instance()->GetClassVarValue('_model_list_value_field');
				
				if ($relatedModelValueField != '') {

					$fieldInfo = array();

					$fieldInfo['type'] = "select";

					// skip population for simple relationship fields
					if (!in_array($relationMethodName, static::$_model_info_simplify_columns)) {
						$valueList = $relatedClass->get_instance()->GetValueList($relatedModelValueField);

						if (count($valueList) > 0) {
							$fieldInfo['values'] = $valueList;
							$fieldInfo['default'] = $valueList[0]['id'];
						}	
					} 

					$fieldInfo['options'] = [
						'valueField'		=> 'id',
						'labelField'		=> $relatedModelValueField
					];

					$fieldsInfo[$relationMethodName] = $fieldInfo;
				}

			} else if ($relationType == 'HasMany' || $relationType == 'MorphToMany' || $relationType == 'MorphedByMany') {   // list columns
				$fieldsInfo[$relationMethodName] = ["type" => "array"];
			}
		}


		// add computed columns
		foreach (static::$_model_info_computed_columns as $field) {
			$fieldsInfo[$field] = ["type" => "computed"];
		}

		// add extra fields
		foreach (static::$_model_info_additional_columns as $field => $fieldType) {
			$fieldsInfo[$field] = ["type" => $fieldType];
		}

		// set custom title for fields
		foreach ($fieldsInfo as $fieldName => $fieldInfo) {
			if (isset(static::$_model_info_fields_title[$fieldName])) {
				$fieldsInfo[$fieldName]['title'] = static::$_model_info_fields_title[$fieldName];
			}
		}

		// set uneditablf fields
		foreach ($fieldsInfo as $fieldName => $fieldInfo) {
			if (in_array($fieldName, static::$_model_info_uneditable_fields)) {
				$fieldsInfo[$fieldName]['uneditable'] = true;
			}
		}

		return $fieldsInfo;
	}


	/**
     * Get front-end field type from model field type
     *
     * @param  Verheijen\Bakery\Objects\BakeryModel $model 		Bakery Model object
     * @param  string $fieldName 	Model Field name
     * @param  string $fieldType 	Model Field type
     * @return array field type information
     */
	protected static function _getFieldTypeInfo($model, $fieldName, $fieldType) {

		$fieldTypeInfo = array();

		$fieldTypeInfo['type'] = isset(static::$_field_types_backend2frontend[$fieldType]) ? static::$_field_types_backend2frontend[$fieldType] : $fieldType;

		// set custom field type
		if (isset(static::$_field_type_custom[$fieldName])) {
			$fieldTypeInfo['type'] = static::$_field_type_custom[$fieldName];
		}

		// for primary key column, set type as 'id'
		if ($fieldName == $model->get_instance()->getKeyName()) {
			$fieldTypeInfo['type'] = 'id';
		}
		
		return $fieldTypeInfo;
	}



	/**
     * Gets preview information of this model
     *
     * @param  Verheijen\Bakery\Objects\BakeryModel $model 		Bakery Model object 
     * @return array associative array describing preview information
     */
	protected static function _getPreviewInfo($model) {

		$previewInfo = array();

		$previewFields = static::$_model_info_preview_fields;

		$fields = array();
		$renders = array();

		foreach ($previewFields as $fieldName => $fieldInfo) {

			$fields[] = $fieldName;

			if ($fieldInfo && isset($fieldInfo['render'])) {
				$renders[$fieldName] = $fieldInfo['render'];
			}

		}

		if (!empty($fields)) $previewInfo['fields'] = $fields;
		if (!empty($renders)) $previewInfo['renders'] = $renders;

		return $previewInfo;

	}


	/**
     * Gets form information of this model
     *
     * @param  Verheijen\Bakery\Objects\BakeryModel $model 		Bakery Model object 
     * @return array associative array describing form information
     */
	protected static function _getFormInfo($model) {

		$formInfo = array();

		$formFields = static::$_model_info_form_fields;

		$fields = array();
		$required = array();

		foreach ($formFields as $fieldName => $fieldInfo) {

			$fields[] = $fieldName;

			if ($fieldInfo && !empty($fieldInfo['required'])) {
				$required[$fieldName] = true;
			}

		}

		if (!empty($fields)) $formInfo['fields'] = $fields;
		if (!empty($required)) $formInfo['required'] = $required;

		return $formInfo;

	}


	/**
	 * Returns the list of fields that are expected in the form post of the model
	 * 
	 * @return array Array of field names
	 */
	public static function GetFormPostFieldsList() {
		
        $modelInfo = static::GetModelStructure();

        // get form info for this model
        $formInfo = isset($modelInfo['form']) ? $modelInfo['form'] : array();

        // get form fields
        $formFieldsList = isset($formInfo['fields']) ? $formInfo['fields'] : array();

        // get preview info for this model
        $previewInfo = isset($modelInfo['preview']) ? $modelInfo['preview'] : array();

        // get preview fields
        $previewFieldsList = isset($previewInfo['fields']) ? $previewInfo['fields'] : array();     

        $fields = array_merge($formFieldsList, $previewFieldsList);

        // append extra validation fields 
        foreach (static::$_model_form_post_extra_validation as $fieldName => $option) {
        	$fields[] = $fieldName;
        }

        return $fields;
	}


	/**
     * Gets validation list for this model
     *
     * @return array list of validation list
     */
	public static function GetValidationList() {

		// get model structure
        $modelInfo = static::GetModelStructure();

        // get form info for this model
        $formInfo = isset($modelInfo['form']) ? $modelInfo['form'] : array();

        // get required info
        $formFieldsRequired = isset($formInfo['required']) ? $formInfo['required'] : array();

        // build required validator list
        $validationList = array();

        // required validation
        foreach($formFieldsRequired as $requiredField => $required) {
            if ($required) {
                $validationList[$requiredField] = 'required';
            }
        }

        // get form fields
        $formFieldsList = isset($formInfo['fields']) ? $formInfo['fields'] : array();

        // enum validation
        foreach ($formFieldsList as $field) {
            $fieldInfo = isset($modelInfo['fields']) && isset($modelInfo['fields'][$field]) ? $modelInfo['fields'][$field] : array();

            if (!empty($fieldInfo) && $fieldInfo['type'] == 'select' && !empty($fieldInfo['values'])) {

            	$idList = array_map(function($el) {
            		return $el['id'];
            	}, $fieldInfo['values']);

                $validationList[$field] = 'required|in:' . trim(implode(",", $idList));
            }
        }

        if (count(static::$_model_form_post_extra_validation)) {
        	$validationList = array_merge($validationList, static::$_model_form_post_extra_validation);
        }

        return $validationList;
	}


	/**
     * Gets value list for this model
     * 
     *
     * @return array list of values
     */
	public static function GetValueList($valueFieldName) {

		$valueField = static::$_model_list_value_field;

		if (empty($valueField)) {
			return [];
		}

		$list = array();

		$objs = self::all();

		foreach ($objs as $obj) {
			$list[] = array('id' => $obj->id, $valueFieldName => $obj->$valueField);
		}

		return $list;
	}


	/**
     * Restructure model attributes based on model structure
     *
     * @return array model attribute values
     */
	public function getModelFieldValues($usePaginationForHasMany = false, $fetchSubModels = true) {

		$result = array();

		// get model structure
		$modelStructure = static::GetModelStructure();

		// iterate fields described in the structure
		foreach ($modelStructure['fields'] as $fieldName => $fieldInfo) {

			$fieldValue = '';

			if ($fieldInfo['type'] == "select") {
				if ($this->$fieldName) {
					$fieldValue = $this->$fieldName->id;
				}
			} else if ($fieldInfo['type'] == 'date') {
				if ($this->$fieldName && is_object($this->$fieldName) && $this->$fieldName->timestamp) {
					$fieldValue = $this->$fieldName->timestamp * 1000;
				} else {
					$fieldValue = $this->$fieldName;
				}
			} else if ($fieldInfo['type'] == 'array') {
				if ($fetchSubModels) {
					$fieldValue = array();

					if ($usePaginationForHasMany) {
						$fieldValue['data'] = array();
						$objects = $this->$fieldName()->paginate(static::$_default_pagination_per_page);
						foreach ($objects as $obj) {
							$fieldValue['data'][] = $obj->getModelFieldValues(true, in_array($fieldName, static::$_submodel_list_for_simple_fetch) ? false : true);	
						}
						$fieldValue['total_items'] = $objects->total();
						$fieldValue['per_page'] = $objects->perPage();
						$fieldValue['current_page'] = $objects->currentPage();
						$fieldValue['total_page'] = $objects->lastPage();
					} else {
						foreach ($this->$fieldName as $val) {
							$fieldValue[] = $val->getModelFieldValues(true, in_array($fieldName, static::$_submodel_list_for_simple_fetch) ? false : true);
						}
					}
				} else {
					continue;
				}
			} else {
				$fieldValue = $this->$fieldName;
			}

			$result[$fieldName] = $fieldValue;
		}

		return $result;
	}


	/**
     * Returns list of model values from collection
     *
     * @param Illuminate\Database\Eloquent\Collection 	$collection 	model collection
     * @return array model attribute values
     */
	public static function GetModelCollectionValues($collection) {

		$result = array();
		foreach($collection as $entity) {
			$result[] = $entity->getModelFieldValues();
		}
		return $result;
	}



	/**
     * Returns static class variable value
     *
     * @param string name 	class variable name
     * @return string class variable value
     */
	protected static function GetClassVarValue($name) {
		$classVars = get_class_vars(get_called_class());
		return isset($classVars[$name]) ? $classVars[$name] : null;
	}


	/**
     * Update model attributes with form data and save
     *
     * @param array $formData 	Form data array
     * @return bool update result
     */
	public function updateModelWithFormData($formData) {
		$this->setAttributesWithFormData($formData);
		return $this->save();
	}

	/**
	 * Explicit setter for attributes in _model_info_additional_columns
	 * 
	 * @param string $name  Name of the attribute
	 * @param mixed $value  Value of the attribute
	 */
	public function setAdditionalAttribute($name, $value) {

	}

	/**
     * Update model attributes with form data
     *
     * @param array $formData 	Form data array
     * @return void
     */
	public function setAttributesWithFormData($formData) {

		// get model structure
        $modelStructure = static::GetModelStructure();
        $modelFields = $modelStructure['fields'];

        $relationStructure = static::GetModelRelationStructure();

        foreach ($formData as $name => $value) {

            $fieldInfo = isset($modelFields[$name]) ? $modelFields[$name] : array();
            $relationInfo = isset($relationStructure[$name]) ? $relationStructure[$name] : array();

            // skip computed fields
            if (!empty($fieldInfo['type']) && $fieldInfo['type'] == 'computed') continue;

            // skip uneditable fields
            if (in_array($name, static::$_model_info_uneditable_fields)) continue;

            // call explicit setters for extra fields
            if (in_array($name, array_keys(static::$_model_info_additional_columns))) {
            	$this->setAdditionalAttribute($name, $value);
            	continue;	
            } 

            // if relation field - select
            if (!empty($fieldInfo['type']) && $fieldInfo['type'] == 'select') {
            	
            	if (!empty($relationInfo) && !empty($relationInfo['foreignKeyColumn'])) {

            		$this->setAttribute($relationInfo['foreignKeyColumn'], $value);

            	}

			// if field is type of date
            } elseif (!empty($fieldInfo['type']) && $fieldInfo['type'] == 'date') { 

            	if (!empty($value)) {
            		// if the value is numeric, we assume it's in milliseconds
            		// and convert it to the UNIX timestamp - frontend requirement
            		if (is_numeric($value)) {
            			$value = (int)($value / 1000);
            		}
                	$this->$name = $value;
            	}

            } else { 		// normal attribute field
            	if (!empty($value)) {
                	$this->$name = $value;
            	}
            }
        }
	}


	/**
     * Get Object by value field
     *
     * @param  string  $value   value for field
     * @return Illuminate\Database\Eloquent\Model
     */
    public static function GetObjectByValueField($value) {
    	if (empty(static::$_model_list_value_field)) return null;
    	return static::where(static::$_model_list_value_field, $value)->first();
    }



    /**
     * Get array of values for select field
     *
     * @param string $fieldName  field name
     * @return array list of available values for this fields
     */
    public static function GetSelectFieldValueList($fieldName) {

    	// get model info
    	$modelInfo = static::GetModelStructure();

    	// result array
    	$result = array();
    	
        if (!empty($modelInfo) && isset($modelInfo['fields']) && isset($modelInfo['fields'][$fieldName]) && isset($modelInfo['fields'][$fieldName]['values']) && isset($modelInfo['fields'][$fieldName]['options'])) {

        	$values = $modelInfo['fields'][$fieldName]['values'];

        	$fieldOptions = $modelInfo['fields'][$fieldName]['options'];
        	$valueField = isset($fieldOptions['valueField']) ? $fieldOptions['valueField'] : '';
        	$labelField = isset($fieldOptions['labelField']) ? $fieldOptions['labelField'] : '';

        	foreach ($values as $value) {
        		$result[$value[$valueField]] = $value[$labelField];
        	}

        } 

        return $result;

    }


    /**
     * Get the message that needs to be logged for the given event name
     *
     * @param string $eventName
     * @return string activity description
     */
    public function getActivityDescriptionForEvent($eventName) {

    	$objDesc = $this->getObjectDescription();

    	if (empty($objDesc)) return '';

    	if ($eventName == 'created') {
    		return $objDesc . " was created.";
    	}

    	if ($eventName == 'updated') {
    		return $objDesc . " was updated.";
    	}

    	if ($eventName == 'deleted') {
    		return $objDesc . " was deleted.";
    	}

    	return '';
    } 


    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
    	//return "Model [$this->id]";
    	return '';
    }
}
