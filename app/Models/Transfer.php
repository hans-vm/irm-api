<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Transfer extends BaseModel
{
	use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_transfers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['source_id', 'source_type', 'destination_id', 'destination_type', 'amount', 'manual_authorization', 'transfer_status_id', 'transfer_type_id'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'transfer_date'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['status', 'type'];    

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'transfer_status_id', 'transfer_type_id'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'manual_authorization'  =>  'Verification',
        'transfer_date'         =>  'Transfer Date',
    ];

    /**
     * Computed Columns
     *
     * @var array
     */
    protected static $_model_info_computed_columns =  [];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'amount'        => [ 'render' => 'money'],
        'type'          => [],
        'status'        => [],
        'transfer_date' => []
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'source_id'        => ['required' => true],
        'destination_id'   => ['required' => true],
        'amount'           => ['required' => true],
        'type'             => [],
        'manual_authorization'  => [],
    ];

    /**
     * The list of additional validation rules for form post
     *
     * @var array
     */
    protected static $_model_form_post_extra_validation =  [

        'source_type'            => 'required', 
        'destination_type'       => 'required'
    ];

    /**
     * Relationship between Transfer:TransferType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\TransferType", "transfer_type_id");
    }

    /**
     * Relationship between Transfer:TransferStatus
     *
     */
    public function status()
    {
        return $this->belongsTo("App\Models\TransferStatus", "transfer_status_id");
    }

    /**
     * Source relationship
     * Transferable: Account/Investment
     *
     */
    public function source()
    {
        return $this->morphTo();
    }

    /**
     * Destination relationship
     * Transferable: Account/Investment
     *
     */
    public function destination()
    {
        return $this->morphTo();
    }

    /**
     * Gets fields information as array
     *
     * @param  Verheijen\Bakery\Objects\BakeryModel $model      Bakery Model object 
     * @return array model fields array
     */
    protected static function _getFieldsInfo($model) {
        
        // Get fields info from parent
        $fieldsInfo = parent::_getFieldsInfo($model);

        // Add additional information
        $fieldsInfo['manual_authorization']['default'] = 0;

        return $fieldsInfo;
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Transfer "' . $this->id . '"';
    }
}
