<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LocalizesId;

class InvestorDocument extends BaseModel
{
	use SoftDeletes;
    use LocalizesId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'investor_id', 'filename', 'name', 'type', 'size', 'disk', 'uuid'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_investor_documents';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'investor_id', 'uuid', 'disk', 'filename'];


    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];


    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

    ];


    /**
     * Computed Columns
     *
     * @var array
     */
    protected static $_model_info_computed_columns =  [

    ];


    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'name'          => [],
        'type'          => [],
        'size'          => []

    ];


    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

    ];

    
    /**
     * The list of additional validation rules for form post
     *
     * @var array
     */
    protected static $_model_form_post_extra_validation =  [

        'investor_id'            => 'required'

    ];

    
    /**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
    protected static $_model_secondary_key_field = 'investor_id';


    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Investor Document "' . $this->name . '"';
    }
}
