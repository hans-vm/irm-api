<?php

namespace App\Models;

use Eloquent;
use Request;
use Config;
use Exception;
use Auth;

class ActivityLog extends BaseModel
{

    /**
     * Actions to Log
     * 
     * @var string
     */
    public static $LOG_ACTION_LOGIN             =  'login';
    public static $LOG_ACTION_LOGOUT            =  'logout';
    public static $LOG_ACTION_CREATED           =  'created';
    public static $LOG_ACTION_UPDATED           =  'updated';
    public static $LOG_ACTION_DELETED           =  'deleted';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'irm_activity_log';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'text', 'ip_address', 'activity_log_type_id', 'entity_id'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['type'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['updated_at', 'user_id', 'entity_id', 'activity_log_type_id'];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [
    
        'text'              =>  'Message',
        'ip_address'        =>  'IP Address',
        'created_at'        =>  'When'
    ];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'text'                          => [],
        'ip_address'                    => [],
        'type'                          => [],
        'created_at'                    => []
    ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["text", "ip_address", "created_at"],
        'renders'       => ['created_at' => 'datetime'],
        'order'         => ['field' => 'created_at', 'sort' => -1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Security Sessions'
    ];

    /**
     * The attributes info for gridFilter
     *
     * @var array
     */
    protected static $_model_info_gridFilter =  [

        'fields'        => ["text", "created_at", "ip_address", "type"],
        'controls'      => [
                                "text"          => ['type' => 'text', 'ico' => 'fa-bookmark-o'],
                                "created_at"    => ['type' => 'datepicker-range', 'fromIco' => 'fa-calendar-minus-o', 'toIco' => 'fa-calendar-plus-o'],
                                "type"          => ['type' => 'select', 'ico' => 'fa-list-alt'],
                                "ip_address"    => ['type' => 'text', 'ico' => 'fa-bookmark-o']
                            ],
        'defaultIco'    => 'fa-bookmark-o'
    ];

    /**
     * Get the user that the activity belongs to.
     *
     * @return object
     */
    public function user()
    {
        return $this->belongsTo("App\Models\User", 'user_id');
    }

    /**
     * Relationship between ActivityLog:ActivityLogType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\ActivityLogType", "activity_log_type_id");
    }

    /**
     * Helper method for logging model events
     *
     * @param BaseModel $model      model object
     * @param string    $eventName  event name
     */
    public static function log(BaseModel $model, $eventName) {
        // both model and the event name must be present
        if (empty($eventName) || empty($model) || empty($model->id)) {
            return;
        }
        
        if ( $eventType = ActivityLogType::GetByTargetAndAction($model->getTable(), $eventName) ) {
            // get current user
            $user = Auth::User();
            ActivityLog::create([
                'user_id'               =>  !empty($user) ? $user->id : null,
                'text'                  =>  sprintf( $eventType->template, $model->getObjectDescription() ),
                'ip_address'            =>  Request::getClientIp(),
                'activity_log_type_id'  =>  $eventType->id,
                'entity_id'             =>  $model->id
            ]);
        }
    }
}
