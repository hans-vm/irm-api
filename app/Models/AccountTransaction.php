<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LocalizesId;

class AccountTransaction extends BaseModel
{
	use SoftDeletes;
    use LocalizesId;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_account_transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'account_id', 'account_transaction_status_id', 'account_transaction_type_id', 'amount', 'note', 'transaction_date'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'local_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'transaction_date'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['status', 'type'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'account_id', 'uuid', 'account_transaction_type_id', 'account_transaction_status_id'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

        'local_id'                =>  'Id',
        'transaction_date'        =>  'Date'
    ];
    
    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'title'                 => [],
        'type'                  => [],
        'amount'                => [ 'render' => 'money'],
        'transaction_date'      => [],
        'status'                => []
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'title'         => ['required' => true],
        'type'          => ['required' => true],
        'amount'        => [],
        'status'        => []
    ];

    /**
     * The attributes info for grid
     *
     * @var array
     */
    protected static $_model_info_grid =  [

        'fields'        => ["local_id", "title", "type", "amount", "transaction_date", "status"],
        'renders'       => ['amount' => 'money', 'transaction_date' => 'date', 'status' => array('function' => 'label', 'bind' => array(1 => 'success', 2 => 'info', 3 => 'danger')) ],
        'order'         => ['field' => 'transaction_date', 'sort' => -1],
        'grid'          => ['page' => 1, 'perpage' => 50, 'lengthOptions' => [10, 20, 50, 100]],
        'title'         => 'Transactions'
    ];

    /**
     * The attributes info for gridFilter
     *
     * @var array
     */
    protected static $_model_info_gridFilter =  [

        'fields'        => ["title", "transaction_date", "type", "amount", "status"],
        'controls'      => [
                                "title"         => ['type' => 'text', 'ico' => 'fa-bookmark-o'],
                                "transaction_date"    => ['type' => 'datepicker-range', 'fromIco' => 'fa-calendar-minus-o', 'toIco' => 'fa-calendar-plus-o'],
                                "type"          => ['type' => 'select', 'ico' => 'fa-list-alt'],
                                "amount"        => ['type' => 'slider'],
                                "status"        => ['type' => 'checkbox', 'default' => false, 'label' => 'Completed', 'value' => 1 ]
                            ],
        'defaultIco'    => 'fa-bookmark-o'
    ];

    /**
     * The list of additional validation rules for form post
     *
     * @var array
     */
    protected static $_model_form_post_extra_validation =  [

        'account_id'            => 'required'
    ];

    /**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
    protected static $_model_secondary_key_field = 'account_id';

    /**
     * Relationship between AccountTransaction:AccountTransactionType
     *
     */
    public function type()
    {
        return $this->belongsTo("App\Models\AccountTransactionType", "account_transaction_type_id");
    }

    /**
     * Relationship between Account:AccountStatus
     *
     */
    public function status()
    {
        return $this->belongsTo("App\Models\AccountTransactionStatus", "account_transaction_status_id");
    }

    /**
     * Get Account Transaction Status ID By Value
     *
     * @param string $value Transaction Status Value
     * @return id  account transaction status id
     */
    public static function GetTransactionStatusIDByValue($value) {

        $accountTransactionStatusValues = static::GetSelectFieldValueList('status');

        foreach ($accountTransactionStatusValues as $id => $val) {
            if ($val == $value) return $id;
        }

        return null;
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Account Transaction "' . $this->title . '"';
    }
}
