<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LocalizesId;

class UpdateUserStatus extends BaseModel
{
	use SoftDeletes;
	use LocalizesId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_update_user_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['update_id', 'user_id', 'is_read', 'local_id'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
    protected static $_model_secondary_key_field = 'user_id';
}
