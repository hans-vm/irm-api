<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\LocalizesId;

class Investment extends BaseModel
{
	use SoftDeletes;
    use LocalizesId;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_investments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'investor_id', 'investment_status_id', 'project_id', 'note', 'investment_date'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'local_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'investment_date'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['status', 'project'];

    /**
     * The attributes that should be excluded from model information array
     *
     * @var array
     */
    protected static $_model_info_exclude_columns =  ['created_at', 'deleted_at', 'updated_at', 'project_id', 'investment_status_id', 'updates', 'investor_id'];
   
    /**
     * The relationship attributes that should not be populated
     *
     * @var array
     */
    protected static $_model_info_simplify_columns =  ['investor', 'project'];

    /**
     * Specific field type mapping for model
     *
     * @var array
     */
    protected static $_field_type_custom =  [

    ];

    /**
     * Custom fields' titles
     *
     * @var array
     */
    protected static $_model_info_fields_title =  [

    	'investment_date'	=> 'Date',
        'balance'           => 'Invested'
    ];

    /**
     * Computed Columns
     *
     * @var array
     */
    protected static $_model_info_computed_columns =  ['balance'];

    /**
     * The attributes that should be included in preview
     *
     * @var array
     */
    protected static $_model_info_preview_fields =  [

        'title'         => [],
        'investment_date'	=> [],
        'project'       => [],
        'status'        => [],
        'balance'       => ['render' => 'money']
    ];

    /**
     * The attributes that should be included in form
     *
     * @var array
     */
    protected static $_model_info_form_fields =  [

        'investor'      => ['required' => true],
        'title'         => ['required' => true],
        'project' 	    => ['required' => true],
        'status'        => []
    ];

    /**
     * The list of additional validation rules for form post
     *
     * @var array
     */
    protected static $_model_form_post_extra_validation =  [

    ];
    
    /**
     * The attribute that should be used for getting list of values for this model
     *
     * @var string
     */
    protected static $_model_list_value_field = 'title';
  
    /**
     * The attribute that should be used for incrementing localized id field
     *
     * @var string
     */
    protected static $_model_secondary_key_field = 'investor_id';

    /**
     * Relationship between Investment:Investor
     *
     */
    public function investor()
    {
        return $this->belongsTo("App\Models\Investor", "investor_id");
    }

    /**
     * Relationship between Investment:Project
     *
     */
    public function project()
    {
        return $this->belongsTo("App\Models\Project", "project_id");
    }

    /**
     * Relationship between Investment:InvestmentStatus
     *
     */
    public function status()
    {
        return $this->belongsTo("App\Models\InvestmentStatus", "investment_status_id");
    }

    /**
     * Relationship between Investment:InvestmentTransactions
     *
     */
    public function transactions() {
        return $this->hasMany('App\Models\InvestmentTransaction', 'investment_id');
    }

    /**
     * Create and Add a Investment Transaction to the Investment
     * 
     * @param string $title  Title
     * @param string $type   Type
     * @param string $status Status
     * @param number $amount Amount
     * @param string $note   Note. Optional.
     * 
     * @return InvestmentTransaction Investment Transaction created.
     */
    public function addTransaction($title, $type, $status, $amount, $note = null) {
        $transaction = new InvestmentTransaction;

        // Set title. Is title absolutely required?
        if (!$title) return null;
        $transaction->title = $title;

        // Associate InvestmentTransactionType object with its value
        $typeObj = InvestmentTransactionType::GetObjectByValueField($type);
        if (!$typeObj) return null;
        $transaction->type()->associate($typeObj);

        // Associate InvestmentTransactionStatus object with its value
        $statusObj = InvestmentTransactionStatus::GetObjectByValueField($status);
        if (!$statusObj) return null;
        $transaction->status()->associate($statusObj);

        // Set amount. We don't need a transaction with amount 0?
        if (empty($amount)) return null;
        $transaction->amount = $amount;

        if ($note) $transaction->note = $note;

        // Add the newly created transaction to the Investment
        return $this->transactions()->save($transaction);
    }

    /**
     * Get Investment balance
     * Calculated by summing up `amount` of all transactions 
     * of which `status` is not 'Canceled'.
     * TODO: Should create a `balance` field on Investment table 
     * instead of calculating everytime.
     * 
     * @return float Investment balance
     */
    public function getBalance() {

        return $this->transactions()
                    ->with('status')
                    ->get()
                    ->filter(function ($transaction) {
                        return $transaction->status->name != 'Canceled';
                    })
                    ->sum('amount');

    }

    /**
     * Restructure model attributes based on model structure
     *
     * @return array model attribute values
     */
    public function getModelFieldValues($usePaginationForHasMany = false, $fetchSubModels = true) {

        $values = parent::getModelFieldValues($usePaginationForHasMany, $fetchSubModels);

        // calculate balance field.
        $values['balance'] = $this->getBalance();

        return $values;
    }

    /**
     * Get the description about this model instance to be used for logging activity
     *
     * @return string 
     */
    public function getObjectDescription() {
        return 'Investment "' . $this->title . '"';
    }
    
}
