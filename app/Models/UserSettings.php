<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserSettings extends BaseModel
{
	use SoftDeletes;
        
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'irm_user_settings';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'settings_field_id', 'value'];
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
