#!/usr/bin/env bash

# Set the mysql password so we're not asked for it during provisioning.
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password angelus'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password angelus'

# add php5.6+ repos.
add-apt-repository -y ppa:ondrej/php5-5.6

# install prep.
apt-get update
apt-get -y upgrade

# install php 5.6+
apt-get -y install libapache2-mod-php5 php5 php5-cli php5-curl php5-mysqlnd php5-mcrypt php5-intl php5-json php5-gd php5-redis php5-xdebug

# install composer
php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

# install apache & mysql
apt-get -y install apache2 mysql-server

# remove MySQL localhost host binding so we can access the server from the host machine.
sed -i "s/bind-address$(printf '\t\t')= 127.0.0.1/bind-address$(printf '\t\t')= 0.0.0.0/" /etc/mysql/my.cnf
service mysql restart

# create Apache virtual host.
cat > /etc/apache2/sites-available/000-default.conf <<EOL
<VirtualHost *:80>
  #ServerName www.example.com

  ServerAdmin webmaster@localhost
  DocumentRoot /home/vagrant/public

  <Directory /home/vagrant/public>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order allow,deny
    Allow from all
    Require all granted
  </Directory>

  ErrorLog \${APACHE_LOG_DIR}/error.log
  CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOL

# change apache user to vagrant so we don't have to mess with permissions.
sed -i "s/User \${APACHE_RUN_USER}/User vagrant/" /etc/apache2/apache2.conf
sed -i "s/Group \${APACHE_RUN_GROUP}/Group vagrant/" /etc/apache2/apache2.conf

# enable required apache modules and restart server.
a2enmod rewrite
service apache2 restart

# create database.
cat > /tmp/angelus-api.sql <<EOL
CREATE DATABASE IF NOT EXISTS angelus_irm_api;
ALTER DATABASE angelus_irm_api CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT USAGE ON angelus_irm_api.* TO 'angelusdev'@'%' IDENTIFIED BY 'angelus';
GRANT ALL PRIVILEGES ON angelus_irm_api.* TO 'angelusdev'@'%'; 
FLUSH PRIVILEGES;
EOL
mysql -uroot -pangelus -hlocalhost < /tmp/angelus-api.sql


# create laravel config environment.
cat > .env <<EOL

APP_ENV=local
APP_DEBUG=true
APP_KEY=base64:VjbJDtbZKhfj9x+zR9qD935yZi3zbvoOrboAf0Hm0TM=
APP_URL=http://localhost
APP_LOG=daily

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=angelus_irm_api
DB_USERNAME=angelusdev
DB_PASSWORD=angelus

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

EOL

# install app dependencies.
composer install -q

# database migration
php artisan migrate

# Seed
php artisan db:seed