@extends($template)

@section('content')

    <section class="content-header">
        <h1>

            <a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5"  style="text-transform:capitalize"><i class="fa fa-arrow-left mar-r-5"></i>{{ $model->get_read_title() }} List</a>
            <a href="{{ route('bakery.add', ['model_slug' => $model->get_slug()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5"  style="text-transform:capitalize">
              <i class="fa fa-plus-circle mar-r-5"></i>Add</a>
            <a href="{{ route('bakery.edit',   ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5"><i class="fa fa-pencil mar-r-5"></i>Edit</a>
            <a href="{{ route('bakery.delete', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5"><i class="fa fa-trash mar-r-5"></i>Delete</a>
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('bakery.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" style="text-transform:capitalize">{{ $model->get_read_title() }}</a></li>
            <li class="active" style="text-transform:capitalize">Viewing</li>
        </ol>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title" style="text-transform:capitalize"><b>{{ $model->get_read_title() }}</b></h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            @if( $model->get_read_attributes($entry)->count() == 0 )
                                <tr>
                                    <td class="text-center">This model does not have attributes it can display...</td>
                                </tr>
                            @else
                                @foreach( $model->get_read_attributes($entry) as $attribute )
                                    <tr>
                                        <td width="10%" class="text-center" style="text-transform:capitalize;background:#F2F2F2;border-bottom:1px solid #E2E2E2;"><strong>{{ $attribute['name'] }}</strong></td>
                                        <td width="90%" style="padding-left:20px;">{!! $attribute['value'] !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if( $model->has_browseable_relations() )
            @foreach( $model->get_browseable_relations() as $relation )
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-default">
                            <div class="box-header">
                                <div class="box-title">
                                <a href="{{ route('bakery.browse', ['model_slug' => $relation->get_related_class()->get_slug()]) }}" style="text-transform:capitalize">  {!! $relation->get_method_name(true) !!}  </a>
                                </div>
                                <div class="box-tools pull-right" style="">
                                    <a href="{{ route('bakery.browse', ['model_slug' => $relation->get_related_class()->get_slug()]) }}" class="btn btn-default btn-xs mar-t-5 mar-r-5" style="display:none;text-transform:capitalize">
                                      {!! $relation->get_method_name(true) !!}
                                    </a>
                                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="box-body table-table-responsive no-padding">
                                <table class="table table-hover">
                                    @if( ! $relation->has_related_records($entry) )
                                        <tr style="display:none">
                                            <td class="xtext-center"><!--No {{ $relation->get_method_name() }}...--></td><!-- {{ strtolower($entry->get_model()->get_classname()) }} -->
                                        </tr>
                                    @else
                                        <tr>
                                            @foreach( $relation->get_related_class()->get_overview_attributes() as $attribute )
                                                <th class="xtext-center" style="text-transform:capitalize;xwidth:10%;xbackground:red;overflow:hidden">
                                                  {{ $attribute->get_name(true) }}
                                                </th>
                                            @endforeach
                                            <th class="text-center" style="background:#F2F2F2;border-bottom:1px solid #E2E2E2;width:13%">Actions</th>
                                        <tr>
                                        @foreach( $relation->get_related_records($entry) as $related_entry )
                                            <tr>
                                                @foreach( $relation->get_related_class()->get_overview_attributes() as $attribute )
                                                    <td class="xtext-center"> {!! $related_entry->get_value($attribute->get_name(), 50, true) !!} </td>
                                                @endforeach
                                                <td class="text-center" style="background:#F2F2F2;border-bottom:1px solid #E2E2E2;">
                                                    <a href="{{ route('bakery.read',   ['model_slug' => $relation->get_related_class()->get_slug(), 'identifier' => $related_entry->get_id()]) }}" class="btn btn-xs mar-r-5">Read</a>
                                                    <a href="{{ route('bakery.edit',   ['model_slug' => $relation->get_related_class()->get_slug(), 'identifier' => $related_entry->get_id()]) }}" class="btn btn-xs text-yellow mar-r-5">Edit</a>
                                                    <a href="{{ route('bakery.delete', ['model_slug' => $relation->get_related_class()->get_slug(), 'identifier' => $related_entry->get_id()]) }}" class="btn btn-xs text-red">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </section>

@stop
