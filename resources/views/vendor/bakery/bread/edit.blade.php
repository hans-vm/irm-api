@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_data()->id]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5" style="text-transform:capitalize"><i class="fa fa-arrow-left mar-r-5"></i>View {{ $model->get_edit_title() }}</a>
            <a href="{{ route('bakery.delete', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5"><i class="fa fa-trash mar-r-5"></i>Delete</a>
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('bakery.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" style="text-transform:capitalize">{{ $model->get_edit_title() }}</a></li>
            <li class="active" style="text-transform:capitalize">Editing</li>
        </ol>

    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                @include('bakery::partials.form-errors')
                <div class="box box-warning">
                    <form action="{{ route('bakery.edit.post', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" method="post" enctype="multipart/form-data" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="box-body pad-b-0">
                            {!! $form->render() !!}
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-warning mar-r-5" style="text-transform:capitalize">Update {{ $model->get_edit_title() }}</button>
                            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </section>

@stop
