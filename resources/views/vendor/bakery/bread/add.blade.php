@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            <!-- Create <b>{{ $model->get_add_title() }}</b> record -->
            <a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5" style="text-transform:capitalize"><i class="fa fa-arrow-left mar-r-5"></i>{{ $model->get_add_title() }} List</a>
        </h1>
        <!--   -->
        <ol class="breadcrumb">
            <li><a href="{{ route('bakery.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" style="text-transform:capitalize">{{ $model->get_add_title() }}</a></li>
            <li class="active" style="text-transform:capitalize">Adding</li>
        </ol>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                @include('bakery::partials.form-errors')
                <div class="box box-success">
                    <form role="form" enctype="multipart/form-data" method="post" action="{{ route('bakery.add.post', ['model_slug' => $model->get_slug()]) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="box-body pad-b-0" style="text-transform:capitalize">
                            {!! $form->render() !!}
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success mar-r-5" style="text-transform:capitalize">Add {{ $model->get_add_title() }}</button>
                            <a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" class="btn btn-default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@stop
