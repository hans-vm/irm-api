@extends($template)

@section('content')

    <section class="content-header">
        <h1>
                  <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_data()->id]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5" style="text-transform:capitalize"><i class="fa fa-arrow-left mar-r-5"></i>Back to Entry</a>
                   <a href="{{ route('bakery.edit',   ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5 mar-r-5"><i class="fa fa-pencil mar-r-5"></i>Edit</a>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('bakery.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" style="text-transform:capitalize">{{ $model->get_delete_title() }}</a></li>
            <li class="active" style="text-transform:capitalize">Deleting</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                @include('bakery::partials.form-errors')
                <form action="{{ route('bakery.delete.post', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title" style="text-transform:capitalize">Removing {{ $model->get_delete_title() }}</h3>
                        </div>
                        <div class="box-body">
                            <p class="mar-b-0">You have requested to remove record #{{ $entry->get_id() }} from the {{ $model->get_delete_title() }} model. <br/>Are you absolutely sure you want to delete this record?</p>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-danger mar-r-10" xstyle="width:100px;" style="text-transform:capitalize">Yes, Remove {{ $model->get_delete_title() }}</button>
                            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </section><!-- /.content -->

@stop
