@extends($template)

@section('content')

    <section class="content-header">
        <h1>
            <a href="{{ route('bakery.add', ['model_slug' => $model->get_slug()]) }}" class="btn btn-sm btn-default xpull-right mar-t-5"  style="text-transform:capitalize">
              <i class="fa fa-plus-circle mar-r-5"></i>Add {{ $model->get_browse_title() }}</a>
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('bakery.dashboard') }}">Dashboard</a></li>
            <li><a href="{{ route('bakery.browse', ['model_slug' => $model->get_slug()]) }}" style="text-transform:capitalize">{{ $model->get_browse_title() }}</a></li>
            <li class="active" style="text-transform:capitalize">Browsing</li>
        </ol>


    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title" style="text-transform:capitalize">{{ $model->get_browse_title() }}</h3>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                @foreach( $model->get_overview_attributes() as $overview_attribute )
                                    <th xclass="text-center" style="text-transform:capitalize">{{ $overview_attribute->get_name(true) }}</th>
                                @endforeach
                                <th class="text-center">Actions</th>
                            </tr>
                            @if( $model->has_entries() )
                                @foreach( $model->get_entries() as $entry )
                                    <tr>
                                        @foreach( $model->get_overview_attributes() as $attribute )
                                            <td xclass="text-center"><a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}">{!! $entry->get_value($attribute->get_name(), 50) !!}</a></td>
                                        @endforeach
                                        <td class="text-center">
                                            <a href="{{ route('bakery.read', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-xs mar-r-5">Read</a>
                                            <a href="{{ route('bakery.edit', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-xs text-yellow mar-r-5">Edit</a>
                                            <a href="{{ route('bakery.delete', ['model_slug' => $model->get_slug(), 'identifier' => $entry->get_id()]) }}" class="btn btn-xs  text-red">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="{{ $model->get_overview_attributes()->count() + 1 }}">  <a style="xwidth:100%" href="{{ route('bakery.add', ['model_slug' => $model->get_slug()]) }}" class="btn xbtn-sm btn-primary xpull-right mar-t-5"  >
                                      New <span style="text-transform:capitalize">{{ $model->get_browse_title() }}</span></a></td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
