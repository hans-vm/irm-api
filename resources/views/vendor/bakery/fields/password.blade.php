<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <input type="password" id="{{ $name }}" class="form-control" name="{{ $name }}" @if($value) value="{{ $value }}" @endif />
</div>
