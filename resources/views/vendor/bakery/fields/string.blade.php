<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <input type="text" id="{{ $name }}" class="form-control" name="{{ $name }}" value="{{ old($name, $value) }}" />
</div>
