<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <select id="{{ $name }}" class="form-control" name="{{ $name }}">
        <option value="1" @if( $value == "1" ) selected @endif>Yes</option>
        <option value="0" @if( $value == "0" ) selected @endif>No</option>
    </select>
</div>
