<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-laptop"></i>
        </div>
        <input type="text" id="{{ $name }}" class="form-control" name="{{ $name }}" data-inputmask="'alias': 'ip'" data-mask value="{{ old($name, $value) }}">
    </div>
</div>
<script>
    $(function(){
        $("[data-mask]").inputmask();
    });
</script>
