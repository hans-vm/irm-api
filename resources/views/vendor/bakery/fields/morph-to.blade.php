<div class="form-group">
    <label for="{{ $name }}_type">What is {{ $name }}?</label>
    <select id="{{ $name }}_type" class="form-control select2" name="{{ $name }}_type" @if($options['morph_types']->count() == 0) disabled @endif>
        @if( $options['morph_types']->count() == 0 )
            <option value="">There are no possible related models to choose from</option>
        @else
            @foreach( $options['morph_types'] as $type )
                <option value="{{ $type['model']->get_namespaced_classname() }}">{{ $type['model']->get_classname() }}</option>
            @endforeach
        @endif
    </select>
</div>

@if( $options['morph_types']->count() > 0 )
    @foreach( $options['morph_types'] as $type )
        <div class="form-group child">
            <label for="{{ strtolower($type['model']->get_classname()) }}">{{ ucfirst($name) }} {{ strtolower($type['model']->get_classname()) }}:</label>
            <select id="{{ strtolower($type['model']->get_classname()) }}" class="form-control select2 child-selector" name="{{ strtolower($type['model']->get_classname()) }}" @if( ! $type['model']->has_entries() ) disabled @endif>
                @if( ! $type['model']->has_entries() )
                    <option value="">There are no {{ str_plural(strtolower($type['model']->get_classname())) }} to choose from</option>
                @else
                    @foreach( $type['model']->get_entries() as $entry )
                        <option value="{{ $entry->get_id() }}">{{ $entry->get_value($type['title']) }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    @endforeach
@endif

@if( is_object($value) and $value->count() > 0 )
    <input type="hidden" id="selected_type" value="{{ $value->get('type') }}"  />
    <input type="hidden" id="selected_id"   value="{{ $value->get('child') }}" />
@endif

<script>
    $(document).ready(function(){

        // Initialize and save our select elements
        type_select   = $("#{{ $name }}_type").select2();
        child_selects = $(".child-selector").select2();

        // Initialize the default setup
        display_appropriate_form();

        // Whenever the morphable type select changes we (might) need to update the form
        type_select.on("change", function(){
            display_appropriate_form();
        });

        // Function to update the form
        function display_appropriate_form()
        {
            $(".child").hide();
            var selected_option = $("#{{ $name }}_type option:selected").text().toLowerCase();
            var child_selects   = $(".child-selector").length;
            for( var i = 0; i < child_selects; i++ ){
                var select = $($(".child-selector")[i]);
                if( select.attr('id') == selected_option ){
                    select.parent('.child').show();
                }
            }
        }

        // If we were given some values to work with
        @if( is_object($value) and $value->count() > 0 )
            // Grab the selected type and its identifier from the hidden form fields
            var type       = $("#selected_type").val();
            var identifier = $("#selected_id").val();
            // Set the morphable type selector to its appropriate value
            type_select.val(type).trigger('change');
            // Determine the element id of the selected child's select
            var child_id   = $("#{{ $name }}_type option:selected").text().toLowerCase();
            // And set that to the correct value as well
            $("#"+child_id).val(identifier).trigger('change');
        @endif

    });
</script>
