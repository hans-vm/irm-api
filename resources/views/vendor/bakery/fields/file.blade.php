<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    @if( !is_null($value) )<p>Current: <i>@if( $value != "" ) {{ $value }} @else Nothing set yet @endif</i></p>@endif
    <input type="file" id="{{ $name }}" name="{{ $name }}" />
</div>
