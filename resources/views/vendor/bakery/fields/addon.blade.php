<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    @if( $description )<p class="text-muted">{{ $description }}</p>@endif
    <div class="input-group">
        @if( $prepend_addon )
            <div class="input-group-addon">{{ $prepend_addon }}</div>
        @endif
        <input type="text" id="{{ $name }}" class="form-control" name="{{ $name }}" value="{{ old($name, $value) }}" />
        @if( $append_addon )
            <div class="input-group-addon">{{ $append_addon }}</div>
        @endif
    </div>
</div>
