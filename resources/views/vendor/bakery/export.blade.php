@extends($template)

@section('content')

    <section class="content-header">
        <h1>Exporting</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                @include('bakery::partials.success')
                @include('bakery::partials.form-errors')
                <form method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Export Records</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <p>Select from which model(s) you would like to export records:</p>
                            <select name="models[]" multiple class="form-control" style="height: 150px;">
                                @foreach( $models as $model )
                                    <option value="{{ $model->get_slug() }}">{{ $model->get_classname() }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="box-footer">
                          <button type="submit" class="btn btn-success mar-r-5" formaction="{{ route('bakery.export.as', 'pdf') }}" ><i class="fa fa-file-pdf-o mar-r-5"></i>Export as PDF</button>    <!-- -->
                            <button type="submit" class="btn btn-success" formaction="{{ route('bakery.export.as', 'excel') }}"><i class="fa fa-file-excel-o mar-r-5"></i>Export as XLS</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

@stop
