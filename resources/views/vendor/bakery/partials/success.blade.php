@if( session('successMessages') )
    <div class="callout callout-success">
        <ul class="list-unstyled">
            @foreach( session('successMessages') as $message )
                <li>{{ $message }}</li>
            @endforeach
        </ul>
    </div>
    <script>$(".callout").delay(3000).fadeOut(1000);</script>
@endif
