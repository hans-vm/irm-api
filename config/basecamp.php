<?php

return [

    'head_title' => 'Angelus IRM',
    'title' => '<b>Angelus</b>IRM',
    'title_small' => '<b>IRM</b>',
    'footer_text' => 'Angelus Investor Dashboard - Version 1.0.1',

    /**The AdminLTE skin we are using
     *
     *Options: `blue`, `red`, `green`, `yellow`, `purple`, `black`, `blue-light`, `black-light`, `purple-light`, `green-light`, `red-light`, `yellow-light`
     */
    'skin' => 'black-light',
    'middleware' => []
];
