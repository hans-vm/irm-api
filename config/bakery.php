<?php

return [
    'middleware' => [],
    'template' => 'basecamp::templates.lte',
    'model_folders' => [
        'app/Models',
    ],
    'model_exclusions' => [
        'App\Models\BaseModel'
    ],
    'browse_max_attributes' => '99',
    'browse_exclude_attributes' => ['id', 'uuid', 'created_at', 'updated_at' ],
    'read_exclude_attributes' => ['id'],
    'custom_fields' => [
        'datetime-local' => 'Verheijen\\Bakery\\Objects\\Fields\\CustomDateTimeLocalField'
    ],
    'default_image_upload_path' => 'assets/uploads/images',
    'default_file_upload_path' => 'assets/uploads/files',
];
