<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;
use App\Models\Investor;
use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\Investment;
use App\Models\InvestmentTransaction;

class TransferTest extends TestCase
{
	use DatabaseTransactions;

	/**
	 * Test Admin User
	 * @var User
	 */
	protected $adminUser;

	/**
	 * Test Admin User Token
	 * @var string
	 */
	protected $userToken;

	/**
	 * Investor of Test Admin User
	 * @var Investor
	 */
	protected $investor;

	/**
	 * Account of Test User Investor
	 * @var Account
	 */
	protected $account;

	/**
	 * Account Transaction
	 * @var AccounTransaction
	 */
	protected $accountTransaction;

	/**
	 * Investment of Test User Investor
	 * @var Investment
	 */
	protected $investment;

	/**
	 * Investment Transaction
	 * @var InvestmentTransaction
	 */
	protected $investmentTransaction;

	/**
	 * Set up
	 */
	protected function setUp() 
	{
		parent::setUp();

		// Create admin user
		$this->adminUser = factory(User::class, 'admin')->create();

		// User token
        $this->userToken = JWTAuth::fromUser($this->adminUser);

        // Create an investor and save it to admin user
        $this->investor = factory(Investor::class)->make();
        $this->adminUser->investors()->save($this->investor);

    	// Create an account for the investor
        $this->account = factory(Account::class)->make();
        $this->investor->accounts()->save($this->account);

        // Add account balance 100
        $this->accountTransaction = factory(AccountTransaction::class)->make([
        	'amount' => 100,
        	'account_transaction_status_id' => 1,
        ]);
        $this->account->transactions()->save($this->accountTransaction);

        // Create an investment for the investor
        $this->investment = factory(Investment::class)->make();
        $this->investor->investments()->save($this->investment);

        // Add investment balance 100
        $this->investmentTransaction = factory(InvestmentTransaction::class)->make([
        	'amount' => 100,
        	'investment_transaction_status_id' => 1,
        ]);
        $this->investment->transactions()->save($this->investmentTransaction);
	}

    /**
     * Test Investment -> Account Transfer
     * 
     * @return void
     */
    public function testInvestment2Account()
    {
    	// Insufficient funds
        $params = [
        	'source_type' => 'Investment',
        	'source_id'   => $this->investment->id,
        	'destination_type' => 'Account',
        	'destination_id'   => $this->account->id,
        	'type'		=> 1,
        	'status'	=> 1,
        	'amount'	=> 500
		];

        $request = $this->json('POST', '/v1/transfer', $params, ['Authorization' => 'Bearer ' . $this->userToken]);

        // check response status
        $request->assertResponseStatus(400);			
        
        // check response
		$request->seeJson([
			'code' => 400,
		 	'message' => 'Insufficient funds.'	
		]);

    	// Successful transfer
        $params = [
        	'source_type' => 'Investment',
        	'source_id'   => $this->investment->id,
        	'destination_type' => 'Account',
        	'destination_id'   => $this->account->id,
        	'type'		=> 1,
        	'status'	=> 1,
        	'amount'	=> 50
		];

        $request = $this->json('POST', '/v1/transfer', $params, ['Authorization' => 'Bearer ' . $this->userToken]);

        // check response status
        $request->assertResponseStatus(200);			
        
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// Check investment balance
		$this->assertEquals(50, $this->investment->getBalance());

		// Check account balance
		$this->assertEquals(150, $this->account->getBalance());

		// Check if the newly created investment transaction is linked to the newly created account transaction.
		// Get newly created account transaction
		$newAccountTransaction = $this->account->transactions()->where('id', '!=', $this->accountTransaction->id)->first();
		$this->assertNotNull($newAccountTransaction);

		// Get newly created investment transaction
		$newInvestmentTransaction = $this->investment->transactions()->where('id', '!=', $this->investmentTransaction->id)->first();
		$this->assertNotNull(0, $newInvestmentTransaction->id);

		$this->assertEquals($newAccountTransaction->id, $newInvestmentTransaction->accountTransaction->id);
    }

    /**
     * Test Investment -> Investment Transfer
     * 
     * @return void
     */
    public function testInvestment2Investment()
    {
    	// Create an investment for the investor
        $otherInvestment = factory(Investment::class)->make();
        $this->investor->investments()->save($otherInvestment);

        // Add investment balance 100
        $otherInvestmentTransaction = factory(InvestmentTransaction::class)->make([
        	'amount' => 100,
        	'investment_transaction_status_id' => 1,
        ]);
        $otherInvestment->transactions()->save($otherInvestmentTransaction);

    	// Insufficient funds
        $params = [
        	'source_type' => 'Investment',
        	'source_id'   => $this->investment->id,
        	'destination_type' => 'Investment',
        	'destination_id'   => $otherInvestment->id,
        	'type'		=> 1,
        	'status'	=> 1,
        	'amount'	=> 500
		];

        $request = $this->json('POST', '/v1/transfer', $params, ['Authorization' => 'Bearer ' . $this->userToken]);

        // check response status
        $request->assertResponseStatus(400);			
        
        // check response
		$request->seeJson([
			'code' => 400,
		 	'message' => 'Insufficient funds.'	
		]);

    	// Successful transfer
        $params = [
        	'source_type' => 'Investment',
        	'source_id'   => $this->investment->id,
        	'destination_type' => 'Investment',
        	'destination_id'   => $otherInvestment->id,
        	'type'		=> 1,
        	'status'	=> 1,
        	'amount'	=> 50
		];

        $request = $this->json('POST', '/v1/transfer', $params, ['Authorization' => 'Bearer ' . $this->userToken]);

        // check response status
        $request->assertResponseStatus(200);			
        
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// Check investment balance
		$this->assertEquals(50, $this->investment->getBalance());

		// Check account balance
		$this->assertEquals(150, $otherInvestment->getBalance());
    }

    /**
     * Test Account -> Account Transfer
     * 
     * @return void
     */
    public function testAccount2Account()
    {
    	// Create an investment for the investor
        $otherAccount = factory(Account::class)->make();
        $this->investor->investments()->save($otherAccount);

        // Add investment balance 100
        $otherAccountTransaction = factory(AccountTransaction::class)->make([
        	'amount' => 100,
        	'account_transaction_status_id' => 1,
        ]);
        $otherAccount->transactions()->save($otherAccountTransaction);

    	// Insufficient funds
        $params = [
        	'source_type' => 'Account',
        	'source_id'   => $this->account->id,
        	'destination_type' => 'Account',
        	'destination_id'   => $otherAccount->id,
        	'type'		=> 1,
        	'status'	=> 1,
        	'amount'	=> 500
		];

        $request = $this->json('POST', '/v1/transfer', $params, ['Authorization' => 'Bearer ' . $this->userToken]);

        // check response status
        $request->assertResponseStatus(400);			
        
        // check response
		$request->seeJson([
			'code' => 400,
		 	'message' => 'Insufficient funds.'	
		]);

    	// Successful transfer
        $params = [
        	'source_type' => 'Account',
        	'source_id'   => $this->account->id,
        	'destination_type' => 'Account',
        	'destination_id'   => $otherAccount->id,
        	'type'		=> 1,
        	'status'	=> 1,
        	'amount'	=> 50
		];

        $request = $this->json('POST', '/v1/transfer', $params, ['Authorization' => 'Bearer ' . $this->userToken]);

        // check response status
        $request->assertResponseStatus(200);			
        
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// Check investment balance
		$this->assertEquals(50, $this->account->getBalance());

		// Check account balance
		$this->assertEquals(150, $otherAccount->getBalance());
    }

    /**
     * Test empty `status` field, which is of 'select' type.
     * Should not complain about it and populate default value from Model info
     * 
     * @return void
     */
    public function testEmptyStatus() {
        $params = [
            'source_type' => 'Investment',
            'source_id'   => $this->investment->id,
            'destination_type' => 'Account',
            'destination_id'   => $this->account->id,
            'type'      => 1,
            'amount'    => 50
        ];

        $this->json('POST', '/v1/transfer', $params, ['Authorization' => 'Bearer ' . $this->userToken])
            ->assertResponseStatus(200)
            ->seeJson([
                'code' => 0,
                'message' => 'success'  
            ]);
    }
}
