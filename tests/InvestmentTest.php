<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Investment;
use App\Models\InvestmentStatus;
use App\Models\InvestmentTransaction;
use App\Models\InvestmentTransactionStatus;
use App\Models\InvestmentTransactionType;

class InvestmentTest extends TestCase
{
	/** 
	 * Test cases 
	 */
	protected static $TestAuthUser = [
		'email'		=> 'admin@test.com',
		'password'	=> 'admin'
	];

	protected static $TestInvestments = [
		[
			'title' 		=> 'Testing investment 01', 
			'project' 		=> 1, 
			'status' 		=> 1,
			'investor'   	=> 1,
		],
		[
			'title' 		=> 'Testing investment 02', 
			'project' 		=> 2, 
			'status' 		=> 2,
			'investor'   	=> 2,
		],
		[
			'title' 		=> 'Testing investment 03', 
			'project' 		=> 3,
			'status' 		=> 3,
			'investor'   	=> 3,
		],
	];

	protected static $TestInvestmentTransactions = [

		[
			'title' => 'Test investment transaction 01', 
			'type' => 1, 
			'status' => 1
		],
		[
			'title' => 'Test investment transaction 02', 
			'type' => 2,
			'status' => 3
		]

	];

    /**
     * Investment related API testing
     *
     * @return void
     */
    public function testInvestment()
    {

        // making auth request
        $request = $this->json('POST', '/v1/user/auth', static::$TestAuthUser);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);

        // user token
        $userToken = $responseContent['data']['token'];

        print_r("Testing investment model information endpoint : started\n");

        // assert model info
        $request = $this->json('GET', '/v1/investment/info', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['fields', 'preview', 'form']
        ]);

        print_r("Testing investment model information endpoint : finished\n");

        print_r("Testing investment list endpoint : started\n");

        // assert investments list api
		$request = $this->json('GET', '/v1/investment', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => [
        		'*'	=> [
        			"id", "title", "note", "investment_date", "project", "status", "transactions", "balance"
        		]
        	]
        ]);  

        print_r("Testing investment list endpoint : finished\n");      
        
        foreach(static::$TestInvestments as $investment) {

        	print_r("Testing investment creation endpoint : started\n");  

	        // creating new investment
	        $request = $this->json('POST', '/v1/investment', $investment, ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);

	        // validate created investment
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'title' 	=> $investment['title']
			 	]
			]);

			// get content 
	        $responseContent = json_decode($request->response->getContent(), true);

	        // created investment id 
	        $investmentId = $responseContent['data']['id'];

	        print_r("Testing investment creation endpoint : finished\n");  

	        print_r("Testing investment details endpoint : started\n");

	        // assert investment details api
	        $request = $this->json('GET', '/v1/investment/' . $investmentId, [], ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'title' 	=> $investment['title']
			 	]
			]);

			print_r("Testing investment details endpoint : finished\n");

			print_r("Testing investment update endpoint : started\n");

			// assert investment update api
			$investment['title'] = $investment['title'] . ' Updated';
			
	        $request = $this->json('PUT', '/v1/investment/' . $investmentId, $investment, ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'title' 	=> $investment['title']
			 	]
			]);

			print_r("Testing investment update endpoint : finished\n");


			// testing investment transaction api endpoints
			foreach (static::$TestInvestmentTransactions as $transaction) {

				print_r("Testing investment transaction creation endpoint : started\n");  

				$transactionData = $transaction;
				$transactionData['investment_id'] = $investmentId;

		        // creating new investment transaction
		        $request = $this->json('POST', '/v1/investment/transaction', $transactionData, ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);

		        // validate created investment
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success',
				 	'data' 		=> $transaction
				]);

				// get content 
		        $responseContent = json_decode($request->response->getContent(), true);

		        // created investment transaction id
		        $transactionId = $responseContent['data']['id'];

		        print_r("Testing investment transaction creation endpoint : finished\n");

		        print_r("Testing investment transaction details endpoint : started\n");

		        // assert investment transaction details api
		        $request = $this->json('GET', '/v1/investment/transaction/' . $transactionId, [], ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);			
		        // check structure and content
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success',
				 	'data' 		=> $transaction
				]);

				print_r("Testing investment transaction details endpoint : finished\n");

				print_r("Testing investment transaction update endpoint : started\n");

				// assert investment transaction update api
				$transaction['title'] = $transaction['title'] . ' Updated';
				$transactionData = $transaction;
				$transactionData['investment_id'] = $investmentId;

		        $request = $this->json('PUT', '/v1/investment/transaction/' . $transactionId, $transactionData, ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);			
		        // check structure and content
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success',
				 	'data' 		=> $transaction
				]);

				print_r("Testing investment transaction update endpoint : finished\n");

				print_r("Testing investment transaction delete endpoint : started\n");

				// assert investment delete api
		        $request = $this->json('DELETE', '/v1/investment/transaction/' . $transactionId, [], ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);			
		        // check structure and content
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success'
				]);

				print_r("Testing investment transaction delete endpoint : finished\n");

			}


			print_r("Testing investment delete endpoint : started\n");

			// assert investment delete api
	        $request = $this->json('DELETE', '/v1/investment/' . $investmentId, [], ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success'
			]);

			print_r("Testing investment delete endpoint : finished\n");			

			// remove test data completely from database
			Investment::withTrashed()->find($investmentId)->forceDelete();
    	}

    }
}
