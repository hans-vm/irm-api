<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Investor;
use App\Models\InvestorType;
use App\Models\InvestorStatus;


class InvestorTest extends TestCase
{
	
	/** 
	 * Test cases 
	 */
	protected static $TestAuthUser = [
		'email'		=> 'admin@test.com',
		'password'	=> 'admin'
	];

	protected static $TestInvestors = [
		[
			'name' 		=> 'Testing investor 01', 
			'type' 		=> 1, 
			'status' 	=> 1,
			'user' 		=> 1,
		],
		[
			'name' 		=> 'Testing investor 02', 
			'type' 		=> 2, 
			'status'	=> 2,
			'user' 		=> 1
		],
		[
			'name' 		=> 'Testing investor 03', 
			'type' 		=> 3,
			'status' 	=> 3,
			'user' 		=> 1
		],
	];


    /**
     * Investor related API testing
     *
     * @return void
     */
    public function testInvestor()
    {

        // making auth request
        $request = $this->json('POST', '/v1/user/auth', static::$TestAuthUser);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);

        // user token
        $userToken = $responseContent['data']['token'];

        print_r("Testing investor model information endpoint : started\n");

        // assert model info
        $request = $this->json('GET', '/v1/investor/info', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['fields', 'preview', 'form']
        ]);

        print_r("Testing investor model information endpoint : finished\n");

        print_r("Testing investor list endpoint : started\n");

        // assert investors list api
		$request = $this->json('GET', '/v1/investor', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => [
        		'*'	=> [
        			"id", "name", "type", "status", "accounts", "investments", "documents"
        		]
        	]
        ]);  

        print_r("Testing investor list endpoint : finished\n");      
        
        foreach(static::$TestInvestors as $investor) {

        	print_r("Testing investor creation endpoint : started\n");  

	        // creating new investor
	        $request = $this->json('POST', '/v1/investor', $investor, ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);

	        // validate created investor
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'name' 		=> $investor['name']
			 	]
			]);

			// get content 
	        $responseContent = json_decode($request->response->getContent(), true);

	        // created investor id 
	        $investorId = $responseContent['data']['id'];

	        print_r("Testing investor creation endpoint : finished\n");  

	        print_r("Testing investor details endpoint : started\n");

	        // assert investor details api
	        $request = $this->json('GET', '/v1/investor/' . $investorId, [], ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'name' 		=> $investor['name']
			 	]
			]);

			print_r("Testing investor details endpoint : finished\n");

			print_r("Testing investor update endpoint : started\n");

			// assert investor update api
			$investor['name'] = $investor['name'] . ' Updated';
			
	        $request = $this->json('PUT', '/v1/investor/' . $investorId, $investor, ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'name' 		=> $investor['name']
			 	]
			]);

			print_r("Testing investor update endpoint : finished\n");

			print_r("Testing investor delete endpoint : started\n");

			// assert investor delete api
	        $request = $this->json('DELETE', '/v1/investor/' . $investorId, [], ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success'
			]);

			print_r("Testing investor delete endpoint : finished\n");			

			// remove test data completely from database
			Investor::withTrashed()->find($investorId)->forceDelete();
    	}

    }
}
