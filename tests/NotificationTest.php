<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;
use App\Models\Investor;
use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\Investment;
use App\Models\InvestmentTransaction;

class NotificationTest extends TestCase
{
	use DatabaseTransactions;

	/**
	 * Test Admin User
	 * 
	 * @var User
	 */
	protected $adminUser;

	/**
	 * Test Admin User Token
	 * 
	 * @var string
	 */
	protected $adminUserToken;

	/**
	 * Investor of Test Admin User
	 * 
	 * @var Investor
	 */
	protected $investor;

	/**
	 * Account of Test User Investor
	 * 
	 * @var Account
	 */
	protected $account;

	/**
	 * Investment of Test User Investor
	 * 
	 * @var Investment
	 */
	protected $investment;

	/**
	 * Set up
	 */
	protected function setUp() 
	{
		parent::setUp();

		// Create admin user
		$this->adminUser = factory(User::class, 'admin')->create();

		// User token
        $this->adminUserToken = JWTAuth::fromUser($this->adminUser);

        // Create an investor and save it to admin user
        $this->investor = factory(Investor::class)->make();
        $this->adminUser->investors()->save($this->investor);

    	// Create an account for the investor
        $this->account = factory(Account::class)->make();
        $this->investor->accounts()->save($this->account);

        // Create an investment for the investor
        $this->investment = factory(Investment::class)->make();
        $this->investor->investments()->save($this->investment);
	}

    /**
     * Test Notification CRUD
     * 
     * @return void
     */
    public function testNotifications()
    {
    	// Post notification params
        $params = [
	 		'title'					=> 'Notification Title 1',
	 		'message'				=> 'Notification Message 1',
	 		'action'				=> 'Notification Alert 1',
	        'notifiable_id'			=> $this->account->id,
	        'notifiable_type'		=> 'Account',
	        'type'					=> 1,
	        'display'				=> [
	        	'display_mode_alert',
	        	'display_mode_sidebar'	
	        ]
  		];

        $request = $this->json('POST', '/v1/notification', $params, ['Authorization' => 'Bearer ' . $this->adminUserToken])
				        ->assertResponseStatus(200)
				        ->seeJson([
							'code' => 0,
						 	'message' => 'success'
						]);

		// Get newly created notification id
        $responseContent = json_decode($request->response->getContent(), true);
        $notificationId1 = $responseContent['data']['id'];

		// Newly created notification should have been linked to the Admin User
		$this->assertEquals(1, $this->adminUser->notifications()->count());

		// Post notification params
        $params = [
	 		'title'					=> 'Notification Title 2',
	 		'message'				=> 'Notification Message 2',
	 		'action'				=> 'Notification Alert 2',
	        'notifiable_id'			=> $this->investment->id,
	        'notifiable_type'		=> 'Investment',
	        'type'					=> 2,
	        'display'				=> [
	        	'display_mode_alert',
		        'display_mode_menu',
		        'display_mode_sidebar',
		        'display_mode_email'
	        ]
  		];

        $request = $this->json('POST', '/v1/notification', $params, ['Authorization' => 'Bearer ' . $this->adminUserToken])
				        ->assertResponseStatus(200)
				        ->seeJson([
							'code' => 0,
						 	'message' => 'success'	
						]);

		// Get newly created notification id
        $responseContent = json_decode($request->response->getContent(), true);
        $notificationId2 = $responseContent['data']['id'];

		// Newly created notification should have been linked to the Admin User
		$this->assertEquals(2, $this->adminUser->notifications()->count());

		// Get 2 newly created notifications
        $this->json('GET', '/v1/notification', [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
			->assertResponseStatus(200)
			->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data' => [
			 		[ 'id' => $notificationId1 ],
			 		[ 'id' => $notificationId2 ]
			 	]
			]);

		// Test show endpoint
		$this->json('GET', '/v1/notification/' . $notificationId2, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
			->assertResponseStatus(200)
			->seeJsonSubset([
				'code' => 0,
		 		'message' => 'success',
		 		'data' => $params
			]);

		// Test show endpoint with another user
		$anotherUser = factory(User::class, 'admin')->create();
        $anotherUserToken = JWTAuth::fromUser($anotherUser);

		$this->json('GET', '/v1/notification/' . $notificationId2, [], ['Authorization' => 'Bearer ' . $anotherUserToken])
			->assertResponseStatus(404)
			->seeJsonSubset([
				'code' => 404,
		 		'message' => 'Notification is not found.'
			]);

		// Test Mark As Read
		$this->json('PUT', '/v1/notification/' . $notificationId1 . '/markAsRead', [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
			->assertResponseStatus(200)
			->seeJson([
				'code' => 0,
				'message' => 'success'
			]);

		$this->json('GET', '/v1/notification/' . $notificationId1, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
			->assertResponseStatus(200)
			->seeJsonSubset([
				'code' => 0,
		 		'message' => 'success',
		 		'data' => [
		 			'id' => $notificationId1,
		 			'is_read' => true
		 		]
			]);

		// Test Mark As Unread
		$this->json('PUT', '/v1/notification/' . $notificationId1 . '/markAsUnread', [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
			->assertResponseStatus(200)
			->seeJson([
				'code' => 0,
				'message' => 'success'
			]);

		$this->json('GET', '/v1/notification/' . $notificationId1, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
			->assertResponseStatus(200)
			->seeJsonSubset([
				'code' => 0,
		 		'message' => 'success',
		 		'data' => [
		 			'id' => $notificationId1,
		 			'is_read' => false
		 		]
			]);
    }
}
