<?php

use App\Models\SettingsField;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class SettingsTest extends TestCase
{
	use DatabaseTransactions;

	/**
	 * Test User
	 * 
	 * @var User
	 */
	protected $testUser;

	/**
	 * Test User Token
	 * 
	 * @var string
	 */
	protected $testUserToken;

	/**
	 * Set up
	 */
	protected function setUp() 
	{
		parent::setUp();

		// Create test user
		$this->testUser = factory(User::class)->create();

		// Create test user token
		$this->testUserToken = JWTAuth::fromUser($this->testUser);
	}

    /**
     * Test default settings
     * 
     * Since we test with a newly created user, 
     * GET /settings should respond with default settings
     * 
     * @return void
     */
    public function testDefaultSettings()
    {
    	// Mock up response data
    	$data = SettingsField::all()->groupBy('group.category')->map(function ($fields) {
            return $fields->filter(function ($field) {
            	// Filter out linked fields
            	return empty($field->linked_field);
            })->map(function ($field) {
                return [
                    'id'    =>  $field->id,
                    'value' =>  $field->default_value
                ];
            });
    	})->toArray();
    	
        $this->json('GET', '/v1/settings', [], ['Authorization' => 'Bearer ' . $this->testUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data'	  => $data
			]);
    }

    /**
     * Test linked fields
     * 
     * Since we test with a newly created user, 
     * GET /settings response should have first name & last name of the user
     * 
     * @return void
     */
    public function testLinkedFields()
    {
        $this->json('GET', '/v1/settings', [], ['Authorization' => 'Bearer ' . $this->testUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data'	  => [
			 		'user'	=>  [
			 			// 0 => [ title ]
			 			1 => [ 'value' => $this->testUser->first_name ],
			 			2 => [ 'value' => $this->testUser->last_name ],
			 		]
			 	]
			]);
    }

}
