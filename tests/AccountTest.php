<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Account;
use App\Models\AccountType;
use App\Models\AccountStatus;
use App\Models\AccountTransaction;
use App\Models\AccountTransactionStatus;
use App\Models\AccountTransactionType;

class AccountTest extends TestCase
{
	/** 
	 * Test cases 
	 */
	protected static $TestAuthUser = [
		'email'		=> 'admin@test.com',
		'password'	=> 'admin'
	];

	protected static $TestAccounts = [
		[
			'title' 	  => 'Testing account 01', 
			'type' 		  => 1, 
			'status' 	  => 1,
			'investor' 	  => 1,
		],
		[
			'title' 	  => 'Testing account 02', 
			'type' 		  => 2, 
			'status' 	  => 2,
			'investor'    => 2,
		],
		[
			'title' 	  => 'Testing account 03', 
			'type' 		  => 3,
			'status' 	  => 3,
			'investor'    => 3,
		],
	];

	protected static $TestAccountTransactions = [

		[
			'title' => 'Test account transaction 01', 
			'type' => 1, 
			'status' => 1
		],
		[
			'title' => 'Test account transaction 02', 
			'type' => 2,
			'status' => 3
		]

	];

    /**
     * Account related API testing
     *
     * @return void
     */
    public function testAccount()
    {

        // making auth request
        $request = $this->json('POST', '/v1/user/auth', static::$TestAuthUser);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);

        // user token
        $userToken = $responseContent['data']['token'];

        print_r("Testing account model information endpoint : started\n");

        // assert model info
        $request = $this->json('GET', '/v1/account/info', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['fields', 'preview', 'form']
        ]);

        print_r("Testing account model information endpoint : finished\n");

        print_r("Testing account list endpoint : started\n");

        // assert accounts list api
		$request = $this->json('GET', '/v1/account', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => [
        		'*'	=> [
        			"id", "title", "note", "account_date", "type", "status", "transactions", "balance"
        		]
        	]
        ]);  

        print_r("Testing account list endpoint : finished\n");      
        
        foreach(static::$TestAccounts as $account) {

        	print_r("Testing account creation endpoint : started\n");  

	        // creating new account
	        $request = $this->json('POST', '/v1/account', $account, ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);

	        // validate created account
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'title'		=> $account['title']
		 		]
			]);

			// get content 
	        $responseContent = json_decode($request->response->getContent(), true);

	        // created account id 
	        $accountId = $responseContent['data']['id'];

	        print_r("Testing account creation endpoint : finished\n");  

	        print_r("Testing account details endpoint : started\n");

	        // assert account details api
	        $request = $this->json('GET', '/v1/account/' . $accountId, [], ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'title'		=> $account['title']
			 	]
			]);

			print_r("Testing account details endpoint : finished\n");

			print_r("Testing account update endpoint : started\n");

			// assert account update api
			$account['title'] = $account['title'] . ' Updated';
			
	        $request = $this->json('PUT', '/v1/account/' . $accountId, $account, ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'title'		=> $account['title']
			 	]
			]);

			print_r("Testing account update endpoint : finished\n");


			// testing account transaction api endpoints
			foreach (static::$TestAccountTransactions as $transaction) {

				print_r("Testing account transaction creation endpoint : started\n");  

				$transactionData = $transaction;
				$transactionData['account_id'] = $accountId;

		        // creating new account transaction
		        $request = $this->json('POST', '/v1/account/transaction', $transactionData, ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);

		        // validate created account
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success',
				 	'data' 		=> $transaction
				]);

				// get content 
		        $responseContent = json_decode($request->response->getContent(), true);

		        // created account transaction id
		        $transactionId = $responseContent['data']['id'];

		        print_r("Testing account transaction creation endpoint : finished\n");

		        print_r("Testing account transaction details endpoint : started\n");

		        // assert account transaction details api
		        $request = $this->json('GET', '/v1/account/transaction/' . $transactionId, [], ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);			
		        // check structure and content
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success',
				 	'data' 		=> $transaction
				]);

				print_r("Testing account transaction details endpoint : finished\n");

				print_r("Testing account transaction update endpoint : started\n");

				// assert account transaction update api
				$transaction['title'] = $transaction['title'] . ' Updated';
				$transactionData = $transaction;
				$transactionData['account_id'] = $accountId;

		        $request = $this->json('PUT', '/v1/account/transaction/' . $transactionId, $transactionData, ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);			
		        // check structure and content
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success',
				 	'data' 		=> $transaction
				]);

				print_r("Testing account transaction update endpoint : finished\n");

				print_r("Testing account transaction delete endpoint : started\n");

				// assert account delete api
		        $request = $this->json('DELETE', '/v1/account/transaction/' . $transactionId, [], ['Authorization' => 'Bearer ' . $userToken]);
		        // check if response is ok
		        $request->assertResponseStatus(200);			
		        // check structure and content
				$request->seeJsonSubset([
					'code' 		=> 0,
					'message'	=> 'success'
				]);

				print_r("Testing account transaction delete endpoint : finished\n");

			}


			print_r("Testing account delete endpoint : started\n");

			// assert account delete api
	        $request = $this->json('DELETE', '/v1/account/' . $accountId, [], ['Authorization' => 'Bearer ' . $userToken]);
	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        // check structure and content
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success'
			]);

			print_r("Testing account delete endpoint : finished\n");			

			// remove test data completely from database
			Account::withTrashed()->find($accountId)->forceDelete();
    	}

    }
}
