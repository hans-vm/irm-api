<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;
use App\Models\Investor;
use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\Investment;
use App\Models\InvestmentTransaction;
use App\Models\Project;

class UpdateTest extends TestCase
{
	use DatabaseTransactions;

	/**
	 * Test Admin User
	 * 
	 * @var User
	 */
	protected $adminUser;

	/**
	 * Test Admin User Token
	 * 
	 * @var string
	 */
	protected $adminUserToken;

	/**
	 * Investor of Test Admin User
	 * 
	 * @var Investor
	 */
	protected $adminInvestor;

	/**
	 * Test Client User
	 * 
	 * @var User
	 */
	protected $clientUser;

	/**
	 * Test Client User Token
	 * 
	 * @var string
	 */
	protected $clientUserToken;

	/**
	 * Investor of Test Client User
	 * 
	 * @var Investor
	 */
	protected $clientInvestor;

	/**
	 * Project on which Test Admin & Client User Invested
	 * 
	 * @var Project
	 */
	protected $project;

	/**
	 * Post Update params
	 * 
	 * @var array
	 */
    protected $updateParams = [
		'title'		=> 'Update Title 1',
		'type'		=> 1,
		'status'	=> 1,
	];

	/**
	 * Set up
	 */
	protected function setUp() 
	{
		parent::setUp();

		// Create admin user
		$this->adminUser = factory(User::class, 'admin')->create();
		// User token
        $this->adminUserToken = JWTAuth::fromUser($this->adminUser);

        // Create an investor and save it to admin user
        $this->adminInvestor = factory(Investor::class)->make();
        $this->adminUser->investors()->save($this->adminInvestor);

		// Create client user
		$this->clientUser = factory(User::class, 'client')->create();
		// User token
        $this->clientUserToken = JWTAuth::fromUser($this->clientUser);

        // Create an investor and save it to client user
        $this->clientInvestor = factory(Investor::class)->make();
        $this->clientUser->investors()->save($this->clientInvestor);

        // Project
    	$this->project = factory(Project::class)->create();

    	// Associate an admin investment on the project
    	$adminInvestment = factory(Investment::class)->make();
    	$this->adminInvestor->investments()->save($adminInvestment);
    	$adminInvestment->project()->associate($this->project);
    	$adminInvestment->save();

    	// Associate a client investment on the project
    	$clientInvestment = factory(Investment::class)->make();
    	$this->clientInvestor->investments()->save($clientInvestment);
    	$clientInvestment->project()->associate($this->project);
    	$clientInvestment->save();
	}

	/**
	 * Test post update with client token.
  	 * Should fail with not authorized.
  	 * 
	 * @return void
	 */
	public function testClientPostUpdate()
	{
  		// Test post update with client token
  		// Should fail with not authorized.
  		$this->json('POST', '/v1/update', [], ['Authorization' => 'Bearer ' . $this->clientUserToken])
  			->assertResponseStatus(401)
  			->seeJson([
  				'code' => 401,
  				'message' => 'Not authorized'
  			]);
	}

    /**
     * Test Updates CRUD
     * 
     * @return void
     */
    public function testUpdates()
    {
    	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		// Test post update
  		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $request = $this->json('POST', '/v1/update', $this->updateParams, ['Authorization' => 'Bearer ' . $this->adminUserToken])
				        ->assertResponseStatus(200)
				        ->seeJsonSubset([
							'code' => 0,
						 	'message' => 'success',
						 	'data' => $this->updateParams
						]);
		// Get newly created update id
        $responseContent = json_decode($request->response->getContent(), true);
        $updateId = $responseContent['data']['id'];

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // List update endpoint should return no updates as no Update is linked to the user yet.
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->json('GET', '/v1/update', [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Show update endpoint should respond with Not found as the Update is not linked to the user yet.
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(404)
	        ->seeJson([
				'code' => 404,
			 	'message' => 'Update is not found.'
			]);		

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Mark as Read endpoint should return success, but nothing should have been changed, since it's not linked yet.
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->json('PUT', '/v1/update/' . $updateId . '/markAsRead', [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);		

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Link an admin investment to the Update.
		// It should link the Update to the Admin User.
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$params = [
			'type' => 'Investment',
			'id'   => $this->adminInvestor->investments->first()->id
		];
        $this->json('POST', '/v1/update/' . $updateId . '/link', $params, ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

		// Show update endpoint should respond with Not found when called with client token.
        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->clientUserToken])
	        ->assertResponseStatus(404)
	        ->seeJson([
				'code' => 404,
			 	'message' => 'Update is not found.'
			]);		

		// Show update endpoint should respond with the Update and `is_read` should still be false.
        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data' => [
			 		'id' 	  	  => $updateId,
			 		'is_read' 	  => false,
			 		'local_id'	  => 1,		// It's the first update linked to the Admin user
			 		'investments' => [
				 		'data' => [
				 			['id' => $this->adminInvestor->investments->first()->id]
				 		]
			 		]
			 	]
			]);

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Test Mark As
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// Mark as Read should mark the Update as Read
        $this->json('PUT', '/v1/update/' . $updateId . '/markAsRead', [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

		// Show update endpoint should respond with the Update and `is_read` should be true.
        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data' => ['is_read' => true]
			]);

		// Mark as Unread should mark the Update as Unread
        $this->json('PUT', '/v1/update/' . $updateId . '/markAsUnread', [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

		// Show update endpoint should respond with the Update and `is_read` should be false.
        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data' => ['is_read' => false]
			]);

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Link the project to the Update.
		// Client should now have access to the Update.
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$params = [
			'type' => 'Project',
			'id'   => $this->project->id
		];
        $this->json('POST', '/v1/update/' . $updateId . '/link', $params, ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

		// Show update endpoint should respond with the Update and `is_read` should still be false.
        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->clientUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data' => [
			 		'id' 	  	  => $updateId,
			 		'is_read' 	  => false,
			 		'local_id'	  => 1,
			 		'investments' => [
				 		'data' => [
				 			['id' => $this->adminInvestor->investments->first()->id]
				 		]
			 		],
			 		'projects' => [
				 		'data' => [
				 			['id' => $this->project->id]
				 		]
			 		],
			 	]
			]);		

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Unlink Admin Investment from the Update.
		// Admin user should still have access to the Update as it's still
		// linked to the project on which the Admin user has an investment.
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$params = [
			'type' => 'Investment',
			'id'   => $this->adminInvestor->investments->first()->id
		];
		$this->json('POST', '/v1/update/' . $updateId . '/unlink', $params, ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success'
			]);

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Unlink Project from the Update.
		// No one should have access to the Update now.
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$params = [
			'type' => 'Project',
			'id'   => $this->project->id
		];
        $this->json('POST', '/v1/update/' . $updateId . '/unlink', $params, ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

		$this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(404)
	        ->seeJson([
				'code' => 404,
			 	'message' => 'Update is not found.'
			]);

		$this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->clientUserToken])
	        ->assertResponseStatus(404)
	        ->seeJson([
				'code' => 404,
			 	'message' => 'Update is not found.'
			]);	

	    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Link the admin investor to the Update.
		// Local Id should now be 2.
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		$params = [
			'type' => 'Investor',
			'id'   => $this->adminInvestor->id
		];
        $this->json('POST', '/v1/update/' . $updateId . '/link', $params, ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJson([
				'code' => 0,
			 	'message' => 'success'
			]);

		// Show update endpoint should respond with the Update and `local_id` should be 2.
        $this->json('GET', '/v1/update/' . $updateId, [], ['Authorization' => 'Bearer ' . $this->adminUserToken])
	        ->assertResponseStatus(200)
	        ->seeJsonSubset([
				'code' => 0,
			 	'message' => 'success',
			 	'data' => [
			 		'id' 	  	  => $updateId,
			 		'is_read' 	  => false,
			 		'local_id'	  => 2,
			 		'investors' => [
				 		'data' => [
				 			['id' => $this->adminInvestor->id]
				 		]
			 		]
			 	]
			]);		
    }
}
