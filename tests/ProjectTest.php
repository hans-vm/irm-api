<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\Project;
use App\Models\ProjectType;
use App\Models\ProjectStatus;
use App\Models\ProjectMarket;

class ProjectTest extends TestCase
{

	/** 
	 * Test users 
	 */
	protected static $TestAdminUser = [
		'email'		=> 'admin@test.com',
		'password'	=> 'admin'
	];

	protected static $TestClientUser = [
		'email'		=> 'client@test.com',
		'password'	=> 'client'
	];


	/** 
	 * Test projects
	 */
	protected static $TestProjects = [
		[
			'name' 		=> 	'Testing project 01', 
			'type' 		=> 	1, 
			'status' 	=> 	1,
			'market'	=> 	1
		],
		[
			'name' 		=> 	'Testing project 02', 
			'type' 		=> 	2, 
			'status' 	=> 	2,
			'market'	=> 	2
		],
		[
			'name' 		=> 'Testing project 03', 
			'type' 		=> 3,
			'status'	=> 3,
			'market'	=> 3
		],
	];


    /**
     * Project related API testing with admin role
     *
     * @return void
     */
    public function testProjectAdmin()
    {

        // making auth request
        $request = $this->json('POST', '/v1/user/auth', static::$TestAdminUser);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);
        // user token
        $userToken = $responseContent['data']['token'];

        // ===========================================================================================

        print_r("Testing project model information endpoint : started\n");

        // assert model info
        $request = $this->json('GET', '/v1/project/info', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['fields', 'preview', 'form']
        ]);

        print_r("Testing project model information endpoint : finished\n");

		// ===========================================================================================        

		print_r("Testing project creation endpoint : started\n");  

        $project = static::$TestProjects[0];

        // creating new project
        $request = $this->json('POST', '/v1/project', $project, ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);

        // validate created project
		$request->seeJsonSubset([
			'code' 		=> 0,
			'message'	=> 'success',
		 	'data' 		=> $project
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);

        // created project id 
        $projectId = $responseContent['data']['id'];
    
        print_r("Testing project creation endpoint : finished\n");  

		// ===========================================================================================        

		print_r("Testing project details endpoint : started\n");

        // assert project details api
        $request = $this->json('GET', '/v1/project/' . $projectId, [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure and content
		$request->seeJsonSubset([
			'code' 		=> 0,
			'message'	=> 'success',
		 	'data' 		=> $project
		]);

		print_r("Testing project details endpoint : finished\n");

		// ===========================================================================================        

		print_r("Testing project update endpoint : started\n");

		// assert project update api
		$project['name'] = $project['name'] . ' Updated';
		
        $request = $this->json('PUT', '/v1/project/' . $projectId, $project, ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure and content
		$request->seeJsonSubset([
			'code' 		=> 0,
			'message'	=> 'success',
		 	'data' 		=> $project
		]);

		print_r("Testing project update endpoint : finished\n");

		// ===========================================================================================        

		print_r("Testing project delete endpoint : started\n");

		// assert project delete api
        $request = $this->json('DELETE', '/v1/project/' . $projectId, [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure and content
		$request->seeJsonSubset([
			'code' 		=> 0,
			'message'	=> 'success'
		]);

		print_r("Testing project delete endpoint : finished\n");			

		// ===========================================================================================        

        print_r("Testing project list endpoint : started\n");

        // assert projects list api
		$request = $this->json('GET', '/v1/project', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => [
        		'*'	=> [
        			"id", "name", "type", "status", "market", "start_date", "end_date", "raise_amount", "cap_amount"
        		]
        	]
        ]);  

        print_r("Testing project list endpoint : finished\n");

		// ===========================================================================================        
   		// remove test data completely from database
		Project::withTrashed()->find($projectId)->forceDelete();

    }


    /**
     * Project related API testing with client role
     *
     * @return void
     */
    public function testProjectClient()
    {

        // making auth request
        $request = $this->json('POST', '/v1/user/auth', static::$TestClientUser);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);
        // user token
        $userToken = $responseContent['data']['token'];

        // ===========================================================================================

        print_r("Testing project model information endpoint : started\n");

        // assert model info
        $request = $this->json('GET', '/v1/project/info', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['fields', 'preview', 'form']
        ]);

        print_r("Testing project model information endpoint : finished\n");

		// ===========================================================================================        

		print_r("Testing project creation endpoint : started\n");  

        $project = static::$TestProjects[0];

        // creating new project
        $request = $this->json('POST', '/v1/project', $project, ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(401);

        print_r("Testing project creation endpoint : finished\n");  

		// ===========================================================================================        

        print_r("Testing project list endpoint : started\n");

        // assert projects list api
		$request = $this->json('GET', '/v1/project', [], ['Authorization' => 'Bearer ' . $userToken]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => [
        		'*'	=> [
        			"id", "name", "type", "status", "market", "start_date", "end_date", "raise_amount", "cap_amount"
        		]
        	]
        ]);  

        print_r("Testing project list endpoint : finished\n");

    }
}
