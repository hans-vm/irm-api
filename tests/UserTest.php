<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Models\User;

class UserTest extends TestCase
{
	/** 
	 * Test cases 
	 */
	protected static $TestAuthUser = [
		'email'		=> 'admin@test.com',
		'password'	=> 'admin'
	];

	protected static $TestRegistrationUsers = [
		[
			'email' => 'testingunit0001@gmail.com', 
			'password' => 'password', 
			'agree_terms'	=> '1'
		],
		[
			'email' => 'testingunit0002@gmail.com', 
			'password' => 'password',
			'agree_terms'	=> '1'
		],
		[
			'email' => 'testingunit0003@gmail.com', 
			'password' => 'password',
			'agree_terms'	=> '1'
		]
	];

    /**
     * User authentication test.
     *
     * @return void
     */
    public function testAuth()
    {
    	print_r("Testing user authentication endpoint : started\n");

    	// making auth request
        $request = $this->json('POST', '/v1/user/auth', static::$TestAuthUser);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		print_r("Testing user authentication endpoint : finished\n");
    }

    /**
     * User registration test.
     *
     * @return void
     */
    public function testRegistration()
    {

  		foreach (static::$TestRegistrationUsers as $key => $regUser) {

  			print_r("Testing user registration endpoint : started\n");

	    	// making registration request
	        $request = $this->json('POST', '/v1/user/register', $regUser, []);

	        // check if response is ok
	        $request->assertResponseStatus(200);			
	        
	        // validate created user
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'email'		=> $regUser['email']
			 	]
			]);

			/*
			// get content 
	        $responseContent = json_decode($request->response->getContent(), true);

	        // user token
	        $userToken = $responseContent['data']['token'];

	        // get registered user information
			$request = $this->json('GET', '/v1/user/self', [], ['Authorization' => 'Bearer ' . $userToken]);

			// get content 
	        $responseContent = json_decode($request->response->getContent(), true);
	        
			// check if response is ok
        	$request->assertResponseStatus(200);

        	// check registered user information
			$request->seeJsonSubset([
				'code' 		=> 0,
				'message'	=> 'success',
			 	'data' 		=> [
			 		'email' 	=> $regUser['email']
			 	]
			]); */        

			// get content 
	        $responseContent = json_decode($request->response->getContent(), true);

	        // user id
	        $userId = $responseContent['data']['id'];
			
			static::$TestRegistrationUsers[$key]['id'] = $userId;

	        print_r("Testing user registration endpoint : finished\n");
	    }	

	    // remove users created for testing
	    foreach (static::$TestRegistrationUsers as $key => $regUser) {
			User::find($regUser['id'])->forceDelete();	    	
	    }
    }

    /**
     * User loginAs test.
     *
     * @return void
     */
    public function testLoginAs()
    {
    	print_r("Testing user loginAs endpoint : started\n");

    	// ===========================================================================
    	// making auth request
        $request = $this->json('POST', '/v1/user/auth', static::$TestAuthUser);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);
        // user token
        $token = $responseContent['data']['token'];

        // ============================================================================
        // login as a client
        $request = $this->json('POST', '/v1/admin/loginas/3', [], ['Authorization' => 'Bearer ' . $token]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);

		// get content 
        $responseContent = json_decode($request->response->getContent(), true);
        // user token
        $token = $responseContent['data']['token'];

        // ============================================================================
        // see if the user still has admin privilege
        $request = $this->json('GET', '/v1/user', [], ['Authorization' => 'Bearer ' . $token]);
        // check if response is ok
        $request->assertResponseStatus(200);			

		// ============================================================================
        // log out to the original admin user
        $request = $this->json('POST', '/v1/user/logout', [], ['Authorization' => 'Bearer ' . $token]);
        // check if response is ok
        $request->assertResponseStatus(200);			
        // check structure
        $request->seeJsonStructure([
        	'code',
        	'message',
        	'data' => ['token']
        ]);
        // check response
		$request->seeJson([
			'code' => 0,
		 	'message' => 'success'	
		]);
        
		print_r("Testing user loginAs endpoint : finished\n");
    }
}
