<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Project::class, function (Faker\Generator $faker) {
    return [
 		'name'	=> $faker->numerify('Project ###'),
 		'project_status_id'	=> $faker->numberBetween(1, 3),
 		'project_type_id'	=> $faker->numberBetween(1, 9),
        'project_market_id' => $faker->numberBetween(1, 5),
 		'raise_amount'		=> $faker->randomFloat(2, 0, 50000),
 		'cap_amount'		=> $faker->randomFloat(2, 50000, 100000)
    ];
});
