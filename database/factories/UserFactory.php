<?php

use App\Models\User;
use App\Models\UserRole;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
 		'first_name'	=> $faker->firstName,
 		'last_name'		=> $faker->lastName,
 		'email'			=> $faker->safeEmail,
 		'password'		=> bcrypt(str_random(10)),
        'user_role_id'  => $faker->numberBetween(1, 3),
        'enabled'       => 1
    ];
});

$factory->defineAs(User::class, 'admin', function (Faker\Generator $faker) use ($factory) {
	$user = $factory->raw(User::class);

    return array_merge($user, ['user_role_id'  => User::GetUserRoleIDByName(UserRole::$ADMIN_ROLE_NAME)]);
});

$factory->defineAs(User::class, 'manager', function (Faker\Generator $faker) use ($factory) {
	$user = $factory->raw(User::class);

    return array_merge($user, ['user_role_id'  => User::GetUserRoleIDByName(UserRole::$MANAGER_ROLE_NAME)]);
});

$factory->defineAs(User::class, 'client', function (Faker\Generator $faker) use ($factory) {
	$user = $factory->raw(User::class);

    return array_merge($user, ['user_role_id'  => User::GetUserRoleIDByName(UserRole::$CLIENT_ROLE_NAME)]);
});