<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Update::class, function (Faker\Generator $faker) {
    return [
 		'title'				=> $faker->numerify('Update ###'),
 		'update_status_id'	=> $faker->numberBetween(1, 2),
 		'update_type_id'	=> $faker->numberBetween(1, 5),
        'uuid'              => $faker->uuid,
        'content'			=> $faker->text(),
        'publisher_id'      => 1,
        'published_at'		=> $faker->dateTimeBetween($startDate = '-1 months',  $endDate = 'now')
    ];
});
