<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Investment::class, function (Faker\Generator $faker) {
    return [
		'title'					=> $faker->numerify('Investment ####'),
 		'investment_status_id'	=> $faker->numberBetween(1, 3),
 		'uuid'					=> $faker->uuid,
 		'note'					=> $faker->text(),
        'investment_date'      	=> $faker->dateTimeBetween($startDate = '-3 months', $endDate = '-1 months')
    ];
});

$factory->define(App\Models\InvestmentTransaction::class, function (Faker\Generator $faker) {
    return [
        'title'         => $faker->numerify('Investment Transaction ######'),
        'investment_transaction_status_id' => $faker->numberBetween(1, 3),
        'investment_transaction_type_id'   => $faker->numberBetween(1, 3),
        'uuid'          => $faker->uuid,
        'amount'        => $faker->randomFloat(2, -500000, 500000),
        'note'          => $faker->text(),
        'transaction_date'      => $faker->dateTimeBetween($startDate = '-1 months',  $endDate = 'now')
    ];
});
