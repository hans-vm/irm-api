<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Transfer::class, function (Faker\Generator $faker) {
    return [
 		'source_id'		     => $faker->numberBetween(1, 3),
        'source_type'        => $faker->randomElement($array = array('Account', 'Investment')),
        'destination_id'     => $faker->numberBetween(1, 3),
        'destination_type'   => $faker->randomElement($array = array('Account', 'Investment')),
 		'transfer_status_id' => $faker->numberBetween(1, 4),
 		'transfer_type_id'	 => $faker->numberBetween(1, 2),
        'uuid'               => $faker->uuid,	
        'amount'             => $faker->randomFloat(2, 100, 1000) 
    ];
});
