<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Notification::class, function (Faker\Generator $faker) {
    return [
 		'title'					=> $faker->numerify('Notification ###'),
 		'message'				=> $faker->text,
 		'action'				=> $faker->url,
        'uuid'              	=> $faker->uuid,
        'created_by'			=> 1,
        'notification_type_id'  => $faker->numberBetween(1, 6),
        // 'notifiable_id'			=> 3,
        // 'notifiable_type'		=> 'User',
        'display_mode_alert' 	=> $faker->randomElement(array(true, false)),
        'display_mode_menu'  	=> $faker->randomElement(array(true, false)),
        'display_mode_sidebar'  => $faker->randomElement(array(true, false)),
        'display_mode_email' 	=> $faker->randomElement(array(true, false))
    ];
});
