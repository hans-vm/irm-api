<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Account::class, function (Faker\Generator $faker) {
    return [
		'title'				=> $faker->numerify('Account ####'),
 		'account_status_id'	=> $faker->numberBetween(1, 3),
 		'account_type_id'	=> $faker->numberBetween(1, 3),
 		'uuid'				=> $faker->uuid,
 		'note'				=> $faker->text(),
        'account_date'      => $faker->dateTimeBetween($startDate = '-3 months', $endDate = '-1 months')
    ];
});

$factory->define(App\Models\AccountTransaction::class, function (Faker\Generator $faker) {
    return [
        'title'         => $faker->numerify('Account Transaction ######'),
        'account_transaction_status_id' => $faker->numberBetween(1, 3),
        'account_transaction_type_id'   => $faker->numberBetween(1, 3),
        'uuid'          => $faker->uuid,
        'amount'        => $faker->randomFloat(2, -500000, 500000),
        'note'          => $faker->text(),
        'transaction_date'      => $faker->dateTimeBetween($startDate = '-1 months',  $endDate = 'now')
    ];
});
