<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Investor::class, function (Faker\Generator $faker) {
    return [
 		'name'				=> $faker->numerify('Investor ###'),
 		'investor_status_id'=> $faker->numberBetween(1, 4),
 		'investor_type_id'	=> $faker->numberBetween(1, 12),
        'uuid'              => $faker->uuid,	    
    ];
});
