<?php

use Illuminate\Database\Seeder;

class NotificationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_notification_types')->insert([
     		'name'	=> 'General'
     	]);

        DB::table('irm_notification_types')->insert([
            'name'  => 'Update'
        ]);

        DB::table('irm_notification_types')->insert([
            'name'  => 'Success'
        ]);
        
        DB::table('irm_notification_types')->insert([
            'name'  => 'Information'
        ]);

        DB::table('irm_notification_types')->insert([
            'name'  => 'Warning'
        ]);

        DB::table('irm_notification_types')->insert([
            'name'  => 'Problem'
        ]);
    }
}
