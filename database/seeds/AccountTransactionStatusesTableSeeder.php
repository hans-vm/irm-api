<?php

use Illuminate\Database\Seeder;

class AccountTransactionStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_account_transaction_statuses')->insert([
     		'name'	=> 'Completed'
     	]);

     	DB::table('irm_account_transaction_statuses')->insert([
     		'name'	=> 'Pending'
     	]);

     	DB::table('irm_account_transaction_statuses')->insert([
     		'name'	=> 'Canceled'
     	]);
    }
}
