<?php

use Illuminate\Database\Seeder;

class UpdateTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('irm_update_types')->insert([
     		'name'	=> 'Update'
     	]);

        DB::table('irm_update_types')->insert([
     		'name'	=> 'Change'
     	]);

     	DB::table('irm_update_types')->insert([
     		'name'	=> 'Addition'
     	]);

        DB::table('irm_update_types')->insert([
            'name'  => 'Removal'
        ]);

        DB::table('irm_update_types')->insert([
            'name'  => 'Confirmation'
        ]);
        
    }
}
