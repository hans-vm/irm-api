<?php

use Illuminate\Database\Seeder;

class InvestmentTransactionStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_investment_transaction_statuses')->insert([
     		'name'	=> 'Completed'
     	]);

     	DB::table('irm_investment_transaction_statuses')->insert([
     		'name'	=> 'Pending'
     	]);

     	DB::table('irm_investment_transaction_statuses')->insert([
     		'name'	=> 'Canceled'
     	]);
    }
}
