<?php

use Illuminate\Database\Seeder;

class InvestorTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('irm_investor_types')->insert([
     		'name'	=> 'Individual'
     	]);

        DB::table('irm_investor_types')->insert([
     		'name'	=> 'GP'
     	]);

     	DB::table('irm_investor_types')->insert([
     		'name'	=> 'LP'
     	]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'LLP'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'LLLP'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'LLC'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'PLLC'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'PC'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'C Corp.'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'S Corp.'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'Sole Proprietorships'
        ]);

        DB::table('irm_investor_types')->insert([
            'name'  => 'Trust'
        ]);

    }
}
