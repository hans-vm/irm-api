<?php

use Illuminate\Database\Seeder;

class UpdateStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_update_statuses')->insert([
     		'name'	=> 'Read'
     	]);

     	DB::table('irm_update_statuses')->insert([
     		'name'	=> 'Unread'
     	]);
    }
}
