<?php

use Illuminate\Database\Seeder;

class TransferTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('irm_transfer_types')->insert([
     		'name'	=> 'Distribution'
     	]);

        DB::table('irm_transfer_types')->insert([
     		'name'	=> 'Investment'
     	]);
        
    }
}
