<?php

use Illuminate\Database\Seeder;

class AccountStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_account_statuses')->insert([
     		'name'	=> 'Open'
     	]);

     	DB::table('irm_account_statuses')->insert([
     		'name'	=> 'Closed'
     	]);

     	DB::table('irm_account_statuses')->insert([
     		'name'	=> 'Pending'
     	]);
    }
}
