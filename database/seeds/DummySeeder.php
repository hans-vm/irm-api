<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Investor;
use App\Models\Account;
use App\Models\AccountTransaction;
use App\Models\Investment;
use App\Models\InvestmentTransaction;
use App\Models\Project;
use App\Models\Update;
use App\Models\Notification;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Create updates
    	$updates = factory(Update::class, 50)->create();

    	// Create projects
    	$projects = factory(Project::class, 10)->create()->each(function ($project) use($updates) {
    		// Associate the project to a random Update
            $updates->random()->projects()->attach($project);
    	});

    	// Take first 3 users
        User::take(3)->each(function ($user) use($projects, $updates) {

        	// Create investors for the user
        	$investors = factory(Investor::class, 2)->create()->each(function ($investor) use($user, $projects, $updates) {
        		
        		// Prepend user name before investor name
        		$investor->name = ucwords($user->first_name) . ' ' . $investor->name;

        		// Create accounts for the investor
        		$accounts = factory(Account::class, 2)
    						->create()
    						->each(function ($account) use($user, $updates) {

				        		// Prepend user name before account title
    							$account->title = ucwords($user->first_name) . ' ' . $account->title;
    							
    							// Create account transactions for the account
    							$transactions = factory(AccountTransaction::class, 50)->make();
    							$account->transactions()->saveMany($transactions);

    							// Associate the account to 2 random Updates
                                $updates->random()->accounts()->attach($account);
                                $updates->random()->accounts()->attach($account);
    						});
	    		$investor->accounts()->saveMany($accounts);

	    		// Create investments for the investor
	    		$investments = factory(Investment::class, 2)
	    							->create()
	    							->each(function ($investment) use($user, $accounts, $projects, $updates) {

						        		// Prepend user name before investment title
	    								$investment->title = ucwords($user->first_name) . ' ' . $investment->title;

	    								// Create investment transactions for the investment
	    								$transactions = factory(InvestmentTransaction::class, 50)
	    													->make()
	    													->each(function ($transaction) use($accounts) {
	    														// Associate investment transaction with a random account transaction
	    														$transaction->accountTransaction()->associate($accounts->pluck('transactions')->flatten(1)->random());
	    													});
	    								$investment->transactions()->saveMany($transactions);

	    								// Associate random project
	    								$investment->project()->associate($projects->random());

		    							// Associate the account to 2 random Updates
		    							$updates->random()->investments()->attach($investment);
                                        $updates->random()->investments()->attach($investment);
	    							});
	    		$investor->investments()->saveMany($investments);
        	});

			$user->investors()->saveMany($investors);    		

            // Create notifications for Investor/Account/Investment
            $notifications = factory(Notification::class, 10)
                                ->make()
                                ->each(function ($notification) use ($investors) {
                                    switch (rand() % 3) {
                                        case 0:
                                            // Investor
                                            $notification->notifiable()->associate($investors->random());
                                            break;
                                        case 1:
                                            // Account
                                            $notification->notifiable()->associate($investors->random()->accounts->random());
                                            break;
                                        case 2:
                                            // Investment
                                            $notification->notifiable()->associate($investors->random()->investments->random());
                                            break;
                                        default:
                                            break;
                                    }
                                    $notification->save();
                                });
            $user->notifications()->attach($notifications);
    	});

        // Sync users related to the Update
        $updates->each(function ($update) {
            $update->syncUsers();
        });
    }
}
