<?php

use Illuminate\Database\Seeder;

class SettingsFieldGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_settings_field_groups')->insert([
     		'name'	      => 'Personal',
            'category'    => 'user',
            'sort_order'  => 1
     	]);

        DB::table('irm_settings_field_groups')->insert([
            'name'        => 'Phone',
            'category'    => 'user',
            'sort_order'  => 2
        ]);

        DB::table('irm_settings_field_groups')->insert([
            'name'        => 'Address',
            'category'    => 'user',
            'sort_order'  => 3
        ]);

        DB::table('irm_settings_field_groups')->insert([
            'name'        => 'Phone',
            'category'    => 'communication',
            'sort_order'  => 1
        ]);

        DB::table('irm_settings_field_groups')->insert([
            'name'        => 'Email',
            'category'    => 'communication',
            'sort_order'  => 2
        ]);
        
    }
}
