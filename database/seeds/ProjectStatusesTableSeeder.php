<?php

use Illuminate\Database\Seeder;

class ProjectStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_project_statuses')->insert([
     		'name'	=> 'Active'
     	]);

     	DB::table('irm_project_statuses')->insert([
     		'name'	=> 'Pending'
     	]);

     	DB::table('irm_project_statuses')->insert([
     		'name'	=> 'Canceled'
     	]);
    }
}
