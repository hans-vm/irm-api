<?php

use Illuminate\Database\Seeder;

class SettingsFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User Personal
        DB::table('irm_settings_fields')->insert([
     		'title'      =>  'Title',
            'name'       =>  'name_title',
            'settings_field_type_id'  =>  5,
            'settings_field_group_id' =>  1,
            'sort_order' =>  1,
            'meta_data'  =>  '{"options" : ["Mr.", "Mrs.", "Ms.", "Miss."]}'
     	]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'First Name',
            'name'       =>  'name_first',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  1,
            'sort_order' =>  2,
            'linked_field' => 'first_name'
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Last Name',
            'name'       =>  'name_last',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  1,
            'sort_order' =>  3,
            'linked_field' => 'last_name'
        ]);

        // User Phone
        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Work',
            'name'       =>  'phone_work',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  2,
            'sort_order' =>  1,
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Home',
            'name'       =>  'phone_home',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  2,
            'sort_order' =>  2,
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Mobile',
            'name'       =>  'phone_mobile',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  2,
            'sort_order' =>  3,
        ]);

        // User Address
        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Street 1',
            'name'       =>  'address_street_1',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  3,
            'sort_order' =>  1,
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Street 2',
            'name'       =>  'address_street_2',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  3,
            'sort_order' =>  2,
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'City',
            'name'       =>  'address_city',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  3,
            'sort_order' =>  3,
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'State',
            'name'       =>  'address_state',
            'settings_field_type_id'  =>  5,
            'settings_field_group_id' =>  3,
            'sort_order' =>  4,
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Zip',
            'name'       =>  'address_zip',
            'settings_field_type_id'  =>  1,
            'settings_field_group_id' =>  3,
            'sort_order' =>  5,
        ]);

        // Communication Phone
        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Work',
            'name'       =>  'communication_phone_work',
            'settings_field_type_id'  =>  3,
            'settings_field_group_id' =>  4,
            'sort_order' =>  1,
            'default_value' => 1
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Home',
            'name'       =>  'communication_phone_home',
            'settings_field_type_id'  =>  3,
            'settings_field_group_id' =>  4,
            'sort_order' =>  2,
            'default_value' => 1
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Mobile',
            'name'       =>  'communication_phone_mobile',
            'settings_field_type_id'  =>  3,
            'settings_field_group_id' =>  4,
            'sort_order' =>  3,
            'default_value' => 1
        ]);

        // Communication Email
        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Notifications',
            'name'       =>  'communication_email_notifications',
            'settings_field_type_id'  =>  3,
            'settings_field_group_id' =>  5,
            'sort_order' =>  1,
            'default_value' => 1
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'Transactions',
            'name'       =>  'communication_email_transactions',
            'settings_field_type_id'  =>  3,
            'settings_field_group_id' =>  5,
            'sort_order' =>  2,
            'default_value' => 1
        ]);

        DB::table('irm_settings_fields')->insert([
            'title'      =>  'News',
            'name'       =>  'communication_email_news',
            'settings_field_type_id'  =>  3,
            'settings_field_group_id' =>  5,
            'sort_order' =>  3,
            'default_value' => 1
        ]);

    }
}
