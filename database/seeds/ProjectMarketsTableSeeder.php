<?php

use Illuminate\Database\Seeder;

class ProjectMarketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_project_markets')->insert([
     		'name'	=> 'Consumer'
     	]);

     	DB::table('irm_project_markets')->insert([
     		'name'	=> 'Energy'
     	]);

     	DB::table('irm_project_markets')->insert([
     		'name'	=> 'Real-Estate'
     	]);

     	DB::table('irm_project_markets')->insert([
     		'name'	=> 'Medical'
     	]);

     	DB::table('irm_project_markets')->insert([
     		'name'	=> 'Technology'
     	]);
    }
}
