<?php

use Illuminate\Database\Seeder;

class ProjectTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('irm_project_types')->insert([
     		'name'	=> 'Acquisition Capital'
     	]);

        DB::table('irm_project_types')->insert([
     		'name'	=> 'Buyout'
     	]);

     	DB::table('irm_project_types')->insert([
     		'name'	=> 'Consolidations'
     	]);

        DB::table('irm_project_types')->insert([
            'name'  => 'Divestitures'
        ]);

        DB::table('irm_project_types')->insert([
            'name'  => 'ESOP'
        ]);

        DB::table('irm_project_types')->insert([
            'name'  => 'Growth Capital'
        ]);

        DB::table('irm_project_types')->insert([
            'name'  => 'Recapitalization'
        ]);

        DB::table('irm_project_types')->insert([
            'name'  => 'Liquidity'
        ]);

        DB::table('irm_project_types')->insert([
            'name'  => 'Turnarounds'
        ]);
    }
}
