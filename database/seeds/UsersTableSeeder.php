<?php

use Illuminate\Database\Seeder;
use App\Models\UserRole;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientUserRole = UserRole::create([
            'role_name'     => 'Client'
        ]);

        $managerUserRole = UserRole::create([
            'role_name'     => 'Manager'
        ]);

        $adminUserRole = UserRole::create([
            'role_name'     => 'Admin'
        ]);

     	User::create([
     		'first_name'	=> 'admin',
     		'last_name'		=> 'admin',
     		'email'			=> 'admin@test.com',
     		'password'		=> bcrypt('admin'),
            'user_role_id'  => $adminUserRole->id,
            'enabled'       => 1
     	]);

        User::create([
            'first_name'    => 'manager',
            'last_name'     => 'manager',
            'email'         => 'manager@test.com',
            'password'      => bcrypt('manager'),
            'user_role_id'  => $managerUserRole->id,
            'enabled'       => 1
        ]);

        User::create([
            'first_name'    => 'client',
            'last_name'     => 'client',
            'email'         => 'client@test.com',
            'password'      => bcrypt('client'),
            'user_role_id'  => $clientUserRole->id,
            'enabled'       => 1
        ]);
    }
}
