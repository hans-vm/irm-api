<?php

use Illuminate\Database\Seeder;

class AccountTransactionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_account_transaction_types')->insert([
     		'name'	=> 'Wire'
     	]);

     	DB::table('irm_account_transaction_types')->insert([
     		'name'	=> 'Transfer'
     	]);

     	DB::table('irm_account_transaction_types')->insert([
     		'name'	=> 'Deposit'
     	]);
    }
}
