<?php

use Illuminate\Database\Seeder;

class InvestmentStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_investment_statuses')->insert([
     		'name'	=> 'Open'
     	]);

     	DB::table('irm_investment_statuses')->insert([
     		'name'	=> 'Closed'
     	]);

     	DB::table('irm_investment_statuses')->insert([
     		'name'	=> 'Pending'
     	]);
    }
}
