<?php

use Illuminate\Database\Seeder;

class TransferStatusesTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_transfer_statuses')->insert([
     		'name'	=> 'Active'
     	]);

     	DB::table('irm_transfer_statuses')->insert([
     		'name'	=> 'Pending'
     	]);

        DB::table('irm_transfer_statuses')->insert([
            'name'  => 'Failed'
        ]);

        DB::table('irm_transfer_statuses')->insert([
            'name'  => 'Complete'
        ]);
    }

}
