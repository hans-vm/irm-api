<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Settings
        $this->call(SettingsFieldTypesTableSeeder::class);
        $this->call(SettingsFieldGroupsTableSeeder::class);
        $this->call(SettingsFieldsTableSeeder::class);
        
        // User
        $this->call(UsersTableSeeder::class);

        // Account
        $this->call(AccountStatusesTableSeeder::class);
        $this->call(AccountTypesTableSeeder::class);
        $this->call(AccountTransactionStatusesTableSeeder::class);
        $this->call(AccountTransactionTypesTableSeeder::class);
        
        // Project
        $this->call(ProjectStatusesTableSeeder::class);
        $this->call(ProjectTypesTableSeeder::class);
        $this->call(ProjectMarketsTableSeeder::class);
        
        // Investment
        $this->call(InvestmentStatusesTableSeeder::class);
        $this->call(InvestmentTransactionStatusesTableSeeder::class);
        $this->call(InvestmentTransactionTypesTableSeeder::class);
        
        // Investor
        $this->call(InvestorStatusesTableSeeder::class);
        $this->call(InvestorTypesTableSeeder::class);
        
        // Update
        $this->call(UpdateStatusesTableSeeder::class);
        $this->call(UpdateTypesTableSeeder::class);
        
        // Transfer
        $this->call(TransferStatusesTableSeeder::class);
        $this->call(TransferTypesTableSeeder::class);
        
        // Notification
        $this->call(NotificationTypesTableSeeder::class);

        // ActivityLog
        $this->call(ActivityLogTypesTableSeeder::class);

        $this->call(DummySeeder::class);        
    }
}
