<?php

use Illuminate\Database\Seeder;

class InvestorStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_investor_statuses')->insert([
     		'name'	=> 'Active'
     	]);

     	DB::table('irm_investor_statuses')->insert([
     		'name'	=> 'Pending'
     	]);

        DB::table('irm_investor_statuses')->insert([
            'name'  => 'Not Active'
        ]);

     	DB::table('irm_investor_statuses')->insert([
     		'name'	=> 'Dissolved'
     	]);
    }
}
