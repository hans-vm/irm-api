<?php

use Illuminate\Database\Seeder;

class InvestmentTransactionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_investment_transaction_types')->insert([
     		'name'	=> 'Wire'
     	]);

     	DB::table('irm_investment_transaction_types')->insert([
     		'name'	=> 'Transfer'
     	]);

     	DB::table('irm_investment_transaction_types')->insert([
     		'name'	=> 'Deposit'
     	]);
    }
}
