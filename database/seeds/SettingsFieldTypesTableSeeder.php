<?php

use Illuminate\Database\Seeder;

class SettingsFieldTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_settings_field_types')->insert([
     		'name'	=> 'single-line-text'
     	]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'multi-line-text'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'checkbox'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'radio'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'single-item-select'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'multi-items-select'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'decimal-number'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'switch'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'date-input'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'dates-range'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'range-slider'
        ]);

        DB::table('irm_settings_field_types')->insert([
            'name'  => 'wysiwyg-editor'
        ]);
    }
}
