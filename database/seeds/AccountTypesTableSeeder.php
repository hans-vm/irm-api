<?php

use Illuminate\Database\Seeder;

class AccountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('irm_account_types')->insert([
     		'name'	=> 'IDA'
     	]);

        DB::table('irm_account_types')->insert([
     		'name'	=> 'RIA'
     	]);

     	DB::table('irm_account_types')->insert([
     		'name'	=> 'Trust'
     	]);

    }
}
