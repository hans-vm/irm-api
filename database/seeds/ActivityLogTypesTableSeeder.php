<?php

use Illuminate\Database\Seeder;

class ActivityLogTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // ======================================================
        // User logs
        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_users',
            'name'      => 'User Login',
            'action'    => 'login',
            'template'  => '%s Login'
        ]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_users',
            'name'      => 'User Logout',
            'action'    => 'logout',
            'template'  => '%s Logout'
        ]);

        DB::table('irm_activity_log_types')->insert([
     		'target'	=> 'irm_users',
            'name'      => 'User Added',
            'action'    => 'created',
            'template'  => '%s Added'
     	]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_users',
            'name'      => 'User Updated',
            'action'    => 'updated',
            'template'  => '%s Updated'
        ]);   

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_users',
            'name'      => 'User Removed',
            'action'    => 'deleted',
            'template'  => '%s Removed'
        ]);

        // ======================================================
        // Account logs
        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_accounts',
            'name'      => 'Account Added',
            'action'    => 'created',
            'template'  => '%s Added'
        ]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_accounts',
            'name'      => 'Account Updated',
            'action'    => 'updated',
            'template'  => '%s Updated'
        ]);   

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_accounts',
            'name'      => 'Account Removed',
            'action'    => 'deleted',
            'template'  => '%s Removed'
        ]);

        // ======================================================
        // Account Transaction logs
        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_account_transactions',
            'name'      => 'Account Transaction Added',
            'action'    => 'created',
            'template'  => '%s Added'
        ]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_account_transactions',
            'name'      => 'Account Transaction Updated',
            'action'    => 'updated',
            'template'  => '%s Updated'
        ]);   

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_account_transactions',
            'name'      => 'Account Transaction Removed',
            'action'    => 'deleted',
            'template'  => '%s Removed'
        ]);

        // ======================================================
        // Investment logs
        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investments',
            'name'      => 'Investment Added',
            'action'    => 'created',
            'template'  => '%s Added'
        ]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investments',
            'name'      => 'Investment Updated',
            'action'    => 'updated',
            'template'  => '%s Updated'
        ]);   

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investments',
            'name'      => 'Investment Removed',
            'action'    => 'deleted',
            'template'  => '%s Removed'
        ]);

        // ======================================================
        // Investment Transaction logs
        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investment_transactions',
            'name'      => 'Investment Transaction Added',
            'action'    => 'created',
            'template'  => '%s Added'
        ]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investment_transactions',
            'name'      => 'Investment Transaction Updated',
            'action'    => 'updated',
            'template'  => '%s Updated'
        ]);   

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investment_transactions',
            'name'      => 'Investment Transaction Removed',
            'action'    => 'deleted',
            'template'  => '%s Removed'
        ]);

        // ======================================================
        // Project logs
        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_projects',
            'name'      => 'Project Added',
            'action'    => 'created',
            'template'  => '%s Added'
        ]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_projects',
            'name'      => 'Project Updated',
            'action'    => 'updated',
            'template'  => '%s Updated'
        ]);   

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_projects',
            'name'      => 'Project Removed',
            'action'    => 'deleted',
            'template'  => '%s Removed'
        ]);

        // ======================================================
        // Investor logs
        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investors',
            'name'      => 'Investor Added',
            'action'    => 'created',
            'template'  => '%s Added'
        ]);

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investors',
            'name'      => 'Investor Updated',
            'action'    => 'updated',
            'template'  => '%s Updated'
        ]);   

        DB::table('irm_activity_log_types')->insert([
            'target'    => 'irm_investors',
            'name'      => 'Investor Removed',
            'action'    => 'deleted',
            'template'  => '%s Removed'
        ]);
    }
}
