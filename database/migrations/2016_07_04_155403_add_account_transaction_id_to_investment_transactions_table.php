<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountTransactionIdToInvestmentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('irm_investment_transactions', function (Blueprint $table) {
            
            $table->bigInteger('account_transaction_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('irm_investment_transactions', function (Blueprint $table) {
            
            $table->dropColumn('account_transaction_id');
            
        });
    }
}
