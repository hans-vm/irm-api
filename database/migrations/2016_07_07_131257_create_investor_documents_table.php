<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('irm_investor_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('investor_id');
            $table->string('name');
            $table->string('filename');
            $table->string('disk');
            $table->string('type');
            $table->bigInteger('size');
            $table->uuid('uuid')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('irm_investor_documents');
    }
}
