<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultsToRaiseCapAmountInProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('irm_projects', function($table) {

            $table->float('raise_amount')->nullable()->change();
            $table->float('cap_amount')->nullable()->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('irm_projects', function($table) {

            $table->float('raise_amount')->change();
            $table->float('cap_amount')->change();

        });
    }
}
