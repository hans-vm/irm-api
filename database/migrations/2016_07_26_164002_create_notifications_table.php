<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('irm_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('message');
            $table->string('action')->nullable();
            $table->bigInteger('created_by');
            $table->bigInteger('notifiable_id');
            $table->string('notifiable_type');
            $table->uuid('uuid')->unique();
            $table->boolean('display_mode_alert')->default(false);
            $table->boolean('display_mode_menu')->default(false);
            $table->boolean('display_mode_sidebar')->default(false);
            $table->boolean('display_mode_email')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('irm_notifications');
    }
}
