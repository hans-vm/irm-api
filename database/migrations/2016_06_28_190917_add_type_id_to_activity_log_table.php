<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeIdToActivityLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('irm_activity_log', function($table) {
            $table->integer('activity_log_type_id')->nullable();
            $table->integer('entity_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('irm_activity_log', function($table) {
            $table->dropColumn('activity_log_type_id');
            $table->dropColumn('entity_id');
        });
    }
}
