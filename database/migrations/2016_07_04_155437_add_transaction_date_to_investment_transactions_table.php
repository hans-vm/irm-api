<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionDateToInvestmentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('irm_investment_transactions', function (Blueprint $table) {
            
            $table->timestamp('transaction_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('irm_investment_transactions', function (Blueprint $table) {
            
            $table->dropColumn('transaction_date');
            
        });
    }
}
