<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('irm_transfers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('source_id');
            $table->string('source_type');
            $table->bigInteger('destination_id');
            $table->string('destination_type');
            $table->integer('transfer_type_id');
            $table->integer('transfer_status_id');
            $table->float('amount');
            $table->uuid('uuid')->unique();
            $table->boolean('manual_authorization')->default(true);
            $table->timestamp('transfer_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('irm_transfers');
    }
}
