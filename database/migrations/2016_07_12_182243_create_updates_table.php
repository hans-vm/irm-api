<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('irm_updates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->integer('update_type_id');
            $table->integer('update_status_id');
            $table->uuid('uuid')->unique();
            $table->bigInteger('publisher_id');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('irm_updates');
    }
}
