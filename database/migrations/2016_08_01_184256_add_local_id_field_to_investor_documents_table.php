<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalIdFieldToInvestorDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('irm_investor_documents', function (Blueprint $table) {
            $table->bigInteger('local_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('irm_investor_documents', function (Blueprint $table) {
            $table->dropColumn('local_id');
        });
    }
}
