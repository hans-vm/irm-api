<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvestmentDateToInvestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('irm_investments', function (Blueprint $table) {
            
            $table->timestamp('investment_date')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('irm_investments', function (Blueprint $table) {
            
            $table->dropColumn('investment_date');
            
        });
    }
}
